package dmc_Reservations_ReservationFlowReaders;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import dmc_Reservations_DataObjects.dmc_Reservations_DataObjects_OccupancyObject;
import dmc_Reservations_DataObjects.dmc_Reservations_DataObjects_Scenario;
import dmc_Reservations_DataObjects.dmc_Reservations_DataObjects_SearchObject;
import dmc_Utilities.login;
import dmc_inputdataLoader.dmc_inputdataLoader_ReservationObjectReader;


public class dmc_Reservations_ReservationFlowReaders_resultPageReaders {
	

	
	public void webResultsPageReader(WebDriver driver,dmc_Reservations_DataObjects_Scenario scenario ) {
		
		dmc_Reservations_DataObjects_SearchObject search=scenario.getSearch();
		
		
		System.out.println(driver.findElement(By.className("criteria-info-count")).getText());
		
		try {
			
			int numberofPackagesperPage=driver.findElements(By.className("result-block")).size();
			System.out.println(numberofPackagesperPage);
			
			for (int i = 0; i < numberofPackagesperPage ; i++) {
				
				WebElement resultblock=driver.findElements(By.className("result-block")).get(i);
				String packageName=resultblock.findElement(By.className("result-details-title")).getText();
				
				if (packageName.contains("Sydney Explorer")) {
					
					try {
						System.out.println(resultblock.findElement(By.className("price-unit")).getText());
						System.out.println(resultblock.findElement(By.className("price-amount")).getText());
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					try {
						System.out.println(resultblock.findElements(By.id("first-text")).get(0).getText());//first-text
						System.out.println(resultblock.findElements(By.id("first-text")).get(1).getText());
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					resultblock.findElement(By.className("fp-select-btn")).click();
					String PackageID=resultblock.findElement(By.className("fixed-packages-details-btn")).getAttribute("href").replace("javascript:result_wjt_WJ_1.moreInfo(", "").replace(",true)",	"").trim();
					System.out.println(PackageID);
					//packagedate hasDatepicker
					((JavascriptExecutor)driver).executeScript("$('#packagedate"+PackageID+"').val('"+search.getPackageStartDate()+"');");
					
					new Select(resultblock.findElement(By.id("rooms-"+PackageID))).selectByValue(search.getNoOfRooms());
					
					int numberofRooms=search.getOccupanyList().size();
					
					for (int j = 0; j < numberofRooms; j++) {
						
						dmc_Reservations_DataObjects_OccupancyObject occupancy=new dmc_Reservations_DataObjects_OccupancyObject();
						occupancy=search.getOccupanyList().get(j);
						
						new Select(resultblock.findElement(By.id("room-"+occupancy.getRoomNo()+"-adult-"+PackageID))).selectByValue(occupancy.getAdultCount());
						new Select(resultblock.findElement(By.id("room-"+occupancy.getRoomNo()+"-child-"+PackageID))).selectByValue(occupancy.getChildCount());
						try {
							new Select(resultblock.findElement(By.id("room-"+occupancy.getRoomNo()+"-infant-"+PackageID))).selectByValue(occupancy.getInfantCount());

						} catch (Exception e) {
							// TODO: handle exception
						}

					}
					
					resultblock.findElement(By.className("fp-search-btn")).click();

					
					
					
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		
		
	}
	
	
	public static void main(String[] args) throws Exception {
		
		dmc_inputdataLoader_ReservationObjectReader reader=new dmc_inputdataLoader_ReservationObjectReader();
		dmc_Reservations_DataObjects_Scenario scenario1=new dmc_Reservations_DataObjects_Scenario();
		scenario1=reader.scenarioRead().get(0);
		
		login newlogin=new login();
		WebDriver driver=newlogin.driverinitialize();
		newlogin.partnerLogin(driver, scenario1);
		
		dmc_Reservations_ReservationFlowReaders_bookingEngineReaders beRead=new dmc_Reservations_ReservationFlowReaders_bookingEngineReaders();
		beRead.bookingEngineLoader(driver, scenario1);
		
		dmc_Reservations_ReservationFlowReaders_resultPageReaders results=new dmc_Reservations_ReservationFlowReaders_resultPageReaders();
		results.webResultsPageReader(driver,scenario1);
		
		
	}
}
