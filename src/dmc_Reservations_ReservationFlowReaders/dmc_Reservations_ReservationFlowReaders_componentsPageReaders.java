package dmc_Reservations_ReservationFlowReaders;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import dmc_Reservations_DataObjects.dmc_Reservations_DataObjects_Scenario;
import dmc_Utilities.login;
import dmc_inputdataLoader.dmc_inputdataLoader_ReservationObjectReader;

public class dmc_Reservations_ReservationFlowReaders_componentsPageReaders {
	
	public void webComponentsRead(WebDriver driver) throws Exception {
		
		System.out.println(driver.findElement(By.id("airridetracer")).getAttribute("value"));
		String rideID=driver.findElement(By.id("airridetracer")).getAttribute("value");
		/*WebDriver driver1;
		login newlogin=new login();
		driver1=newlogin.driverinitialize();
		driver1.get("http://dev3.rezg.net:8080/Browser.jsp?sort=-3&file=%2Fvar%2Flog%2Frezg%2Fapp%2Fairxml%2F05-02-2016%2FSabre+-+AIR2%2FAIR2_LowFareSearchRequest_"+rideID+"_2016-02-05_15%3A31%3A15.272.xml");
		driver1.quit();
		System.out.println(driver.findElement(By.id("airridetracer")).getAttribute("value"));*/
		
		driver.findElement(By.className("dmc-hotel-wrapper"));
		
		//HotelDetails
		
		System.out.println(driver.findElement(By.className("dmc-hotel-details")).findElement(By.className("rooms")).getText());
		System.out.println(driver.findElement(By.className("dmc-hotel-details")).findElement(By.className("adult")).getText());
		System.out.println(driver.findElement(By.className("dmc-hotel-details")).findElement(By.className("children")).getText());

		
		int roomCount=driver.findElements(By.className("dmc-room-selection-wrap")).size();
		
		for (int i = 1; i <=roomCount; i++) {
			
			int numberofRoomOptions=driver.findElements(By.id("RadioGroupDMC_rooms_"+i)).size();
			System.out.println("numberofRoomOptions"+numberofRoomOptions);
			
			for (int j = 0; j < numberofRoomOptions; j++) {
				
				System.out.println(driver.findElements(By.className("dmc-room-selection-wrap")).get(i-1).findElements(By.className("radio-selection-dmc")).get(j).findElement(By.className("radio-btn")).getText());
			try {
				
				System.out.println(driver.findElements(By.className("dmc-room-selection-wrap")).get(i-1).findElements(By.className("radio-selection-dmc")).get(j).findElement(By.className("price")).getText());

			} catch (Exception e) {
				// TODO: handle exception
			}
				
				
				
			}
			
		}
	
	
	
	}
	
	public void name() {
		
	}
	
	public static void  main(String[] args) throws Exception {

		
		dmc_inputdataLoader_ReservationObjectReader reader=new dmc_inputdataLoader_ReservationObjectReader();
		dmc_Reservations_DataObjects_Scenario scenario1=new dmc_Reservations_DataObjects_Scenario();
		scenario1=reader.scenarioRead().get(0);
		
		login newlogin=new login();
		WebDriver driver=newlogin.driverinitialize();
		newlogin.partnerLogin(driver, scenario1);
		
		dmc_Reservations_ReservationFlowReaders_bookingEngineReaders beRead=new dmc_Reservations_ReservationFlowReaders_bookingEngineReaders();
		beRead.bookingEngineLoader(driver, scenario1);
		
		dmc_Reservations_ReservationFlowReaders_resultPageReaders results=new dmc_Reservations_ReservationFlowReaders_resultPageReaders();
		results.webResultsPageReader(driver,scenario1);
		
		dmc_Reservations_ReservationFlowReaders_componentsPageReaders components=new dmc_Reservations_ReservationFlowReaders_componentsPageReaders();
		components.webComponentsRead(driver);
		
		
	
		
	}

}
