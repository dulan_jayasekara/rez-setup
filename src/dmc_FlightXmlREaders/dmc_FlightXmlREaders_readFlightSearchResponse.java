package dmc_FlightXmlREaders;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class dmc_FlightXmlREaders_readFlightSearchResponse {
	
	public String urlGenerator(String url,String supplier)
	{

		String URL = null;

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal = Calendar.getInstance();
		String DATE = dateFormat.format(cal.getTime());
		URL = url.replace("DATE", DATE).replace("SUPPLIER", supplier);



		return URL;

	}

}
