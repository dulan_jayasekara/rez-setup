package dmc_Reservations_DataObjects;

public class dmc_Reservations_DataObjects_Scenario {
	
	String url;
	String partnerType;
	String bookingChannel;
	
	dmc_Reservations_DataObjects_SearchObject search=new dmc_Reservations_DataObjects_SearchObject();
	
	
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	public String getBookingChannel() {
		return bookingChannel;
	}
	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}
	public dmc_Reservations_DataObjects_SearchObject getSearch() {
		return search;
	}
	public void setSearch(dmc_Reservations_DataObjects_SearchObject search) {
		this.search = search;
	}
	
	
	
	

}
