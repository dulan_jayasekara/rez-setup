package dmc_Reservations_DataObjects;

import java.util.ArrayList;
import java.util.List;

public class dmc_Reservations_DataObjects_SearchObject {
	
	
	String countryOfResidence;
	String flightInclusive;
	String Origin;
	String OriginValue;
	String destination;
	String destinationValue;
	String departureMonth;
	String duration;
	String packageType;
	String preferredCurrency;
	String packageStartDate;
	String noOfRooms;
	
	List<dmc_Reservations_DataObjects_OccupancyObject> occupanyList=new ArrayList<dmc_Reservations_DataObjects_OccupancyObject>();
	
	
	
	public String getCountryOfResidence() {
		return countryOfResidence;
	}
	public void setCountryOfResidence(String countryOfResidence) {
		this.countryOfResidence = countryOfResidence;
	}
	public String getFlightInclusive() {
		return flightInclusive;
	}
	public void setFlightInclusive(String flightInclusive) {
		this.flightInclusive = flightInclusive;
	}
	public String getOrigin() {
		return Origin;
	}
	public void setOrigin(String origin) {
		Origin = origin;
	}
	public String getOriginValue() {
		return OriginValue;
	}
	public void setOriginValue(String originValue) {
		OriginValue = originValue;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDestinationValue() {
		return destinationValue;
	}
	public void setDestinationValue(String destinationValue) {
		this.destinationValue = destinationValue;
	}
	public String getDepartureMonth() {
		return departureMonth;
	}
	public void setDepartureMonth(String departureMonth) {
		this.departureMonth = departureMonth;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getPackageType() {
		return packageType;
	}
	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}
	public String getPreferredCurrency() {
		return preferredCurrency;
	}
	public void setPreferredCurrency(String preferredCurrency) {
		this.preferredCurrency = preferredCurrency;
	}
	public String getPackageStartDate() {
		return packageStartDate;
	}
	public void setPackageStartDate(String packageStartDate) {
		this.packageStartDate = packageStartDate;
	}
	public String getNoOfRooms() {
		return noOfRooms;
	}
	public void setNoOfRooms(String noOfRooms) {
		this.noOfRooms = noOfRooms;
	}
	public List<dmc_Reservations_DataObjects_OccupancyObject> getOccupanyList() {
		return occupanyList;
	}
	public void setOccupanyList(
			List<dmc_Reservations_DataObjects_OccupancyObject> occupanyList) {
		this.occupanyList = occupanyList;
	}
	

	
	
}
