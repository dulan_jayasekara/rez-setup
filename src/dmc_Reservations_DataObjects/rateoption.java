package dmc_Reservations_DataObjects;

public class rateoption {
	
	int optionindex;
	String ratecaption;
	double optionrate;
	double ratedifference;
	
	
	public int getOptionindex() {
		return optionindex;
	}
	public void setOptionindex(int optionindex) {
		this.optionindex = optionindex;
	}
	public String getRatecaption() {
		return ratecaption;
	}
	public void setRatecaption(String ratecaption) {
		this.ratecaption = ratecaption;
	}
	public double getOptionrate() {
		return optionrate;
	}
	public void setOptionrate(double optionrate) {
		this.optionrate = optionrate;
	}
	public double getRatedifference() {
		return ratedifference;
	}
	public void setRatedifference(double ratedifference) {
		this.ratedifference = ratedifference;
	}
	
	

}
