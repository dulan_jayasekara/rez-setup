package dmc_Reservations_DataObjects;

public class dmc_Reservations_DataObjects_DisplayRateObject {
	
	int airbasefare;
	int airtax;
	int airbookingfee;
	int landbasefare;
	int landtax;
	int profitmarkup;
	
	int webratedisplay;
	
	int ccsubtotaldisplay;
	int cctaxdisplay;
	
	public int getAirbasefare() {
		return airbasefare;
	}
	public void setAirbasefare(int airbasefare) {
		this.airbasefare = airbasefare;
	}
	public int getAirtax() {
		return airtax;
	}
	public void setAirtax(int airtax) {
		this.airtax = airtax;
	}
	public int getAirbookingfee() {
		return airbookingfee;
	}
	public void setAirbookingfee(int airbookingfee) {
		this.airbookingfee = airbookingfee;
	}
	public int getLandbasefare() {
		return landbasefare;
	}
	public void setLandbasefare(int landbasefare) {
		this.landbasefare = landbasefare;
	}
	public int getLandtax() {
		return landtax;
	}
	public void setLandtax(int landtax) {
		this.landtax = landtax;
	}
	public int getProfitmarkup() {
		return profitmarkup;
	}
	public void setProfitmarkup(int profitmarkup) {
		this.profitmarkup = profitmarkup;
	}
	public int getWebratedisplay() {
		return webratedisplay;
	}
	public void setWebratedisplay(int webratedisplay) {
		this.webratedisplay = webratedisplay;
	}
	public int getCcsubtotaldisplay() {
		return ccsubtotaldisplay;
	}
	public void setCcsubtotaldisplay(int ccsubtotaldisplay) {
		this.ccsubtotaldisplay = ccsubtotaldisplay;
	}
	public int getCctaxdisplay() {
		return cctaxdisplay;
	}
	public void setCctaxdisplay(int cctaxdisplay) {
		this.cctaxdisplay = cctaxdisplay;
	}
	
	
	
	

}
