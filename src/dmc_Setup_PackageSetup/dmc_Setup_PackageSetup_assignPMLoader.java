package dmc_Setup_PackageSetup;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import dmc_Setup_DataObjects.dmc_Setup_DataObjects_DMCPackageObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_partnerMarkupObject;
import setup.com.utill.ExtentReportTemplate;


public class dmc_Setup_PackageSetup_assignPMLoader {
	
	 private Map<String, String> propertyset = null;
	   
	   
	   public dmc_Setup_PackageSetup_assignPMLoader(Map<String, String> propertyset) {
		this.propertyset = propertyset;
	}

	public void assignProfitMarkup(dmc_Setup_DataObjects_DMCPackageObject dmcObj,WebDriver driver) throws InterruptedException {

		driver.get(propertyset.get("Hotel.BaseUrl")+"/dmc/setup/DMCPackageMarkupCommissionSetupPage.do");

		driver.findElement(By.id("packageName")).clear();
		driver.findElement(By.id("packageName")).sendKeys(dmcObj.getStandardInfo().getPackageinfo().getPackageName());


		driver.findElement(By.id("packageName_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.className("rezgLook0")).click();

		driver.switchTo().defaultContent();

		driver.findElement(By.id("partnerType_DC")).click();
		if (dmcObj.getProfitMarkup().getDirectCustomerMarkup().getProfitMarkupType().contains("percentage")) {

			driver.findElement(By.id("markupByDC_P")).click();
		}else {

			driver.findElement(By.id("markupByDC_V")).click();
		}

		driver.findElement(By.id("markupAmountDC")).clear();
		driver.findElement(By.id("markupAmountDC")).sendKeys((String.valueOf(dmcObj.getProfitMarkup().getDirectCustomerMarkup().getProfitMarkup())));
		//TO net
		driver.findElement(By.id("partnerType_TON")).click();

		Map<String, dmc_Setup_DataObjects_partnerMarkupObject> tonetMap=new HashMap<String, dmc_Setup_DataObjects_partnerMarkupObject>();
		tonetMap=dmcObj.getProfitMarkup().getToNetMarkupmap();

		for (Entry<String, dmc_Setup_DataObjects_partnerMarkupObject> tonetentry:tonetMap.entrySet()) {

			if (tonetentry.getValue().getProfitMarkupType().contains("percentage")) {

				driver.findElement(By.id("markupByTON_P")).click();
			}else {

				driver.findElement(By.id("markupByTON_V")).click();
			}

			driver.findElement(By.id("markupAmountTON")).clear();
			driver.findElement(By.id("markupAmountTON")).sendKeys((String.valueOf(tonetentry.getValue().getProfitMarkup())));
			
		    Thread.sleep(5000);

			((JavascriptExecutor)driver).executeScript("addToList()");
		}

		//TO com		
		driver.findElement(By.id("partnerType_TOC")).click();

		Map<String, dmc_Setup_DataObjects_partnerMarkupObject> tocomMap=new HashMap<String, dmc_Setup_DataObjects_partnerMarkupObject>();
		tonetMap=dmcObj.getProfitMarkup().getToComMarkupmap();

		for (Entry<String, dmc_Setup_DataObjects_partnerMarkupObject> tocomentry:tocomMap.entrySet()) {

			if (tocomentry.getValue().getProfitMarkupType().contains("percentage")) {

				driver.findElements(By.id("markupByTOC_P")).get(0).click();
			}else {

				driver.findElements(By.id("markupByTOC_P")).get(1).click();
			}

			driver.findElement(By.id("markupAmountTOC")).clear();
			driver.findElement(By.id("markupAmountTOC")).sendKeys((String.valueOf(tocomentry.getValue().getProfitMarkup())));

			
			if (tocomentry.getValue().getCommissionType().contains("percentage")) {

				driver.findElements(By.id("commissionTypeTOC_P")).get(0).click();
			}else {

				driver.findElements(By.id("commissionTypeTOC_V")).get(1).click();
			}

			
			driver.findElement(By.id("commisionAmountTOC")).clear();
			driver.findElement(By.id("commisionAmountTOC")).sendKeys((String.valueOf(tocomentry.getValue().getProfitMarkup())));

			((JavascriptExecutor)driver).executeScript("addToList()");
		}


//affiliate
		
		driver.findElement(By.id("partnerType_AFF")).click();

		Map<String, dmc_Setup_DataObjects_partnerMarkupObject> affMap=new HashMap<String, dmc_Setup_DataObjects_partnerMarkupObject>();
		tonetMap=dmcObj.getProfitMarkup().getAffMarkupmap();

		for (Entry<String, dmc_Setup_DataObjects_partnerMarkupObject> affentry:affMap.entrySet()) {

			if (affentry.getValue().getProfitMarkupType().contains("Percentage")) {

				driver.findElements(By.id("markupByAFF_P")).get(0).click();
			}else {

				driver.findElements(By.id("markupByAFF_P")).get(1).click();
			}

			driver.findElement(By.id("markupAmountTOC")).clear();
			driver.findElement(By.id("markupAmountTOC")).sendKeys((String.valueOf(affentry.getValue().getProfitMarkup())));

			
			if (affentry.getValue().getCommissionType().contains("Percentage")) {

				driver.findElements(By.id("commissionTypeAFF_P")).get(0).click();
			}else {

				driver.findElements(By.id("commissionTypeAFF_V")).get(1).click();
			}
			

			driver.findElement(By.id("commisionAmountTOC")).clear();
			driver.findElement(By.id("commisionAmountTOC")).sendKeys((String.valueOf(affentry.getValue().getProfitMarkup())));

			((JavascriptExecutor)driver).executeScript("addToList()");
			
			
			
		}
		
		
		
		((JavascriptExecutor)driver).executeScript("confirmSave('submit');");
		
		WebDriverWait wait =  new WebDriverWait(driver, 60);
		try {
			
			
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
		    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
		   
		    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
		    	
		    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
				System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Assign markup page Saving function "+dmcObj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Assign markup should be saved", TextMessage , true);
				
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
		    }
		    else{
		    	
		    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Assign markup Saving function "+dmcObj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Assign markup should be saved", TextMessage , false);
				
				driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
		    }
			
		} catch (Exception e) {
			
			ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Assign markup page Saving function "+dmcObj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Assign markup should be saved", e.toString() , false);
			
		}



	}

}
