package dmc_Setup_PackageSetup;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import dmc_Setup_DataObjects.dmc_Setup_DataObjects_DMCPackageObject;
import setup.com.utill.ExtentReportTemplate;

public class dmc_Setup_PackageSetup_inventoryLoader {
	
	
	 private Map<String, String> propertyset = null;
	   
	   
	   public dmc_Setup_PackageSetup_inventoryLoader(Map<String, String> propertyset) {
		this.propertyset = propertyset;
	}

	

	dmc_Setup_PackageSetup_SetupSupports supportClass=new dmc_Setup_PackageSetup_SetupSupports();

	public void InventoryLoad(dmc_Setup_DataObjects_DMCPackageObject obj, WebDriver driver) throws InterruptedException {

		driver.get(propertyset.get("Hotel.BaseUrl")+"/dmc/setup/DMCPackageAddInventoryRatePage.do?actionType=displayPage");

		driver.findElement(By.id("packageName")).clear();
		driver.findElement(By.id("packageName")).sendKeys(obj.getStandardInfo().getPackageinfo().getPackageName());

		driver.findElement(By.id("packageName_lkup")).click();
		driver.switchTo().defaultContent();
		supportClass.lookupReader(obj.getStandardInfo().getPackageinfo().getPackageName(), driver);


		driver.findElement(By.id("noOfPackageDays")).getText();


		new Select(driver.findElement(By.id("periodFrom_Day_ID"))).selectByVisibleText(obj.getStandardInfo().getPackageinfo().getStayperiodFrom().split("-")[0]);
		new Select(driver.findElement(By.id("periodFrom_Month_ID"))).selectByVisibleText(obj.getStandardInfo().getPackageinfo().getStayperiodFrom().split("-")[1]);
		new Select(driver.findElement(By.id("periodFrom_Year_ID"))).selectByVisibleText(obj.getStandardInfo().getPackageinfo().getStayperiodFrom().split("-")[2]);

		new Select(driver.findElement(By.id("periodTo_Day_ID"))).selectByVisibleText(obj.getStandardInfo().getPackageinfo().getStayPeriodTo().split("-")[0]);
		new Select(driver.findElement(By.id("periodTo_Month_ID"))).selectByVisibleText(obj.getStandardInfo().getPackageinfo().getStayPeriodTo().split("-")[1]);
		new Select(driver.findElement(By.id("periodTo_Year_ID"))).selectByVisibleText(obj.getStandardInfo().getPackageinfo().getStayPeriodTo().split("-")[2]);


		for (int i = 0; i < 7; i++) {

			driver.findElement(By.id("unit_"+i)).clear();
			driver.findElement(By.id("unit_"+i)).sendKeys(obj.getInventory().getTotalunits());

			driver.findElement(By.id("cutoffDays_"+i)).clear();
			driver.findElement(By.id("cutoffDays_"+i)).sendKeys(obj.getInventory().getCutoff());


		}
Thread.sleep(4000);

		((JavascriptExecutor)driver).executeScript("submitData();");
		

		Thread.sleep(4000);
		
		WebDriverWait wait =  new WebDriverWait(driver, 60);
		try {
			
			
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
		    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
		   
		    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
		    	
		    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
				System.out.println(TextMessage);
				
				if(TextMessage.equalsIgnoreCase("Data Saved Successfully") ){
				
				ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Inventory page Saving function "+obj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Inventory markup should be saved", TextMessage , true);
				
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				}
				else {
					
					ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Inventory Saving function "+obj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Inventory markup should be saved", TextMessage , false);
					driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				}
		    }
		    else{
		    	
		    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Inventory Saving function "+obj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Inventory markup should be saved", TextMessage , false);
				
				driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
		    }
			
		} catch (Exception e) {
			
			ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Inventory page Saving function "+obj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Inventory markup should be saved", e.toString() , false);
			
		}

		
		
	}

}
