package dmc_Setup_PackageSetup;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import dmc_Setup_DataObjects.dmc_Setup_DataObjects_DMCPackageObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_Image;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_ItineraryObject;


public class dmc_Setup_PackageSetup_ContentLoader {
		
		 private Map<String, String> propertyset = null;
		   
		   
		   public dmc_Setup_PackageSetup_ContentLoader(Map<String, String> propertyset) {
			this.propertyset = propertyset;
		}
		

	dmc_Setup_PackageSetup_SetupSupports supportClass=new dmc_Setup_PackageSetup_SetupSupports();

	public void itineraryLoader(dmc_Setup_DataObjects_DMCPackageObject dmcObject,WebDriver driver) {

		int dayCount=dmcObject.getStandardInfo().getPackageinfo().getNumberOfPackageDays();

		for (int i = 0; i < dayCount ; i++) {

			driver.findElement(By.id("selectedDays"+(i+1))).click();

			driver.findElement(By.id("itenaryFile"+(i+1)+" ")).clear();
			driver.findElement(By.id("itenaryFile"+(i+1)+" ")).clear();



			driver.findElement(By.id("itenaryTitle_"+(i+1))).clear();
			driver.findElement(By.id("itenaryTitle_"+(i+1))).sendKeys("");


			driver.findElement(By.id("itenaryDescription_"+(i+1))).clear();
			driver.findElement(By.id("itenaryDescription_"+(i+1))).sendKeys("");


			((JavascriptExecutor)driver).executeScript("addItenaryImage("+(i+1)+")");





		}





	}


	public void packageImages(dmc_Setup_DataObjects_DMCPackageObject dmcObject,WebDriver driver) {

	}

	public void contentLoad(WebDriver driver, String content, String savingstring) {

		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");

		driver.switchTo().frame("mce_editor_0");
		System.out.println("content****************"+content);

		driver.findElement(By.tagName("body")).sendKeys(content);
		

	driver.switchTo().defaultContent();
	
		driver.switchTo().frame("texteditor");

		((JavascriptExecutor)driver).executeScript(savingstring);

		driver.switchTo().defaultContent();

		((JavascriptExecutor)driver).executeScript("submitEditorValues();");


		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");









	}

	public void contenttabLoad(dmc_Setup_DataObjects_DMCPackageObject obj,WebDriver driver) throws InterruptedException {

		driver.get(propertyset.get("Hotel.BaseUrl")+"/dmc/setup/DMCPackageContentSetupPage.do");

		driver.findElement(By.id("packageName")).clear();
		driver.findElement(By.id("packageName")).sendKeys(obj.getStandardInfo().getPackageinfo().getPackageName());

		driver.findElement(By.id("packageName_lkup")).click();
		driver.switchTo().defaultContent();
		supportClass.lookupReader(obj.getStandardInfo().getPackageinfo().getPackageName(), driver);

		/*((JavascriptExecutor)driver).executeScript("loadEditorVal('shortDescription','Short Description');");

		contentLoad(driver, obj.getContent().getShortDescription(), "parent.submitSubValue(getEditorValue(),'shortDescription');");

		((JavascriptExecutor)driver).executeScript("submitEditorValues();");


		((JavascriptExecutor)driver).executeScript("closeDialogMsg(dialogMsgBox);");
		 

		((JavascriptExecutor)driver).executeScript("loadEditorVal('packageInclusion','Package Inclusion');");		
		contentLoad(driver, obj.getContent().getPackageInclusion(), "parent.submitSubValue(getEditorValue(),'packageInclusion');");

		((JavascriptExecutor)driver).executeScript("loadEditorVal('packageExclusion','Package Exclusion');");
		contentLoad(driver, obj.getContent().getPackageInclusion(), "parent.submitSubValue(getEditorValue(),'packageExclusion');");

		((JavascriptExecutor)driver).executeScript("loadEditorVal('destinationDetails','Destination Details');");
		contentLoad(driver, obj.getContent().getDestinationDetails(), "parent.submitSubValue(getEditorValue(),'destinationDetails');");

		((JavascriptExecutor)driver).executeScript("loadEditorVal('specialNotes','Special Notes');");
		contentLoad(driver, obj.getContent().getSpecialNotes(), "parent.submitSubValue(getEditorValue(),'specialNotes');");

		((JavascriptExecutor)driver).executeScript("loadEditorVal('contactDetailsOnArrival','Contact Details on Arrival');");
		contentLoad(driver, obj.getContent().getContactDetailsonArrival(), "parent.submitSubValue(getEditorValue(),'contactDetailsOnArrival');");

		((JavascriptExecutor)driver).executeScript("loadEditorVal('termsAndConditions','Terms and Conditions');");
		contentLoad(driver, obj.getContent().getTermsandConditions(), "parent.submitSubValue(getEditorValue(),'termsAndConditions');");
*/

		/*driver.findElement(By.id("currencyCode")).clear();
		driver.findElement(By.id("currencyCode")).sendKeys(obj.getContent().getPackageCurrencyCode());*/
		/* 
		try {

			driver.findElement(By.id("airSectorPrice")).clear();
			driver.findElement(By.id("airSectorPrice")).sendKeys(obj.getContent().getAirSectorPrice());



		} catch (Exception e) {
			// TODO: handle exception
		}

		try {

			driver.findElement(By.id("landSectorPrice")).clear();
			driver.findElement(By.id("landSectorPrice")).sendKeys(obj.getContent().getLandSectorPrice());



		} catch (Exception e) {
			// TODO: handle exception
		}*/


	//	int imagesize=obj.getContent().getImagelist().size();

		/*for (int i = 0; i < imagesize; i++) {

			dmc_Setup_DataObjects_Image image=new dmc_Setup_DataObjects_Image();
			image=obj.getContent().getImagelist().get(i);

			
			driver.switchTo().defaultContent();
			((JavascriptExecutor)driver).executeScript("addImageInfo()");

			driver.switchTo().defaultContent();
			driver.switchTo().frame("dialogwindow");

			driver.findElement(By.id("thumbnailimage_"+image.getThumbnailImage())).click();
			
			System.out.println(image.getImageFile());

			driver.findElement(By.id("theFile")).sendKeys(image.getImageFile());

			try {
				driver.findElement(By.id("caption")).clear();
				driver.findElement(By.id("caption")).sendKeys(image.getImageCaption());

			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			try {
				driver.findElement(By.id("displayorder")).clear();
				driver.findElement(By.id("displayorder")).sendKeys(image.getDisplayorder());

			} catch (Exception e) {
				// TODO: handle exception
			}
			
			driver.findElement(By.id("saveButId")).click();
			
			((JavascriptExecutor)driver).executeScript("close_reload();");
			
			driver.switchTo().defaultContent();

		}*/

		int itineraryDays=obj.getContent().getItineraryDayWise().size();


		for (int i = 0; i <3 /*itineraryDays*/; i++) {
			
			System.out.println("i="+i);

			dmc_Setup_DataObjects_ItineraryObject dayItinerary=new dmc_Setup_DataObjects_ItineraryObject();
			dayItinerary=obj.getContent().getItineraryDayWise().get(i);
			driver.findElement(By.id("selectedDays"+(dayItinerary.getDay()))).click();
			Thread.sleep(5000);
			driver.findElement(By.id("ItenaryImageGrid_"+dayItinerary.getDay())).findElement(By.id("itenaryFile"+dayItinerary.getDay())).sendKeys("E:\\Display Image\\Spices_Restaurant.jpg");

			System.out.println(dayItinerary.getTitle());
			driver.findElement(By.id("itenaryTitle_"+(dayItinerary.getDay()))).sendKeys(dayItinerary.getTitle());
			String itineraryText=dayItinerary.getDescription();
			
			String[] itineraryArray=itineraryText.split("Linebreak");
			
			itineraryText=itineraryText.replace("Linebreak", "");
			System.out.println(itineraryText);
			//driver.findElement(By.id("itenaryDescription_"+(dayItinerary.getDay()))).sendKeys("123");
			driver.findElement(By.id("itenaryDescription_"+(dayItinerary.getDay()))).sendKeys(itineraryText);
			

			/*
			for (int j = 0; j < itineraryArray.length; j++) {
				System.out.println(itineraryArray[j]);
				driver.findElement(By.id("itenaryDescription_"+(dayItinerary.getDay()))).sendKeys(itineraryArray[j]);
				driver.findElement(By.id("itenaryDescription_"+(dayItinerary.getDay()))).sendKeys(Keys.chord(Keys.SHIFT, Keys.ENTER));
			}*/
/*			driver.findElement(By.id("itenaryFile"+dayItinerary.getDay()+" ")).sendKeys(dayItinerary.getImage());
*/

			((JavascriptExecutor)driver).executeScript("addItenaryImage("+dayItinerary.getDay()+")");
		//	driver.findElements(By.className("selbut1")).get(1).click();
		}



		//((JavascriptExecutor)driver).executeScript("submitData();");
	}



}
