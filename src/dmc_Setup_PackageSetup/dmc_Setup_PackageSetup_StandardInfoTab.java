package dmc_Setup_PackageSetup;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import dmc_Setup_DataObjects.dmc_Setup_DataObjects_DMCPackageObject;
import setup.com.utill.ExtentReportTemplate;

public class dmc_Setup_PackageSetup_StandardInfoTab {

   private Map<String, String> propertyset = null;
   
   
   public dmc_Setup_PackageSetup_StandardInfoTab(Map<String, String> propertyset) {
	this.propertyset = propertyset;
}

	public void standardInfoFill(WebDriver driver, dmc_Setup_DataObjects_DMCPackageObject dmcObj) throws InterruptedException {

		dmc_Setup_PackageSetup_SetupSupports supportClass=new dmc_Setup_PackageSetup_SetupSupports();


		driver.get(propertyset.get("Hotel.BaseUrl")+"/dmc/setup/DMCPackageStandardInfoSetupPage.do?actionType=resetFormData&screenAction=create");
		System.out.println("dmcObj.getStandardInfo().getPackageinfo().getPackageName()"+dmcObj.getStandardInfo().getPackageinfo().getPackageName());
		driver.findElement(By.id("packageName")).sendKeys(dmcObj.getStandardInfo().getPackageinfo().getPackageName());
		new Select(driver.findElement(By.id("pkg_name_slect"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getPackageType());
		driver.findElement(By.id("noOfDays")).clear();
		driver.findElement(By.id("noOfDays")).sendKeys(String.valueOf(dmcObj.getStandardInfo().getPackageinfo().getNumberOfPackageDays()));

		driver.findElement(By.id("applicableBookingChannel")).click();
		driver.findElement(By.id("applicableBookingChannelReservation")).click();

		if (dmcObj.getStandardInfo().getPackageinfo().getProductIncludes().contains("Air+Land")) {

			driver.findElement(By.id("element_td_components_FL")).click();

		}else {
			driver.findElement(By.id("element_td_components_L")).click();
		}


		driver.findElement(By.id("childAllowed_Y")).click();
		driver.findElement(By.id("currencyCode")).sendKeys(dmcObj.getStandardInfo().getPackageinfo().getCurrencyCode());
//try {
//	
//
//
//		driver.findElement(By.id("supplierName")).sendKeys(dmcObj.getStandardInfo().getPackageinfo().getSupplierName());
//		driver.findElement(By.id("supplierName_lkup")).click();
//		driver.switchTo().defaultContent();
//		driver.switchTo().frame("lookup");
//
//
//		driver.findElement(By.className("rezgLook0")).click();
//
//} catch (Exception e) {
//	// TODO: handle exception
//}
		driver.switchTo().defaultContent();
		

		//--------------Stay period

		/*new Select(driver.findElement(By.id("utilizeFromDate_Day_ID"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getStayperiodFrom().split("-")[0]);
		new Select(driver.findElement(By.id("utilizeFromDate_Month_ID"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getStayperiodFrom().split("-")[1]);
		new Select(driver.findElement(By.id("utilizeFromDate_Year_ID"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getStayperiodFrom().split("-")[2]);
*/
		new Select(driver.findElement(By.id("utilizeToDate_Day_ID"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getStayPeriodTo().split("-")[0]);
		new Select(driver.findElement(By.id("utilizeToDate_Month_ID"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getStayPeriodTo().split("-")[1]);
		new Select(driver.findElement(By.id("utilizeToDate_Year_ID"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getStayPeriodTo().split("-")[2]);

		/*new Select(driver.findElement(By.id("bookingFromDate_Day_ID"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getBookingPeriodFrom().split("-")[0]);
		new Select(driver.findElement(By.id("bookingFromDate_Month_ID"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getBookingPeriodFrom().split("-")[1]);
		new Select(driver.findElement(By.id("bookingFromDate_Year_ID"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getBookingPeriodFrom().split("-")[2]);
*/
		new Select(driver.findElement(By.id("bookingToDate_Day_ID"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getBookingPeriodTo().split("-")[0]);
		new Select(driver.findElement(By.id("bookingToDate_Month_ID"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getBookingPeriodTo().split("-")[1]);
		new Select(driver.findElement(By.id("bookingToDate_Year_ID"))).selectByVisibleText(dmcObj.getStandardInfo().getPackageinfo().getBookingPeriodTo().split("-")[2]);



		driver.findElement(By.id("add_destination_info")).click();

		driver.switchTo().frame("dialogwindow");

		driver.findElement(By.id("countryName")).clear();
		driver.findElement(By.id("countryName")).sendKeys(dmcObj.getStandardInfo().getPackageinfo().getCountry());


		driver.findElement(By.id("countryName_lkup")).click();
		driver.switchTo().defaultContent();

		supportClass.lookupReader(dmcObj.getStandardInfo().getPackageinfo().getCountry(), driver);
		driver.switchTo().frame("dialogwindow");

		driver.findElement(By.id("cityName")).clear();
		driver.findElement(By.id("cityName")).sendKeys(dmcObj.getStandardInfo().getPackageinfo().getCity());


		driver.findElement(By.id("cityName_lkup")).click();
		driver.switchTo().defaultContent();
		supportClass.lookupReader(dmcObj.getStandardInfo().getPackageinfo().getCity(), driver);
		driver.switchTo().frame("dialogwindow");

		((JavascriptExecutor)driver).executeScript("saveData()");




		driver.switchTo().defaultContent();

		int cancellationpolicysize=dmcObj.getStandardInfo().getCxlList().size();

		for (int i = 0; i < cancellationpolicysize ; i++) {

			driver.findElement(By.id("add_can_policy")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("dialogwindow");

			String periodTo=dmcObj.getStandardInfo().getCxlList().get(i).getToDate();
			String periodFrom=dmcObj.getStandardInfo().getCxlList().get(i).getFromDate();
			
			System.out.println("Cxl to date"+periodTo);

		/*	new Select(driver.findElement(By.id("fromDate_Day_ID"))).selectByVisibleText(periodFrom.split("-")[0]);
			new Select(driver.findElement(By.id("fromDate_Month_ID"))).selectByVisibleText(periodFrom.split("-")[1]);
			new Select(driver.findElement(By.id("fromDate_Year_ID"))).selectByVisibleText(periodFrom.split("-")[2]);
*/
			new Select(driver.findElement(By.id("toDate_Day_ID"))).selectByVisibleText(periodTo.split("-")[0]);
			new Select(driver.findElement(By.id("toDate_Month_ID"))).selectByVisibleText(periodTo.split("-")[1]);
			new Select(driver.findElement(By.id("toDate_Year_ID"))).selectByVisibleText(periodTo.split("-")[2]);

			driver.findElement(By.id("isNonRefundable_"+dmcObj.getStandardInfo().getCxlList().get(i).getPackageRefundable())).click();

			if (dmcObj.getStandardInfo().getCxlList().get(i).getPackageRefundable().contains("N"))
				
			{
				driver.findElement(By.id("standardCancellationBasedOn_"+dmcObj.getStandardInfo().getCxlList().get(i).getBasedon())).click();
				if (dmcObj.getStandardInfo().getCxlList().get(i).getBasedon().contains("R")) {
					
					driver.findElement(By.id("resInSc")).sendKeys("80");
					
				}else {
					driver.findElement(By.id("valueInSc")).sendKeys("80");
				}
				
				driver.findElement(By.id("noShowCancellationBasedOn_R")).click();
				driver.findElement(By.id("resInNc")).sendKeys("100");
				
			}
			((JavascriptExecutor)driver).executeScript("saveDataCnPlcy()");
			driver.switchTo().defaultContent();
		}

Thread.sleep(4000);

		driver.findElement(By.id("add_contact_details")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("dialogwindow");

		driver.findElement(By.id("contactname")).sendKeys(dmcObj.getStandardInfo().getContactInfo().getContactName());
		driver.findElement(By.id("email")).sendKeys(dmcObj.getStandardInfo().getContactInfo().getEmail());
		Thread.sleep(4000);
	//	driver.findElement(By.id("confirmationtype_Email")).click();
		
		new Select(driver.findElement(By.id("contact_type_select"))).selectByVisibleText("Reservations");
		Thread.sleep(4000);
		((JavascriptExecutor)driver).executeScript("saveData()");

		driver.switchTo().defaultContent();
		
		
		((JavascriptExecutor)driver).executeScript("beforeSave();");
		
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.switchTo().defaultContent();
		
		try {
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
		    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
		   
		    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
		    	
		    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
				System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("DMC Standard info page Saving function "+dmcObj.getStandardInfo().getPackageinfo().getPackageName(), "DMC Standard info page should be saved", TextMessage , true);
				
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
		    }
		    else{
		    	
		    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("DMC Standard info page Saving function "+dmcObj.getStandardInfo().getPackageinfo().getPackageName(), "DMC Standard info page should be saved", TextMessage , false);
				
				driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
		    }
			
		} catch (Exception e) {
			
			ExtentReportTemplate.getInstance().addtoReport("DMC Standard info page Saving function "+dmcObj.getStandardInfo().getPackageinfo().getPackageName(), "DMC Standard info page should be saved", e.toString() , false);
			
		}
		
		
		Thread.sleep(5000);


	}



}
