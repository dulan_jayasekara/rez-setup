package dmc_Setup_PackageSetup;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


import dmc_Setup_DataObjects.dmc_Setup_DataObjects_DMCPackageObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_PaxGroupObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_PaxOccupancyObject;
import setup.com.utill.ExtentReportTemplate;

public class dmc_Setup_PackageSetup_ContractLoader{
	
	 private Map<String, String> propertyset = null;
	 
	   public dmc_Setup_PackageSetup_ContractLoader(Map<String, String> propertyset) {
		this.propertyset = propertyset;
	}
	
	dmc_Setup_PackageSetup_SetupSupports supportClass=new dmc_Setup_PackageSetup_SetupSupports();

	public void contractinfoLoader(WebDriver driver,dmc_Setup_DataObjects_DMCPackageObject obj) throws InterruptedException {

		driver.get(propertyset.get("Hotel.BaseUrl")+"/dmc/setup/DMCPackageContractSetupActionPage.do");

		driver.findElement(By.id("packageName")).clear();
		driver.findElement(By.id("packageName")).sendKeys(obj.getStandardInfo().getPackageinfo().getPackageName());

		driver.findElement(By.id("packageName_lkup")).click();
		driver.switchTo().defaultContent();
		supportClass.lookupReader(obj.getStandardInfo().getPackageinfo().getPackageName(), driver);


		driver.findElement(By.id("noOfPackageDays")).getText();



		// Adding pax occupancy
		int numberofpaxOccupancy=obj.getContract().getPaxOccupancylist().size();

		for (int i = 0; i < numberofpaxOccupancy; i++) {


			dmc_Setup_DataObjects_PaxOccupancyObject paxOcc=new dmc_Setup_DataObjects_PaxOccupancyObject();
			paxOcc=obj.getContract().getPaxOccupancylist().get(i);

			addingPaxoccupancy(paxOcc, driver);

		}




		// add the pax group

		int numberofpaxGroup=obj.getContract().getPaxGroupList().size();


		for (int i = 0; i < numberofpaxGroup; i++) {


			dmc_Setup_DataObjects_PaxGroupObject paxgroup=new dmc_Setup_DataObjects_PaxGroupObject();
			paxgroup=obj.getContract().getPaxGroupList().get(i);

			addingPaxGroup(paxgroup, driver);

		}



		

		driver.findElement(By.id("inventoryType_"+obj.getContract().getInventoryType())).click();
		driver.findElement(By.id("searchSatisfied_"+obj.getContract().getSearchSatisfiedweb())).click();
		driver.findElement(By.id("searchSatisfiedCall_"+obj.getContract().getSearchSatisfiedCC())).click();

		driver.findElement(By.id("inventoryExhuasted_"+obj.getContract().getInventoryExhaustedweb())).click();
		driver.findElement(By.id("inventoryExhuastedCall_"+obj.getContract().getInventoryExhaustedCC())).click();
		driver.findElement(By.id("cutOff_"+obj.getContract().getCutoffAppliedweb())).click();
		driver.findElement(By.id("cutOffCall_"+obj.getContract().getCutoffAppliedCC())).click();
		driver.findElement(By.id("blackout_"+obj.getContract().getBlackoutweb())).click();
		driver.findElement(By.id("blackoutCall_"+obj.getContract().getBlackoutCC())).click();
		
		
		Thread.sleep(4000);

		
		((JavascriptExecutor)driver).executeScript("saveContractData();;");
		
		Thread.sleep(4000);
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		
		try {
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
		    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
		   
		    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
		    	
		    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
				System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Contract page Saving function "+obj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Contract should be saved", TextMessage , true);
				
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
		    }
		    else{
		    	
		    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Contract Saving function "+obj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Contract page should be saved", TextMessage , false);
				
				driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
		    }
			
		} catch (Exception e) {
			
			ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Contract page Saving function "+obj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Contract should be saved", e.toString() , false);
			
		}


	}


	public void addingPaxoccupancy(dmc_Setup_DataObjects_PaxOccupancyObject obj,WebDriver driver) {

		driver.findElements(By.id("add_origin_info")).get(0).click();

		driver.switchTo().defaultContent();
		driver.switchTo().frame("dialogwindow");

		new Select(driver.findElement(By.id("room_type_slect"))).selectByVisibleText(obj.getRoomType());
		driver.findElement(By.id("standardAdult")).sendKeys(obj.getNoOfAdults());
		driver.findElement(By.id("children")).sendKeys(obj.getNoOfChildren());


		((JavascriptExecutor)driver).executeScript("saveData()");
		
	WebDriverWait wait = new WebDriverWait(driver, 120);
	
	driver.switchTo().defaultContent();
		
		try {
			
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgBox_msgDisplayArea")));
			boolean isMessageAvail = driver.findElement(By.id("dialogMsgBox_msgDisplayArea")).isDisplayed();
		    System.out.println(isMessageAvail);
		   
		    if(isMessageAvail){
		    	
		    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
				System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("addingPaxoccupancy function ", "addingPaxoccupancy should be saved", TextMessage , false);
				
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				driver.switchTo().frame("dialogwindow");
				driver.findElement(By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img")).click();
				driver.switchTo().defaultContent();
				////*[@id="dialogwindowdragdrop"]/td[3]/a/img
		    }
		    else{
		    	
		    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("addingPaxoccupancy function ", "addingPaxoccupancy should be saved", TextMessage , true);
				
				driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				driver.switchTo().frame("dialogwindow");
				driver.findElement(By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img")).click();
				driver.switchTo().defaultContent();
				
		    }
			
		} catch (Exception e) {
			
			ExtentReportTemplate.getInstance().addtoReport("addingPaxoccupancy function ", "addingPaxoccupancy should be saved", e.toString() , false);
			
			
		}

		

		//driver.switchTo().defaultContent();


	}

	public void addingPaxGroup(dmc_Setup_DataObjects_PaxGroupObject obj,WebDriver driver) {


		driver.findElements(By.id("add_origin_info")).get(1).click();

		driver.switchTo().defaultContent();
		driver.switchTo().frame("dialogwindow");

		driver.findElement(By.id("maxPaxCount")).sendKeys(obj.getMaxNoOfPax());
		System.out.println("obj.getMaxNoOfPax()"+obj.getMaxNoOfPax());
		driver.findElement(By.id("minPaxCount")).sendKeys(obj.getMinNoOfPax());
		System.out.println("obj.getMinNoOfPax()"+obj.getMinNoOfPax());


		((JavascriptExecutor)driver).executeScript("saveData()");

		driver.switchTo().defaultContent();


	}

}
