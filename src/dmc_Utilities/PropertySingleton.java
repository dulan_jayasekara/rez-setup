package dmc_Utilities;

import java.io.IOException;
import java.util.HashMap;

public class PropertySingleton {
	


	private static PropertySingleton SingletonInstance = null;
	private HashMap<String, String>  propertyList      = null;
	
	private PropertySingleton() throws Exception {

		try {
			propertyList = ReadProperties.readpropeties(PathConfigurator.PROPERTY_FILE_PATH);
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Properties not loaded");
		}
	}


	public static PropertySingleton getInstance() throws Exception {
		if (SingletonInstance == null) {
			synchronized (PropertySingleton.class) {
				SingletonInstance = new PropertySingleton();
			}
		}

		return SingletonInstance;
	}
	
	
	public String getProperty(String property){
		System.out.println(property+" --->"+propertyList.get(property));
		return propertyList.get(property);
		
	}



}
