package setup.com.dataObjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import setup.com.readers.ReadExcel;



public class assignAir {
	
	public String packageName;
	public String originAir;
	public String DestinationAir;
	public String CurrencyCode;
	public String AdultFare;
	public String AdultTax;
	public String childFare;
	public String childTax;
	
	
	
	
	
	public String getOriginAir() {
		return originAir;
	}

	public void setOriginAir(String originAir) {
		this.originAir = originAir;
	}

	public String getDestinationAir() {
		return DestinationAir;
	}

	public void setDestinationAir(String destinationAir) {
		DestinationAir = destinationAir;
	}

	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	public String getAdultFare() {
		return AdultFare;
	}

	public void setAdultFare(String adultFare) {
		AdultFare = adultFare;
	}

	public String getAdultTax() {
		return AdultTax;
	}

	public void setAdultTax(String adultTax) {
		AdultTax = adultTax;
	}

	public String getChildFare() {
		return childFare;
	}

	public void setChildFare(String childFare) {
		this.childFare = childFare;
	}

	public String getChildTax() {
		return childTax;
	}

	public void setChildTax(String childTax) {
		this.childTax = childTax;
	}

	public Map<String, assignAir> readinfo(String excelPath,String PackageName)throws Exception{
		
		
	
		
		ReadExcel reader=new ReadExcel();
		Map<String, assignAir>AirMap=new HashMap<String, assignAir>();
		ArrayList<Map<Integer, String>> list1=reader.init(excelPath);
		Map<Integer, String> loginDetails=list1.get(1);
		
		Iterator<Map.Entry<Integer, String>> it = loginDetails.entrySet().iterator();
		while(it.hasNext())
		{
			
		
		String[] allvalues=it.next().getValue().split(",");
		/*obj.airInfo(allvalues, PackageName);*/
		if (allvalues[0].trim().contains(PackageName.trim())) {
			assignAir air=new assignAir();
			
			air.packageName=allvalues[0];
			air.originAir=allvalues[1];
			air.DestinationAir=allvalues[2];
			air.CurrencyCode=allvalues[3];
			air.AdultFare=allvalues[4];
			air.AdultTax=allvalues[5];
			air.childFare=allvalues[6];
			air.childTax=allvalues[7];
			
		AirMap.put(air.originAir, air);
			
		}
		/*if (obj.packageName.contains(PackageName)) {
			air=obj;
		}*/
		
		}
		
		
		return AirMap;
		
		
		
	}
	
	public assignAir airInfo(String[] airValues, String PackageName) {
		
		assignAir obj = new assignAir();
		
			packageName=airValues[0];
			originAir=airValues[1];
			DestinationAir=airValues[2];
			CurrencyCode=airValues[3];
			AdultFare=airValues[4];
			AdultTax=airValues[5];
			childFare=airValues[6];
			childTax=airValues[7];
			
			System.out.println("airInfo"+CurrencyCode);
			
			
			
				
			
		
		return obj;
		
		
		
	}
	
	public static void main(String[] args) throws Exception{
		
		assignAir air=new assignAir();
		
		Map<String, assignAir> airmap=new HashMap<String, assignAir>();
		airmap=air.readinfo("MultiRegionOriginTest.xls","Multi Region Dates");
		
		for (Entry<String, assignAir> entryval:airmap.entrySet()) {
			
			System.out.println(entryval.getValue().getAdultFare());
			System.out.println(entryval.getValue().getAdultTax());
			System.out.println(entryval.getValue().getChildFare());
			System.out.println(entryval.getValue().getChildTax());

		}
		//System.out.println("SSSSSS"+air.CurrencyCode);
		//System.out.println("LLLLLL"+air.getAdultFare());
		
	}

}
