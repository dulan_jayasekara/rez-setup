package setup.com.dataObjects;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import setup.com.readers.ReadExcel;

public class RateObjectSet {
	
	
	public RatesObject readRates(String[] AllValues) throws ParseException {
		
		
			
			RatesObject obj=new RatesObject();
			
			
			obj.setContractFromDate(AllValues[3]);
			obj.setContractToDate(AllValues[4]);
			obj.setStdRate(AllValues[5]);
			obj.setAddRate(AllValues[6]);
			obj.setChildRate(AllValues[7]);
			
			
			
			return obj;
			
			
			
			
			
			
		
		
		
		
	}
	

	
	public Map<String, RegionRates> contractsRead(String excelPath,String HotelName,String RoomType) throws ParseException {
		System.out.println("HotelName"+HotelName);
		System.out.println("RoomType"+RoomType);
		ReadExcel reader=new ReadExcel();
		ArrayList<Map<Integer, String>> list1=reader.init(excelPath);
		Map<Integer, String> loginDetails=list1.get(6);
		

		Iterator<Map.Entry<Integer, String>> it = loginDetails.entrySet().iterator();
		//int a=0;
		
		String regiontext = "";
		RegionRates regionrate=new RegionRates();
		Map<String, RegionRates> regionlist=new HashMap<String, RegionRates>();

		while(it.hasNext())
		{
			String[]   AllValues     = it.next().getValue().split(",");
			
			System.out.println(AllValues[0]+"KKKKKKKKKKKKKK");
			System.out.println(AllValues[1]+"aaaaaaaaaaaaa");
			
			if ((AllValues[0].trim().contains(HotelName)) ) {
				
				System.out.println("HotelsMatch");
				
				if (RoomType.trim().contains(AllValues[1])) {
					
					System.out.println("roomsMatch");

				
				System.out.println("AllValues[0]"+AllValues[0]);
				System.out.println("AllValues[1]"+AllValues[1]);
				
				
				
				if (regiontext.contains(AllValues[2])) {
					
					System.out.println("regionrate"+regionrate.getRegion());
					List<RatesObject> ratelist=new ArrayList<RatesObject>();
					ratelist=regionrate.getRatelist();
					ratelist.add(readRates(AllValues));
					regionrate.setRatelist(ratelist);
					
				}else {
					regiontext=AllValues[2];
				System.out.println("new");
					RegionRates regionratenew=new RegionRates();
					
					List<RatesObject> ratelist=new ArrayList<RatesObject>();
					
					regionratenew.setRegion(AllValues[2]);
					ratelist.add(readRates(AllValues));                                
					regionratenew.setRatelist(ratelist);
					regionrate=regionratenew;
					
				}
				
				regionlist.put(regiontext, regionrate);
				
				}
				
			}
			
		
			
			
			
			
		}
		
		return regionlist;
		
	}
	
	public static void main(String[] args) throws ParseException {
		
		RateObjectSet rate=new RateObjectSet();
		Map<String, RegionRates> ratemap=new HashMap<String, RegionRates>();
		ratemap=rate.contractsRead("MultiRegionOriginTest234.xls","Hotel Adlon Kempinski Berlin","Double");
		
		for (Entry<String, RegionRates> entryval:ratemap.entrySet()) {
			
			System.out.println(entryval.getKey());
			
			for (int i = 0; i < entryval.getValue().getRatelist().size(); i++) {
				System.out.println(entryval.getValue().getRatelist().get(i).getStdRate());
				
				
			}
			
		}
		
		
		
	}

}
