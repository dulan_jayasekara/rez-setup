package setup.com.dataObjects;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import setup.com.readers.ReadExcel;

public class assignActivity {



	public List<ActivityDetails> readinfo(String excelPath, String PackageName) throws ParseException {

		List<ActivityDetails> activitylist=new ArrayList<ActivityDetails>();		
		
		activitylist=assignInfo(excelPath, PackageName);

				
			

		return activitylist;
	}


	public List<ActivityDetails> assignInfo(String excelPath, String PackageName) throws ParseException {


        RateObjectSetActivity regionrateActivity=new RateObjectSetActivity();

		List<ActivityDetails> activitylist=new ArrayList<ActivityDetails>();		
		ReadExcel reader=new ReadExcel();
		ArrayList<Map<Integer, String>> list1=reader.init(excelPath);
		Map<Integer, String> ActivityDetails=list1.get(3);

		Iterator<Map.Entry<Integer, String>> it = ActivityDetails.entrySet().iterator();
	
		while (it.hasNext()) {

			String[]   AllValues     = it.next().getValue().split(",");
			
			if (AllValues[0].equalsIgnoreCase(PackageName)) {
				
				ActivityDetails obj=new ActivityDetails();


				obj.setDay(AllValues[1]);
				obj.setProgram(AllValues[2]);
				obj.setActivityName(AllValues[3]);
				obj.setPeriod(AllValues[4]);
				obj.setRateplan(AllValues[5]);
				obj.setSalesTax(AllValues[6]);
				obj.setMiscellaneousTax(AllValues[7]);
				
				obj.setRegionRatemap(regionrateActivity.contractsReadActivity(excelPath, obj.getProgram(), obj.getActivityName()));
			    activitylist.add(obj);
			
			
			}
		}

		


		return activitylist;

	}

	public static void main(String[] args) throws ParseException {

		assignActivity activity=new assignActivity();
		List<ActivityDetails> list=activity.readinfo("MultiRegionOriginTest234.xls","Multi Region Dates");
		System.out.println(list.get(0).getDay());
	}

}
