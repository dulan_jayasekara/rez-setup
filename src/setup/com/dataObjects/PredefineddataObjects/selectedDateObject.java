package setup.com.dataObjects.PredefineddataObjects;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class selectedDateObject {

	Date toDate;
	Date fromDate;
	Map<String, PredefRateObject> ratesmap=new HashMap<String, PredefRateObject>();
	
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Map<String, PredefRateObject> getRatesmap() {
		return ratesmap;
	}
	public void setRatesmap(Map<String, PredefRateObject> ratesmap) {
		this.ratesmap = ratesmap;
	}
	
	
}
