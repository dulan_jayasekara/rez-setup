package setup.com.dataObjects.PredefineddataObjects;

public class PredefRateObject {
	
	int air;
	int hotel;
	int program;
	int totaltax;
	int finalRate;
	int agentCommission;
	int affCommission;
	
	
	public int getAir() {
		return air;
	}
	public void setAir(String air) {
		this.air = Integer.parseInt(air);
	}
	public int getHotel() {
		return hotel;
	}
	public void setHotel(String hotel) {
		this.hotel = Integer.parseInt(hotel);
	}
	public int getProgram() {
		return program;
	}
	public void setProgram(String program) {
		this.program = Integer.parseInt(program);
	}
	public int getTotaltax() {
		return totaltax;
	}
	public void setTotaltax(String totaltax) {
		this.totaltax = Integer.parseInt(totaltax);
	}
	public int getFinalRate() {
		return finalRate;
	}
	public void setFinalRate(String finalRate) {
		this.finalRate = Integer.parseInt(finalRate);
	}
	public int getAgentCommission() {
		return agentCommission;
	}
	public void setAgentCommission(String agentCommission) {
		this.agentCommission = Integer.parseInt(agentCommission);
	}
	public int getAffCommission() {
		return affCommission;
	}
	public void setAffCommission(String affCommission) {
		this.affCommission = Integer.parseInt(affCommission);
	}
	
	

}
