package setup.com.dataObjects;

import java.util.HashMap;
import java.util.Map;

public class ActivityDetails {
	
	String Day;
	 String Program;
	 String ActivityName;
	 String Period;
	 String Rateplan;
	 
	 String ActivityAdultRate;
	 String ActivityChildRate;
	 
	 String SalesTax;
	 String MiscellaneousTax;
	 
	 
	 Map<String, RegionRates> regionRatemap=new HashMap<String, RegionRates>();
	 
	 
	 
	 
	public Map<String, RegionRates> getRegionRatemap() {
		return regionRatemap;
	}
	public void setRegionRatemap(Map<String, RegionRates> regionRatemap) {
		this.regionRatemap = regionRatemap;
	}
	public String getPeriod() {
		return Period;
	}
	public void setPeriod(String period) {
		Period = period;
	}
	public String getRateplan() {
		return Rateplan;
	}
	public void setRateplan(String rateplan) {
		Rateplan = rateplan;
	}
	public String getDay() {
		return Day;
	}
	public void setDay(String day) {
		Day = day;
	}
	public String getProgram() {
		return Program;
	}
	public void setProgram(String program) {
		Program = program;
	}
	
	public String getActivityName() {
		return ActivityName;
	}
	public void setActivityName(String activityName) {
		ActivityName = activityName;
	}
	public String getActivityAdultRate() {
		return ActivityAdultRate;
	}
	public void setActivityAdultRate(String activityAdultRate) {
		ActivityAdultRate = activityAdultRate;
	}
	public String getActivityChildRate() {
		return ActivityChildRate;
	}
	public void setActivityChildRate(String activityChildRate) {
		ActivityChildRate = activityChildRate;
	}
	public String getSalesTax() {
		return SalesTax;
	}
	public void setSalesTax(String salesTax) {
		SalesTax = salesTax;
	}
	public String getMiscellaneousTax() {
		return MiscellaneousTax;
	}
	public void setMiscellaneousTax(String miscellaneousTax) {
		MiscellaneousTax = miscellaneousTax;
	}
	
	 
	 

}
