package setup.com.dataObjects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import setup.com.readers.ReadExcel;

public class ContentObjRead {
	
	
	public ContentObj ContentObjReader(String excelPath,String PackageName) {
		
			
			List<ContentObj> contentlist=new ArrayList<ContentObj>();		
			ReadExcel reader=new ReadExcel();
			ArrayList<Map<Integer, String>> list1=reader.init(excelPath);
			Map<Integer, String> contentDetails=list1.get(4);
			ContentObj obj=new ContentObj();
			Iterator<Map.Entry<Integer, String>> it = contentDetails.entrySet().iterator();
			int a=0;
			
			while (it.hasNext()) {
				
				String[]   AllValues     = it.next().getValue().split(",");
				if (AllValues[0].equalsIgnoreCase(PackageName)) {
					
					obj=assicontent(AllValues, PackageName);
					
					
				}
				
			}
		
			return obj;
		}
	
public ContentObj assicontent(String[] contentValues, String PackageName) {
		
		ContentObj obj = new ContentObj();
		/*if (PackageName.equalsIgnoreCase(contentValues[0])) {*/
		
			obj.setPackageName(contentValues[0]);
			obj.setShortDescription(contentValues[1]);
			obj.setPackageinclusions(contentValues[2]);
			obj.setPackageExclusions(contentValues[3]);
			obj.setPackagedescription(contentValues[4]);
			obj.setSpecialNotes(contentValues[5]);
			obj.setContacts(contentValues[6]);
			obj.setTermsandConditions(contentValues[7]);
			obj.setThumbnail(contentValues[8]);
			obj.setImageGallery(contentValues[9]);
			
			
			
			
		/*}*/
		return obj;
		
		
		
	}


public static void main(String []args) {
	
	ContentObjRead read=new ContentObjRead();
	System.out.println("ddddddddddddddddd");
	ContentObj obj=read.ContentObjReader("PackageSetup_Prod.xls","Eclectic London");
	System.out.println(obj.getPackageName());
	System.out.println(obj.getPackageExclusions());
}
		
	

}
