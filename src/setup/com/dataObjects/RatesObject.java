package setup.com.dataObjects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RatesObject {
	
	Date contractFromDate;
	Date contractToDate;
	
	double stdRate;
	double addRate;
	double childRate;
	
	
	
	
	public Date getContractFromDate() {
		return contractFromDate;
	}
	public void setContractFromDate(String contractFromDate) throws ParseException {
		
		
		
		this.contractFromDate = settingDate(contractFromDate);
	}
	public Date getContractToDate() {
		return contractToDate;
	}
	public void setContractToDate(String contractToDate) throws ParseException {
		this.contractToDate = settingDate(contractToDate);
	}
	public double getStdRate() {
		return stdRate;
	}
	public void setStdRate(String stdRate) {
		this.stdRate = Double.parseDouble(stdRate);
	}
	public double getAddRate() {
		return addRate;
	}
	public void setAddRate(String addRate) {
		this.addRate = Double.parseDouble(addRate);
	}
	public double getChildRate() {
		return childRate;
	}
	public void setChildRate(String childRate) {
		this.childRate = Double.parseDouble(childRate);
	}
	
	
	public Date settingDate(String textdate) throws ParseException {
		
		Date dateformatted=new Date();
		
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MMM/yyyy");
		dateformatted=sdf.parse(textdate);
		
		return dateformatted;
		
	}
	

}
