package setup.com.dataObjects;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import setup.com.readers.ReadExcel;

public class RateObjectSetActivity {
	
	
	public RatesObject readRatesactivity(String[] AllValues) throws ParseException {
		
		
		
		RatesObject obj=new RatesObject();
		
		
		obj.setContractFromDate(AllValues[3]);
		obj.setContractToDate(AllValues[4]);
		obj.setStdRate(AllValues[5]);
		
		
		
		return obj;
		
		
		
		
		
		
	
	
	
	
}
	
	public Map<String, RegionRates> contractsReadActivity(String excelPath,String Programmname,String activityType) throws ParseException {
		
		ReadExcel reader=new ReadExcel();
		ArrayList<Map<Integer, String>> list1=reader.init(excelPath);
		Map<Integer, String> loginDetails=list1.get(7);
		

		Iterator<Map.Entry<Integer, String>> it = loginDetails.entrySet().iterator();
		//int a=0;
		
		String regiontext = "";
		RegionRates regionrate=new RegionRates();
		Map<String, RegionRates> regionlist=new HashMap<String, RegionRates>();

		while(it.hasNext())
		{
			String[]   AllValues     = it.next().getValue().split(",");
			
		
			
			if ((AllValues[0].trim().contains(Programmname)) ) {
				
				System.out.println("HotelsMatch");
				
				if (activityType.trim().contains(AllValues[1])) {
					
					System.out.println("roomsMatch");

				
			
				
				
				if (regiontext.contains(AllValues[2])) {
					
					List<RatesObject> ratelist=new ArrayList<RatesObject>();
					ratelist=regionrate.getRatelist();
					ratelist.add(readRatesactivity(AllValues));
					regionrate.setRatelist(ratelist);
					
				}else {
					regiontext=AllValues[2];
				System.out.println("new");
					RegionRates regionratenew=new RegionRates();
					
					List<RatesObject> ratelist=new ArrayList<RatesObject>();
					
					regionratenew.setRegion(AllValues[2]);
					ratelist.add(readRatesactivity(AllValues));                                
					regionratenew.setRatelist(ratelist);
					regionrate=regionratenew;
					
				}
				
				regionlist.put(regiontext, regionrate);
				
				}
				
			}
			
		
			
			
			
			
		}
		
		return regionlist;
		
	}

}
