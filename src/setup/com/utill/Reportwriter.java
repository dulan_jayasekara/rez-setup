package setup.com.utill;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Reportwriter {

	
	public String reportwrite() {
		
		DateFormat date=new SimpleDateFormat("dd-MMM-yyyy");
		Date todate=Calendar.getInstance().getTime();
		String Dateinformat=date.format(todate);
		
		 File Location = new File(Dateinformat+"/");

		  // if the directory does not exist, create it
		  if (!Location.exists()) {
		    System.out.println("creating directory: " + Location);
		    boolean result = false;

		    try{
		    	Location.mkdir();
		        result = true;
		     } catch(SecurityException se){
		        //handle it
		     }        
		     if(result) {    
		       System.out.println("DIR created");  
		     }
		  }else{
			  System.out.println("Location Already exists");
		  }
		  
		  String loc=Dateinformat+"\\";
		  return loc;
		
	}
	
	public static void main(String[] args) {
		
		Reportwriter report=new Reportwriter();
		report.reportwrite();
		
		
	}
	
	
}
