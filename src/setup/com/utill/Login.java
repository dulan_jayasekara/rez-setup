package setup.com.utill;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;



public class Login {
	private  WebDriver           driver      = null;
	private  Map<String, String> Propertymap = null;
	
	
	public Login() {
		Propertymap = loadProperties("../Hotel_Setup_Details/Config.properties");
	}
	
	public  WebDriver driverinitialize() throws Exception{
		
		/*File profilepath = new File(ReadProperties.readProperty("firefox.profile.path", "ConfigurationProperties.properties"));
		 FirefoxProfile profile=new FirefoxProfile(profilepath);
		driver = new FirefoxDriver(profile);*/
		
		 
		String chromeFilePath = "E:/Projects/Drivers/chromedriver.exe";
   	    System.setProperty("webdriver.chrome.driver", chromeFilePath);
        driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		if (Propertymap.get("Window.maximized").trim().equalsIgnoreCase("yes"))
		driver.manage().window().maximize();	
		return driver;
		
	}
	
	public Boolean login(WebDriver driver)throws IOException, InterruptedException {
		
		driver.get(Propertymap.get("Hotel.BaseUrl") + "/admin/common/LoginPage.do");
		driver.findElement(By.id("user_id")).clear();
		driver.findElement(By.id("user_id")).sendKeys(Propertymap.get("Hotel.UserName"));
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(Propertymap.get("Hotel.Password"));
		driver.findElement(By.xpath(".//*[@id='loginbutton']/img")).submit();
		
		//Thread.sleep(5000);
		
		
		//driver.findElement(By.id("loginbutton")).click();
		
		
		try {
			driver.findElement(By.id("mainmenurezglogo"));
			return true;
		} catch (Exception e) {
			return false;
		}
		
	
		
	}
	
	public Map<String, String> loadProperties(String filepath) {

		Map<String, String> PropertyMap = new HashMap<String, String>();
		try {
			Properties prop = new Properties();
			FileReader fs = new FileReader(new File(filepath));
			prop.load(fs);

			for (String key : prop.stringPropertyNames()) {
				PropertyMap.put(key, prop.getProperty(key));
			}

		} catch (Exception e) {
			System.out.println("Error With Adding Properties to the Map");
			
		}
		return PropertyMap;
	}
	

}
