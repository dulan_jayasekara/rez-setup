package setup.com.utill;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;



public class Calender {


	public static String getDate(int filed,int increment)
	{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		cal.add(filed,increment);
		
		return sdf.format(cal.getTime());
	}
	
	public static String getDate(int filed,int increment,String pattern)
	{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		cal.add(filed,increment);
		
		return sdf.format(cal.getTime());
	}
	
	
	public static String getDate(int months,int days,String pattern,String ee)
	{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		cal.add(Calendar.MONTH,months);
		cal.add(Calendar.DATE,days);
		
		return sdf.format(cal.getTime());
	}
	
	public static String getDate(String AvailPattern,String NeededPattern,String Date) throws ParseException
	{
		//Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf1 = new SimpleDateFormat(AvailPattern);
		SimpleDateFormat sdf2 = new SimpleDateFormat(NeededPattern);
		return sdf2.format(sdf1.parse(Date));
	}
	
	
	public static String getDate(String AvailPattern,String Date, int Filed , int increment) throws ParseException
	{
		//Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf1 = new SimpleDateFormat(AvailPattern);
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf1.parse(Date));
		cal.add(Filed,increment);
		return sdf1.format(cal.getTime());
	}
	
	public static String getToday(String pattern)
	{
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}



	public String differenceBetweenDays(Date day1, Date day2)
	{
		long diff = day1.getTime() - day2.getTime();
		return String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
	}
}
