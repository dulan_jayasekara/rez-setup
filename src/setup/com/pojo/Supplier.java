package setup.com.pojo;

public class Supplier {
	
	public  String  SupplierName      ="";
	public  String  SupplierType      ="";
	public  String  Address           ="";
	public  String  Country           ="";
	public  String  City              ="";
	public  boolean isActive          =true;
	public  String  Currency          ="";
	public  String  ContactName       ="";
	public  String  Email             ="";
	public  String  ContactMedia      ="";
	public  String  ContactType       ="";
	public  String  SupplierCode      ="";
	
	
	
	
	
	public String getSupplierCode() {
		return SupplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		SupplierCode = supplierCode;
	}
	public String getSupplierName() {
		return SupplierName;
	}
	public void setSupplierName(String supplierName) {
		SupplierName = supplierName;
	}
	public String getSupplierType() {
		return SupplierType;
	}
	public void setSupplierType(String supplierType) {
		SupplierType = supplierType;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(String isActive) {

		if(isActive.equalsIgnoreCase("yes"))
		this.isActive = true;
		else
		this.isActive = false;
	}
	public String getCurrency() {
		return Currency;
	}
	public void setCurrency(String currency) {
		Currency = currency;
	}
	public String getContactName() {
		return ContactName;
	}
	public void setContactName(String contactName) {
		ContactName = contactName;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getContactMedia() {
		return ContactMedia;
	}
	public void setContactMedia(String contactMedia) {
		ContactMedia = contactMedia;
	}
	public String getContactType() {
		return ContactType;
	}
	public void setContactType(String contactType) {
		ContactType = contactType;
	}

	
	

	
	
	
}
