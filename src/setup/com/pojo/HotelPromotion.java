package setup.com.pojo;

import setup.com.enumtypes.BookingChannelType;
import setup.com.enumtypes.DiscountRateApplicableType;
import setup.com.enumtypes.PartnerType;
import setup.com.enumtypes.PromotionApplicablePeriodType;
import setup.com.enumtypes.PromotionBasedOnType;
import setup.com.enumtypes.PromotionSpecialRateApplicableType;
import setup.com.enumtypes.PromotionType;

public class HotelPromotion {

	private String                                  Supp_Name;
	private String                                  Hotel_Name;
	private String                                  Room_Type;
	private String                                  Bed_Type;
	private String                                  Rate_Plan;
	private String                                  Calculation_Logic;
	private PromotionBasedOnType                    Promotion_BasedOn; 
	private String                                  Nights_Book;
	private String                                  Prior_Arrival;
    private PromotionType                           PromotionType; 
    private String                                  FN_FreeNights;
	private String                                  FN_MaxNumberofNights;
	private String                                  FN_Surchage_Fee;
	private PromotionSpecialRateApplicableType      SpecialRateApplicableType;
	private DiscountRateApplicableType              Discountrateapplicabletype;
	private PromotionApplicablePeriodType           ApplicablePeriod;
	private String                                  Promo_Value;
	private String                                  Note;
	private String                                  PromoCode;
	private String                                  Booking_Date_From;
	private String                                  Booking_Date_To;
	private String                                  Stay_Date_From;
	private String                                  Stay_Date_To;
	private BookingChannelType                      Booking_Channel;
	private PartnerType                             PartnerType;
	private boolean                                 Is_Active;
	private boolean                                 Isbestrateguarantee;
	private String                                  AgentRegion;
	private String                                  AgentName;
	
	
	
	public String getAgentRegion() {
		return AgentRegion;
	}
	public void setAgentRegion(String agentRegion) {
		AgentRegion = agentRegion;
	}
	public String getSupp_Name() {
		return Supp_Name;
	}
	public void setSupp_Name(String supp_Name) {
		Supp_Name = supp_Name;
	}
	public String getHotel_Name() {
		return Hotel_Name;
	}
	public void setHotel_Name(String hotel_Name) {
		Hotel_Name = hotel_Name;
	}
	public String getRoom_Type() {
		return Room_Type;
	}
	public void setRoom_Type(String room_Type) {
		Room_Type = room_Type;
	}
	public String getBed_Type() {
		return Bed_Type;
	}
	public void setBed_Type(String bed_Type) {
		Bed_Type = bed_Type;
	}
	public String getRate_Plan() {
		return Rate_Plan;
	}
	public void setRate_Plan(String rate_Plan) {
		Rate_Plan = rate_Plan;
	}
	public String getCalculation_Logic() {
		return Calculation_Logic;
	}
	public void setCalculation_Logic(String calculation_Logic) {
		Calculation_Logic = calculation_Logic;
	}
	public PromotionBasedOnType getPromotion_BasedOn() {
		return Promotion_BasedOn;
	}
	public void setPromotion_BasedOn(PromotionBasedOnType promotion_BasedOn) {
		Promotion_BasedOn = promotion_BasedOn;
	}
	public String getNights_Book() {
		return Nights_Book;
	}
	public void setNights_Book(String nights_Book) {
		Nights_Book = nights_Book;
	}
	public String getPrior_Arrival() {
		return Prior_Arrival;
	}
	public void setPrior_Arrival(String prior_Arrival) {
		Prior_Arrival = prior_Arrival;
	}
	public PromotionType getPromotionType() {
		return PromotionType;
	}
	public void setPromotionType(PromotionType promotionType) {
		PromotionType = promotionType;
	}
	public String getFN_FreeNights() {
		return FN_FreeNights;
	}
	public void setFN_FreeNights(String fN_FreeNights) {
		FN_FreeNights = fN_FreeNights;
	}
	public String getFN_MaxNumberofNights() {
		return FN_MaxNumberofNights;
	}
	public void setFN_MaxNumberofNights(String fN_MaxNumberofNights) {
		FN_MaxNumberofNights = fN_MaxNumberofNights;
	}
	public String getFN_Surchage_Fee() {
		return FN_Surchage_Fee;
	}
	public void setFN_Surchage_Fee(String fN_Surchage_Fee) {
		FN_Surchage_Fee = fN_Surchage_Fee;
	}
	public PromotionSpecialRateApplicableType getSpecialRateApplicableType() {
		return SpecialRateApplicableType;
	}
	public void setSpecialRateApplicableType(
			PromotionSpecialRateApplicableType specialRateApplicableType) {
		SpecialRateApplicableType = specialRateApplicableType;
	}
	public DiscountRateApplicableType getDiscountrateapplicabletype() {
		return Discountrateapplicabletype;
	}
	public void setDiscountrateapplicabletype(
			DiscountRateApplicableType discountrateapplicabletype) {
		Discountrateapplicabletype = discountrateapplicabletype;
	}
	public PromotionApplicablePeriodType getApplicablePeriod() {
		return ApplicablePeriod;
	}
	public void setApplicablePeriod(PromotionApplicablePeriodType applicablePeriod) {
		ApplicablePeriod = applicablePeriod;
	}
	public String getPromo_Value() {
		return Promo_Value;
	}
	public void setPromo_Value(String promo_Value) {
		Promo_Value = promo_Value;
	}
	public String getNote() {
		return Note;
	}
	public void setNote(String note) {
		Note = note;
	}
	public String getPromoCode() {
		return PromoCode;
	}
	public void setPromoCode(String promoCode) {
		PromoCode = promoCode;
	}

	public BookingChannelType getBooking_Channel() {
		return Booking_Channel;
	}


	public String getBooking_Date_From() {
		return Booking_Date_From;
	}
	public void setBooking_Date_From(String booking_Date_From) {
		Booking_Date_From = booking_Date_From;
	}
	public String getBooking_Date_To() {
		return Booking_Date_To;
	}
	public void setBooking_Date_To(String booking_Date_To) {
		Booking_Date_To = booking_Date_To;
	}
	public String getStay_Date_From() {
		return Stay_Date_From;
	}
	public void setStay_Date_From(String stay_Date_From) {
		Stay_Date_From = stay_Date_From;
	}
	public String getStay_Date_To() {
		return Stay_Date_To;
	}
	public void setStay_Date_To(String stay_Date_To) {
		Stay_Date_To = stay_Date_To;
	}
	public void setBooking_Channel(BookingChannelType booking_Channel) {
		Booking_Channel = booking_Channel;
	}
	public PartnerType getPartnerType() {
		return PartnerType;
	}
	public void setPartnerType(PartnerType partnerType) {
		PartnerType = partnerType;
	}
	public boolean isIs_Active() {
		return Is_Active;
	}
	public void setIs_Active(String is_Active) {
		Is_Active = (is_Active.equalsIgnoreCase("Yes"))? true:false;
	}
	public boolean isIsbestrateguarantee() {
		return Isbestrateguarantee;
	}
	public void setIsbestrateguarantee(String isbestrateguarantee) {
		Isbestrateguarantee = (isbestrateguarantee.equalsIgnoreCase("Yes"))? true : false;
	}
	public String getAgentName() {
		return AgentName;
	}
	public void setAgentName(String agentName) {
		AgentName = agentName;
	}

	
	
	
		
}
