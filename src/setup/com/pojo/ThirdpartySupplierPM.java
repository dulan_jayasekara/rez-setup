package setup.com.pojo;

public class ThirdpartySupplierPM {
	
	
	
	
	 public String getBookingChannel() {
		return BookingChannel;
	}
	public void setBookingChannel(String bookingChannel) {
		BookingChannel = bookingChannel;
	}
	public String getPartner() {
		return Partner;
	}
	public void setPartner(String partner) {
		Partner = partner;
	}
	public String getRegion() {
		return Region;
	}
	public void setRegion(String region) {
		Region = region;
	}
	public String getProfitMarkupType() {
		return ProfitMarkupType;
	}
	public void setProfitMarkupType(String profitMarkupType) {
		ProfitMarkupType = profitMarkupType;
	}
	public String getProfitMarkupPeriodFrom() {
		return ProfitMarkupPeriodFrom;
	}
	public void setProfitMarkupPeriodFrom(String profitMarkupPeriodFrom) {
		ProfitMarkupPeriodFrom = profitMarkupPeriodFrom;
	}
	public String getProfitMarkupPeriodTo() {
		return ProfitMarkupPeriodTo;
	}
	public void setProfitMarkupPeriodTo(String profitMarkupPeriodTo) {
		ProfitMarkupPeriodTo = profitMarkupPeriodTo;
	}
	public String getThirdPartySupplier() {
		return ThirdPartySupplier;
	}
	public void setThirdPartySupplier(String thirdPartySupplier) {
		ThirdPartySupplier = thirdPartySupplier;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public boolean isOverwriteSpecificMarkup() {
		return OverwriteSpecificMarkup;
	}
	public void setOverwriteSpecificMarkup(boolean overwriteSpecificMarkup) {
		OverwriteSpecificMarkup = overwriteSpecificMarkup;
	}
	public String getFixProfitMarkupDayplan() {
		return FixProfitMarkupDayplan;
	}
	public void setFixProfitMarkupDayplan(String fixProfitMarkupDayplan) {
		FixProfitMarkupDayplan = fixProfitMarkupDayplan;
	}
	public String getProfitMarkup() {
		return ProfitMarkup;
	}
	public void setProfitMarkup(String profitMarkup) {
		ProfitMarkup = profitMarkup;
	}
	private String   BookingChannel = "";
	 private String  Partner="";
	 private String Region ="";
	 private String ProfitMarkupType="";
	 private String ProfitMarkupPeriodFrom="";
	 private String ProfitMarkupPeriodTo="";
	 private String ThirdPartySupplier="";
	 private String Country="";
	 private String City="";
	 private boolean OverwriteSpecificMarkup=false;
	 private String FixProfitMarkupDayplan = "";
	 private String ProfitMarkup="";


}
