package setup.com.pojo;

public class Contract {
	
	public String ActivityProgramName_Contract;
	
	public String Region;
	public String intentoryObtainedby;
	public String rateContract;
	public String comnComType;
	public String Contract_From;
	public String Contract_To;
	public String InventoryType;
	
	
	public String getIntentoryObtainedby() {
		return intentoryObtainedby;
	}
	public void setIntentoryObtainedby(String intentoryObtainedby) {
		this.intentoryObtainedby = intentoryObtainedby;
	}
	public String getRateContract() {
		return rateContract;
	}
	public void setRateContract(String rateContract) {
		this.rateContract = rateContract;
	}
	public String getComnComType() {
		return comnComType;
	}
	public void setComnComType(String comnComType) {
		this.comnComType = comnComType;
	}
	public String getActivityProgramName_Contract() {
		return ActivityProgramName_Contract;
	}
	public void setActivityProgramName_Contract(String activityProgramName_Contract) {
		ActivityProgramName_Contract = activityProgramName_Contract;
	}
	
	public String getRegion() {
		return Region;
	}
	public void setRegion(String region) {
		Region = region;
	}
	
	public String getContract_From() {
		return Contract_From;
	}
	public void setContract_From(String contract_From) {
		Contract_From = contract_From;
	}
	public String getContract_To() {
		return Contract_To;
	}
	public void setContract_To(String contract_To) {
		Contract_To = contract_To;
	}
	public String getInventoryType() {
		return InventoryType;
	}
	public void setInventoryType(String inventoryType) {
		InventoryType = inventoryType;
	}
	
	

}
