package setup.com.pojo;

public class Tax {
	
	public String ActivityProgramName_Tax;
	public String Taxes_From;
	public String Taxes_To;
	public String Sales_Tax;
	public String Sales_Tax_Value;
	public String Mis_Tax;
	public String Mis_Tax_Value;
	public String getActivityProgramName_Tax() {
		return ActivityProgramName_Tax;
	}
	public void setActivityProgramName_Tax(String activityProgramName_Tax) {
		ActivityProgramName_Tax = activityProgramName_Tax;
	}
	public String getTaxes_From() {
		return Taxes_From;
	}
	public void setTaxes_From(String taxes_From) {
		Taxes_From = taxes_From;
	}
	public String getTaxes_To() {
		return Taxes_To;
	}
	public void setTaxes_To(String taxes_To) {
		Taxes_To = taxes_To;
	}
	public String getSales_Tax() {
		return Sales_Tax;
	}
	public void setSales_Tax(String sales_Tax) {
		Sales_Tax = sales_Tax;
	}
	public String getSales_Tax_Value() {
		return Sales_Tax_Value;
	}
	public void setSales_Tax_Value(String sales_Tax_Value) {
		Sales_Tax_Value = sales_Tax_Value;
	}
	public String getMis_Tax() {
		return Mis_Tax;
	}
	public void setMis_Tax(String mis_Tax) {
		Mis_Tax = mis_Tax;
	}
	public String getMis_Tax_Value() {
		return Mis_Tax_Value;
	}
	public void setMis_Tax_Value(String mis_Tax_Value) {
		Mis_Tax_Value = mis_Tax_Value;
	}

	
	
}
