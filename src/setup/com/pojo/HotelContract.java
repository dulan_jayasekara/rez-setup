package setup.com.pojo;

import setup.com.enumtypes.ChargeByType;
import setup.com.enumtypes.HotelInventoryType;
import setup.com.enumtypes.InventoryActionType;
import setup.com.enumtypes.InventoryObtainedByType;
import setup.com.enumtypes.RateContractByType;
import setup.com.enumtypes.RateSetupBy;

public class HotelContract {
	
private InventoryObtainedByType InventoryObtainedType    = null;
private RateSetupBy             RateSetUpByType           = null;
private RateContractByType      RateContract              = null;
private HotelInventoryType      InventoryType             = null;
private String                  HotelCommission           = "0";
private String                  ContractFrom              = "";
private String                  ContractTo                = "";
private InventoryActionType     SearchSatistied           = null;
private InventoryActionType     InventoryExhausted        = null;
private InventoryActionType     CutoffApplied             = null;
private InventoryActionType     MinNightRestriction       = null;
private InventoryActionType     MAxNightRestriction       = null;
private InventoryActionType     Blackout                  = null;
private InventoryActionType     NoArrival                 = null;
private ChargeByType            CommType                  = ChargeByType.PERCENTAGE;
private String                  Commvalue                 = "0";







public ChargeByType getCommType() {
	return CommType;
}
public void setCommType(String commType) {
	CommType = ChargeByType.getChargeType(commType.trim());
}
public String getCommvalue() {
	return Commvalue;
}
public void setCommvalue(String commvalue) {
	Commvalue = commvalue;
}


public InventoryObtainedByType getInventoryObtainedType() {
	return InventoryObtainedType;
}
public void setInventoryObtainedType(
		InventoryObtainedByType inventoryObtainedType) {
	InventoryObtainedType = inventoryObtainedType;
}
public RateSetupBy getRateSetUpByType() {
	return RateSetUpByType;
}
public void setRateSetUpByType(RateSetupBy rateSetUpByType) {
	RateSetUpByType = rateSetUpByType;
}
public RateContractByType getRateContract() {
	return RateContract;
}
public void setRateContract(RateContractByType rateContract) {
	RateContract = rateContract;
}
public HotelInventoryType getInventoryType() {
	return InventoryType;
}
public void setInventoryType(HotelInventoryType inventoryType) {
	InventoryType = inventoryType;
}
public String getHotelCommission() {
	return HotelCommission;
}
public void setHotelCommission(String hotelCommission) {
	HotelCommission = hotelCommission;
}
public String getContractFrom() {
	return ContractFrom;
}
public void setContractFrom(String contractFrom) {
	ContractFrom = contractFrom;
}
public String getContractTo() {
	return ContractTo;
}
public void setContractTo(String contractTo) {
	ContractTo = contractTo;
}
public InventoryActionType getSearchSatistied() {
	return SearchSatistied;
}
public void setSearchSatistied(InventoryActionType searchSatistied) {
	SearchSatistied = searchSatistied;
}
public InventoryActionType getInventoryExhausted() {
	return InventoryExhausted;
}
public void setInventoryExhausted(InventoryActionType inventoryExhausted) {
	InventoryExhausted = inventoryExhausted;
}
public InventoryActionType getCutoffApplied() {
	return CutoffApplied;
}
public void setCutoffApplied(InventoryActionType cutoffApplied) {
	CutoffApplied = cutoffApplied;
}
public InventoryActionType getMinNightRestriction() {
	return MinNightRestriction;
}
public void setMinNightRestriction(InventoryActionType minNightRestriction) {
	MinNightRestriction = minNightRestriction;
}
public InventoryActionType getMAxNightRestriction() {
	return MAxNightRestriction;
}
public void setMAxNightRestriction(InventoryActionType mAxNightRestriction) {
	MAxNightRestriction = mAxNightRestriction;
}
public InventoryActionType getBlackout() {
	return Blackout;
}
public void setBlackout(InventoryActionType blackout) {
	Blackout = blackout;
}
public InventoryActionType getNoArrival() {
	return NoArrival;
}
public void setNoArrival(InventoryActionType noArrival) {
	NoArrival = noArrival;
}


	


}
