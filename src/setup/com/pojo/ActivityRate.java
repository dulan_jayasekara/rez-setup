package setup.com.pojo;

public class ActivityRate {
	
	
	public String Rate_ActivityProgramName;
	public String Rate_Region;
	public String Rate_Contract_from;
	public String Rate_Contract_to;
	public String Rate_ActivityRatePeriod;
	public String Rate_FixRate;
	public String Rate_NetRate;
	public String Rate_SellRate;
	
	public String Rate_Friday_Inventory;
	public String Rate_Saturday_Inventory;
	public String Rate_Sunday_Inventory;
	
	
	public String getRate_Region() {
		return Rate_Region;
	}
	public void setRate_Region(String rate_Region) {
		Rate_Region = rate_Region;
	}
	public String getRate_ActivityProgramName() {
		return Rate_ActivityProgramName;
	}
	public void setRate_ActivityProgramName(String rate_ActivityProgramName) {
		Rate_ActivityProgramName = rate_ActivityProgramName;
	}
	public String getRate_Friday_Inventory() {
		return Rate_Friday_Inventory;
	}
	public void setRate_Friday_Inventory(String rate_Friday_Inventory) {
		Rate_Friday_Inventory = rate_Friday_Inventory;
	}
	public String getRate_Saturday_Inventory() {
		return Rate_Saturday_Inventory;
	}
	public void setRate_Saturday_Inventory(String rate_Saturday_Inventory) {
		Rate_Saturday_Inventory = rate_Saturday_Inventory;
	}
	public String getRate_Sunday_Inventory() {
		return Rate_Sunday_Inventory;
	}
	public void setRate_Sunday_Inventory(String rate_Sunday_Inventory) {
		Rate_Sunday_Inventory = rate_Sunday_Inventory;
	}
	public String getRate_SellRate() {
		return Rate_SellRate;
	}
	public void setRate_SellRate(String rate_SellRate) {
		Rate_SellRate = rate_SellRate;
	}
	
	
	public String getRate_Contract_from() {
		return Rate_Contract_from;
	}
	public void setRate_Contract_from(String rate_Contract_from) {
		Rate_Contract_from = rate_Contract_from;
	}
	public String getRate_Contract_to() {
		return Rate_Contract_to;
	}
	public void setRate_Contract_to(String rate_Contract_to) {
		Rate_Contract_to = rate_Contract_to;
	}
	public String getRate_ActivityRatePeriod() {
		return Rate_ActivityRatePeriod;
	}
	public void setRate_ActivityRatePeriod(String rate_ActivityRatePeriod) {
		Rate_ActivityRatePeriod = rate_ActivityRatePeriod;
	}
	public String getRate_FixRate() {
		return Rate_FixRate;
	}
	public void setRate_FixRate(String rate_FixRate) {
		Rate_FixRate = rate_FixRate;
	}
	public String getRate_NetRate() {
		return Rate_NetRate;
	}
	public void setRate_NetRate(String rate_NetRate) {
		Rate_NetRate = rate_NetRate;
	}
	

}
