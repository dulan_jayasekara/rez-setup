package setup.com.pojo;

public class Discount {
	
	
	
	public String discountName 		= "";
	public	String status				= "";
	public	String bookingChannel 		= "";
	public	String partner				= "";
	public	String discountType 		= "";
	public	String discount				= "";
	public	String validityPeriod		= "";
	public	String bookingDateFrom		= "";
	public	String bookingDateTo		= "";
	public	String checkinFrom			= "";
	public	String checkinTo			= "";
	public	String discountApplicableOn	= "";
	public	String date					= "";
	public	String productType			= "";
	public	String couponType			= "";
	public	String component			= "";
	// edited for picc
	public	String fullOrBookingFee		= "";
	public	String country				= "";
	public	String country2				= "";
	public	String allOrSelected		= "";
	public	String city					= "";
	public	String singleCoupon			= "";
	public	String sequenceCoupon		= "";
	public	String hotels				= "";
	public	String activities			= "";
	public	String car					= "";
	public	String numOfCoupons			= "";
	public	String reusability			= "";
	public	String numOfTimes			= "";
	public	String execute 				= "";
	public	String diactivate			= "";
	public	String checkAvailability  	= "";
	public	String count				= "";
	public	String modify				= "";
	
	
	
	
	public String getDiscountName() {
		return discountName;
	}
	public void setDiscountName(String discountName) {
		this.discountName = discountName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBookingChannel() {
		return bookingChannel;
	}
	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	public String getDiscountType() {
		return discountType;
	}
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getValidityPeriod() {
		return validityPeriod;
	}
	public void setValidityPeriod(String validityPeriod) {
		this.validityPeriod = validityPeriod;
	}
	public String getBookingDateFrom() {
		return bookingDateFrom;
	}
	public void setBookingDateFrom(String bookingDateFrom) {
		this.bookingDateFrom = bookingDateFrom;
	}
	public String getBookingDateTo() {
		return bookingDateTo;
	}
	public void setBookingDateTo(String bookingDateTo) {
		this.bookingDateTo = bookingDateTo;
	}
	public String getCheckinFrom() {
		return checkinFrom;
	}
	public void setCheckinFrom(String checkinFrom) {
		this.checkinFrom = checkinFrom;
	}
	public String getCheckinTo() {
		return checkinTo;
	}
	public void setCheckinTo(String checkinTo) {
		this.checkinTo = checkinTo;
	}
	public String getDiscountApplicableOn() {
		return discountApplicableOn;
	}
	public void setDiscountApplicableOn(String discountApplicableOn) {
		this.discountApplicableOn = discountApplicableOn;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getCouponType() {
		return couponType;
	}
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public String getFullOrBookingFee() {
		return fullOrBookingFee;
	}
	public void setFullOrBookingFee(String fullOrBookingFee) {
		this.fullOrBookingFee = fullOrBookingFee;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountry2() {
		return country2;
	}
	public void setCountry2(String country2) {
		this.country2 = country2;
	}
	public String getAllOrSelected() {
		return allOrSelected;
	}
	public void setAllOrSelected(String allOrSelected) {
		this.allOrSelected = allOrSelected;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getSingleCoupon() {
		return singleCoupon;
	}
	public void setSingleCoupon(String singleCoupon) {
		this.singleCoupon = singleCoupon;
	}
	public String getSequenceCoupon() {
		return sequenceCoupon;
	}
	public void setSequenceCoupon(String sequenceCoupon) {
		this.sequenceCoupon = sequenceCoupon;
	}
	public String getHotels() {
		return hotels;
	}
	public void setHotels(String hotels) {
		this.hotels = hotels;
	}
	public String getActivities() {
		return activities;
	}
	public void setActivities(String activities) {
		this.activities = activities;
	}
	public String getCar() {
		return car;
	}
	public void setCar(String car) {
		this.car = car;
	}
	public String getNumOfCoupons() {
		return numOfCoupons;
	}
	public void setNumOfCoupons(String numOfCoupons) {
		this.numOfCoupons = numOfCoupons;
	}
	public String getReusability() {
		return reusability;
	}
	public void setReusability(String reusability) {
		this.reusability = reusability;
	}
	public String getNumOfTimes() {
		return numOfTimes;
	}
	public void setNumOfTimes(String numOfTimes) {
		this.numOfTimes = numOfTimes;
	}
	public String getExecute() {
		return execute;
	}
	public void setExecute(String execute) {
		this.execute = execute;
	}
	public String getDiactivate() {
		return diactivate;
	}
	public void setDiactivate(String diactivate) {
		this.diactivate = diactivate;
	}
	public String getCheckAvailability() {
		return checkAvailability;
	}
	public void setCheckAvailability(String checkAvailability) {
		this.checkAvailability = checkAvailability;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public String getModify() {
		return modify;
	}
	public void setModify(String modify) {
		this.modify = modify;
	}
	public String getSaveError() {
		return saveError;
	}
	public void setSaveError(String saveError) {
		this.saveError = saveError;
	}
	public String getDiscountstatus() {
		return discountstatus;
	}
	public void setDiscountstatus(String discountstatus) {
		this.discountstatus = discountstatus;
	}
	public	String saveError = "N/A";
	public	String discountstatus = "";

}
