package setup.com.pojo;

import setup.com.enumtypes.ChargeByType;

public class HotelPolicy {
	
private String From = "";
private String To   = "";
private String ArrivalLessThan           = "";
private String CancellatrionBuffer       = "";
private ChargeByType StdChargeByType     = null;
private String       StdValue            = "";
private ChargeByType NoShowChargeByType  = null;
private String       NoshowValue         = "";

private ChargeByType SalesTax                 = null;
private String       SalesTaxValue            = "";
private ChargeByType OccupancyTax             = null;
private String       OccupancyTaxValue        = "";
private ChargeByType EnergyTax                = null;
private String       EnergyTaxValue           = "";
private String       MiscellaneousFees        = "";



public ChargeByType getSalesTax() {
	return SalesTax;
}
public void setSalesTax(ChargeByType salesTax) {
	SalesTax = salesTax;
}
public String getSalesTaxValue() {
	return SalesTaxValue;
}
public void setSalesTaxValue(String salesTaxValue) {
	SalesTaxValue = salesTaxValue;
}
public ChargeByType getOccupancyTax() {
	return OccupancyTax;
}
public void setOccupancyTax(ChargeByType occupancyTax) {
	OccupancyTax = occupancyTax;
}
public String getOccupancyTaxValue() {
	return OccupancyTaxValue;
}
public void setOccupancyTaxValue(String occupancyTaxValue) {
	OccupancyTaxValue = occupancyTaxValue;
}
public ChargeByType getEnergyTax() {
	return EnergyTax;
}
public void setEnergyTax(ChargeByType energyTax) {
	EnergyTax = energyTax;
}
public String getEnergyTaxValue() {
	return EnergyTaxValue;
}
public void setEnergyTaxValue(String energyTaxValue) {
	EnergyTaxValue = energyTaxValue;
}
public String getMiscellaneousFees() {
	return MiscellaneousFees;
}
public void setMiscellaneousFees(String miscellaneousFees) {
	MiscellaneousFees = miscellaneousFees;
}
public String getFrom() {
	return From;
}
public void setFrom(String from) {
	From = from;
}
public String getTo() {
	return To;
}
public void setTo(String to) {
	To = to;
}
public String getArrivalLessThan() {
	return ArrivalLessThan;
}
public void setArrivalLessThan(String arrivalLessThan) {
	ArrivalLessThan = arrivalLessThan;
}
public String getCancellatrionBuffer() {
	return CancellatrionBuffer;
}
public void setCancellatrionBuffer(String cancellatrionBuffer) {
	CancellatrionBuffer = cancellatrionBuffer;
}
public ChargeByType getStdChargeByType() {
	return StdChargeByType;
}
public void setStdChargeByType(ChargeByType stdChargeByType) {
	StdChargeByType = stdChargeByType;
}
public String getStdValue() {
	return StdValue;
}
public void setStdValue(String stdValue) {
	StdValue = stdValue;
}
public ChargeByType getNoShowChargeByType() {
	return NoShowChargeByType;
}
public void setNoShowChargeByType(ChargeByType noShowChargeByType) {
	NoShowChargeByType = noShowChargeByType;
}
public String getNoshowValue() {
	return NoshowValue;
}
public void setNoshowValue(String noshowValue) {
	NoshowValue = noshowValue;
}




}
