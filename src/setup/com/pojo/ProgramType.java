package setup.com.pojo;

public class ProgramType {
	
	public String Activiy_P_Cat = "";
	public String Rank = ""; 
	public String pick_drop_info = "";
	public String Enable_location = "";
	public String Enable_Act_Type = "";
	
	
	public String getEnable_location() {
		return Enable_location;
	}
	public void setEnable_location(String enable_location) {
		Enable_location = enable_location;
	}
	public String getEnable_Act_Type() {
		return Enable_Act_Type;
	}
	public void setEnable_Act_Type(String enable_Act_Type) {
		Enable_Act_Type = enable_Act_Type;
	}
	public String getActiviy_P_Cat() {
		return Activiy_P_Cat;
	}
	public void setActiviy_P_Cat(String activiy_P_Cat) {
		Activiy_P_Cat = activiy_P_Cat;
	}
	public String getRank() {
		return Rank;
	}
	public void setRank(String rank) {
		Rank = rank;
	}
	public String getPick_drop_info() {
		return pick_drop_info;
	}
	public void setPick_drop_info(String pick_drop_info) {
		this.pick_drop_info = pick_drop_info;
	}
	

}
