package setup.com.pojo;

public class HotelRoom {
	
private String RoomType   = ""; 
private String BedType    = "";
private String RatePlan   = "";
private String StdAdults  = "";
private String Children   = "";
private String AAdults  = "";

private boolean isMultipleChildRatesApplied   = false;
private boolean isCombinationActive           = true;
private String Region              = ""; 
private String TourOperator        = "";
private String TotalRooms          = "";
private String MinimumNightsStay   = "";
private String MaximumNightsStay   = "";
private String CutOff              = "";
private String NetRate             = "";
private String AdditionalAdultRate = "";
private String ChildNetRate        = "";




public String getAAdults() {
	return AAdults;
}
public void setAAdults(String aAdults) {
	AAdults = aAdults;
}
public String getRoomType() {
	return RoomType;
}
public void setRoomType(String roomType) {
	RoomType = roomType;
}
public String getBedType() {
	return BedType;
}
public void setBedType(String bedType) {
	BedType = bedType;
}
public String getRatePlan() {
	return RatePlan;
}
public void setRatePlan(String ratePlan) {
	RatePlan = ratePlan;
}
public String getStdAdults() {
	return StdAdults;
}
public void setStdAdults(String stdAdults) {
	StdAdults = stdAdults;
}
public String getChildren() {
	return Children;
}
public void setChildren(String children) {
	Children = children;
}
public boolean isMultipleChildRatesApplied() {
	return isMultipleChildRatesApplied;
}
public void setMultipleChildRatesApplied(boolean isMultipleChildRatesApplied) {
	this.isMultipleChildRatesApplied = isMultipleChildRatesApplied;
}
public boolean isCombinationActive() {
	return isCombinationActive;
}
public void setCombinationActive(boolean isCombinationActive) {
	this.isCombinationActive = isCombinationActive;
}
public String getRegion() {
	return Region;
}
public void setRegion(String region) {
	Region = region;
}
public String getTourOperator() {
	return TourOperator;
}
public void setTourOperator(String tourOperator) {
	TourOperator = tourOperator;
}
public String getTotalRooms() {
	return TotalRooms;
}
public void setTotalRooms(String totalRooms) {
	TotalRooms = totalRooms;
}
public String getMinimumNightsStay() {
	return MinimumNightsStay;
}
public void setMinimumNightsStay(String minimumNightsStay) {
	MinimumNightsStay = minimumNightsStay;
}
public String getMaximumNightsStay() {
	return MaximumNightsStay;
}
public void setMaximumNightsStay(String maximumNightsStay) {
	MaximumNightsStay = maximumNightsStay;
}
public String getCutOff() {
	return CutOff;
}
public void setCutOff(String cutOff) {
	CutOff = cutOff;
}
public String getNetRate() {
	return NetRate;
}
public void setNetRate(String netRate) {
	NetRate = netRate;
}
public String getAdditionalAdultRate() {
	return AdditionalAdultRate;
}
public void setAdditionalAdultRate(String additionalAdultRate) {
	AdditionalAdultRate = additionalAdultRate;
}
public String getChildNetRate() {
	return ChildNetRate;
}
public void setChildNetRate(String childNetRate) {
	ChildNetRate = childNetRate;
}






}
