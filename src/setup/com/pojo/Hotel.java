package setup.com.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import setup.com.enumtypes.CommssionableType;
import setup.com.enumtypes.InventoryObtainedByType;
import setup.com.enumtypes.RateContractByType;
import setup.com.enumtypes.RateSetupBy;
import setup.com.pojo.HotelPromotion;
import setup.com.pojo.HotelSupplementary;;

public class Hotel {

	private String HotelName = "";
	private String Supplier = "";
	private String AddressLine1 = "";
	private String AddressLine2 = "";
	private String Country      = "";
	private String City         = "";
	private String State        = "";
	private String HotelGroup = "";
	private String StarCategory = "";
	private boolean FeaturedStatus = false;
	private boolean Passportrequired = false;
	private boolean DisplayinCC = false;
	private boolean DisplayinWeb = false;
	private boolean ChildrenAllowed = false;
	private String ChargesFreeAgeFrom = "";
	private String ChargesFreeAgeTo = "";
	private String ChildRateApplicableFrom = "";
	private String ChildRateApplicableTo = "";
	private String StandardCheckIn = "";
	private String StandardCheckOut = "";
	private String HotelCurrency = "";
	private boolean isTaxApplicable = false;
	private boolean isSetupNeeded = true;
	/*
	 * private String SalesTax = ""; private String OccupancyTax = ""; private String EnergyTax = ""; private String MisceleniousTax = "";
	 */
	private ArrayList<HotelContract> HotelContracts = new ArrayList<HotelContract>();
	private ArrayList<HotelPolicy> HotelPolicies = new ArrayList<HotelPolicy>();
	private ArrayList<HotelTax> HotelTax = new ArrayList<HotelTax>();

	private ArrayList<HotelRoom> HotelRooms = new ArrayList<HotelRoom>();
	private ArrayList<ProfitMarkup> HotelProfitMarkUps = new ArrayList<ProfitMarkup>();
	private Map<String, HotelSupplementary> HotelSupplimentary = new HashMap<String, HotelSupplementary>();
	private ArrayList<HotelPromotion> HotelPromotions = new ArrayList<HotelPromotion>();
	private InventoryObtainedByType InventoryObtainedType = null;
	private RateSetupBy RateSetUpByType = null;
	private RateContractByType RateContract = null;
	private String HotelContractFromDate = null;
	private String HotelContractToDate = null;
	private CommssionableType CommHotelType = CommssionableType.NONE;

	private String ShortDescription = "";
	private String LongDescription = "";
	private String Latitude = "";
	private String Longitude = "";
	private String Filter = "";
	private String Path = "";

	public Map<String, HotelSupplementary> getHotelSupplimentary() {
		return HotelSupplimentary;
	}

	public void addHotelSupplimentary(String SuppName, HotelSupplementary hotelSupplimentary) {
		this.HotelSupplimentary.put(SuppName, hotelSupplimentary);
	}

	public ArrayList<HotelPromotion> getHotelPromotions() {
		return HotelPromotions;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public void addHotelPromotions(HotelPromotion hotelPromotion) {
		HotelPromotions.add(hotelPromotion);
	}

	public boolean isSetupNeeded() {
		return isSetupNeeded;
	}

	public void setSetupNeeded(String isSetupNeeded) {
		if (isSetupNeeded.equalsIgnoreCase("Yes"))
			this.isSetupNeeded = true;
		else
			this.isSetupNeeded = false;
	}

	public String getHotelCurrency() {
		return HotelCurrency;
	}

	public void setHotelCurrency(String hotelCurrency) {
		HotelCurrency = hotelCurrency;
	}

	public String getPath() {
		return Path;
	}

	public void setPath(String path) {
		Path = path;
	}

	public String getFilter() {
		return Filter;
	}

	public void setFilter(String string) {
		Filter = string;
	}

	public String getShortDescription() {
		return ShortDescription;
	}

	public void setShortDescription(String shortDescription) {
		ShortDescription = shortDescription;
	}

	public String getLongDescription() {
		return LongDescription;
	}

	public void setLongDescription(String longDescription) {
		LongDescription = longDescription;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public CommssionableType getCommHotelType() {
		return CommHotelType;
	}

	public void setCommHotelType(String commHotelType) {
		CommHotelType = CommssionableType.getChargeType(commHotelType);
	}

	public String getHotelContractFromDate() {
		return HotelContractFromDate;
	}

	public void setHotelContractFromDate(String hotelContractFromDate) {
		HotelContractFromDate = hotelContractFromDate;
	}

	public String getHotelContractToDate() {
		return HotelContractToDate;
	}

	public void setHotelContractToDate(String hotelContractToDate) {
		HotelContractToDate = hotelContractToDate;
	}

	public InventoryObtainedByType getInventoryObtainedType() {
		return InventoryObtainedType;
	}

	public void setInventoryObtainedType(InventoryObtainedByType inventoryObtainedType) {
		InventoryObtainedType = inventoryObtainedType;
	}

	public RateSetupBy getRateSetUpByType() {
		return RateSetUpByType;
	}

	public void setRateSetUpByType(RateSetupBy rateSetUpByType) {
		RateSetUpByType = rateSetUpByType;
	}

	public RateContractByType getRateContract() {
		return RateContract;
	}

	public void setRateContract(RateContractByType rateContract) {
		RateContract = rateContract;
	}

	public boolean isTaxApplicable() {
		return isTaxApplicable;
	}

	public void setTaxApplicable(String isTaxApplicable) {
		if (isTaxApplicable.equalsIgnoreCase("yes"))
			this.isTaxApplicable = true;
		else
			this.isTaxApplicable = false;
	}

	public void setFeaturedStatus(boolean featuredStatus) {
		FeaturedStatus = featuredStatus;
	}

	public String getHotelName() {
		return HotelName;
	}

	public void setHotelName(String hotelName) {
		HotelName = hotelName;
	}

	public String getSupplier() {
		return Supplier;
	}

	public void setSupplier(String supplier) {
		Supplier = supplier;
	}

	public String getAddressLine1() {
		return AddressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		AddressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return AddressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		AddressLine2 = addressLine2;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getHotelGroup() {
		return HotelGroup;
	}

	public void setHotelGroup(String hotelGroup) {
		HotelGroup = hotelGroup;
	}

	public String getStarCategory() {
		return StarCategory;
	}

	public void setStarCategory(String starCategory) {
		StarCategory = starCategory;
	}

	public boolean isFeaturedStatus() {
		return FeaturedStatus;
	}

	public void setFeaturedStatus(String featuredStatus) {
		if (featuredStatus.equalsIgnoreCase("yes"))
			FeaturedStatus = true;
		else
			FeaturedStatus = false;
	}

	public boolean isPassportrequired() {
		return Passportrequired;

	}

	public void setPassportrequired(String passportrequired) {
		if (passportrequired.equalsIgnoreCase("yes"))
			Passportrequired = true;
		else
			Passportrequired = false;
	}

	public boolean isDisplayinCC() {
		return DisplayinCC;
	}

	public void setDisplayinCC(String displayinCC) {
		if (displayinCC.equalsIgnoreCase("yes"))
			DisplayinCC = true;
		else
			DisplayinCC = false;
	}

	public boolean isDisplayinWeb() {
		return DisplayinWeb;
	}

	public void setDisplayinWeb(String displayinWeb) {
		if (displayinWeb.equalsIgnoreCase("yes"))
			DisplayinWeb = true;
		else
			DisplayinWeb = false;
	}

	public boolean isChildrenAllowed() {
		return ChildrenAllowed;
	}

	public void setChildrenAllowed(String childrenAllowed) {
		if (childrenAllowed.equalsIgnoreCase("yes"))
			ChildrenAllowed = true;
		else
			ChildrenAllowed = false;
	}

	public String getChargesFreeAgeFrom() {
		return ChargesFreeAgeFrom;
	}

	public void setChargesFreeAgeFrom(String chargesFreeAgeFrom) {
		ChargesFreeAgeFrom = chargesFreeAgeFrom;
	}

	public String getChargesFreeAgeTo() {
		return ChargesFreeAgeTo;
	}

	public void setChargesFreeAgeTo(String chargesFreeAgeTo) {
		ChargesFreeAgeTo = chargesFreeAgeTo;
	}

	public String getChildRateApplicableFrom() {
		return ChildRateApplicableFrom;
	}

	public void setChildRateApplicableFrom(String childRateApplicableFrom) {
		ChildRateApplicableFrom = childRateApplicableFrom;
	}

	public String getChildRateApplicableTo() {
		return ChildRateApplicableTo;
	}

	public void setChildRateApplicableTo(String childRateApplicableTo) {
		ChildRateApplicableTo = childRateApplicableTo;
	}

	public String getStandardCheckIn() {
		return StandardCheckIn;
	}

	public void setStandardCheckIn(String standardCheckIn) {
		StandardCheckIn = standardCheckIn;
	}

	public String getStandardCheckOut() {
		return StandardCheckOut;
	}

	public void setStandardCheckOut(String standardCheckOut) {
		StandardCheckOut = standardCheckOut;
	}

	public ArrayList<HotelContract> getHotelContracts() {
		return HotelContracts;
	}

	public void setHotelContracts(ArrayList<HotelContract> hotelContracts) {
		HotelContracts = hotelContracts;
	}

	public ArrayList<HotelPolicy> getHotelPolicies() {
		return HotelPolicies;
	}

	public void setHotelPolicies(ArrayList<HotelPolicy> hotelPolicies) {
		HotelPolicies = hotelPolicies;
	}

	public ArrayList<HotelRoom> getHotelRooms() {
		return HotelRooms;
	}

	public void setHotelRooms(ArrayList<HotelRoom> hotelRooms) {
		HotelRooms = hotelRooms;
	}

	public ArrayList<ProfitMarkup> getHotelProfitMarkUps() {
		return HotelProfitMarkUps;
	}

	public void setHotelProfitMarkUps(ArrayList<ProfitMarkup> hotelProfitMarkUps) {
		HotelProfitMarkUps = hotelProfitMarkUps;
	}

	public void AddContract(HotelContract contract) {
		HotelContracts.add(contract);
	}

	public void AddPolicy(HotelPolicy Policy) {
		HotelPolicies.add(Policy);
	}

	public void AddTax(HotelTax tax) {
		HotelTax.add(tax);
	}

	public void AddRoom(HotelRoom Room) {

		HotelRooms.add(Room);
	}

	public void AddMarkup(ProfitMarkup Markup) {
		HotelProfitMarkUps.add(Markup);
	}

	public ArrayList<HotelTax> getHotelTax() {
		return HotelTax;
	}

}
