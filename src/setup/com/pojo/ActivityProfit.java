package setup.com.pojo;

public class ActivityProfit {
	
	public String ActivityProgramName;
	public String Booking_Channel; 
	public String Partner; 
	public String Rate_Region; 
	public String ProfitMarkup_Type; 
	public String Profit_Period_From; 
	public String Profit_Period_To; 
	public String Country; 
	public String City; 
	public String Supplier; 
	public String Profit_ApplyTo; 
	
	public String Select_Program; 
	public String Markup_Rate; 
	public String Specific_Markup; 
	public String Fix_Profit_Markup; 
	public String Profit_Markup_V; 
	public String Friday_Profit; 
	public String Saturday_Profit;
	
	public String selectProfitActivity;
	
	
	public String getSelectProfitActivity() {
		return selectProfitActivity;
	}
	public void setSelectProfitActivity(String selectProfitActivity) {
		this.selectProfitActivity = selectProfitActivity;
	}
	public String getActivityProgramName() {
		return ActivityProgramName;
	}
	public void setActivityProgramName(String activityProgramName) {
		ActivityProgramName = activityProgramName;
	}
	public String getBooking_Channel() {
		return Booking_Channel;
	}
	public void setBooking_Channel(String booking_Channel) {
		Booking_Channel = booking_Channel;
	}
	public String getPartner() {
		return Partner;
	}
	public void setPartner(String partner) {
		Partner = partner;
	}
	public String getRate_Region() {
		return Rate_Region;
	}
	public void setRate_Region(String rate_Region) {
		Rate_Region = rate_Region;
	}
	public String getProfitMarkup_Type() {
		return ProfitMarkup_Type;
	}
	public void setProfitMarkup_Type(String profitMarkup_Type) {
		ProfitMarkup_Type = profitMarkup_Type;
	}
	public String getProfit_Period_From() {
		return Profit_Period_From;
	}
	public void setProfit_Period_From(String profit_Period_From) {
		Profit_Period_From = profit_Period_From;
	}
	public String getProfit_Period_To() {
		return Profit_Period_To;
	}
	public void setProfit_Period_To(String profit_Period_To) {
		Profit_Period_To = profit_Period_To;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getSupplier() {
		return Supplier;
	}
	public void setSupplier(String supplier) {
		Supplier = supplier;
	}
	public String getProfit_ApplyTo() {
		return Profit_ApplyTo;
	}
	public void setProfit_ApplyTo(String profit_ApplyTo) {
		Profit_ApplyTo = profit_ApplyTo;
	}
	public String getSelect_Program() {
		return Select_Program;
	}
	public void setSelect_Program(String select_Program) {
		Select_Program = select_Program;
	}
	public String getMarkup_Rate() {
		return Markup_Rate;
	}
	public void setMarkup_Rate(String markup_Rate) {
		Markup_Rate = markup_Rate;
	}
	public String getSpecific_Markup() {
		return Specific_Markup;
	}
	public void setSpecific_Markup(String specific_Markup) {
		Specific_Markup = specific_Markup;
	}
	public String getFix_Profit_Markup() {
		return Fix_Profit_Markup;
	}
	public void setFix_Profit_Markup(String fix_Profit_Markup) {
		Fix_Profit_Markup = fix_Profit_Markup;
	}
	public String getProfit_Markup_V() {
		return Profit_Markup_V;
	}
	public void setProfit_Markup_V(String profit_Markup_V) {
		Profit_Markup_V = profit_Markup_V;
	}
	public String getFriday_Profit() {
		return Friday_Profit;
	}
	public void setFriday_Profit(String friday_Profit) {
		Friday_Profit = friday_Profit;
	}
	public String getSaturday_Profit() {
		return Saturday_Profit;
	}
	public void setSaturday_Profit(String saturday_Profit) {
		Saturday_Profit = saturday_Profit;
	} 

	
	
}
