package setup.com.pojo;

public class PeriodSetup {
	
	public String Period_Name = "";
	public String Cutoff_detup = "";
	public String Cut_time = "";
	public String Period_desc = "";
	public String Duration_days = "";
	
	public String getPeriod_Name() {
		return Period_Name;
	}
	public void setPeriod_Name(String period_Name) {
		Period_Name = period_Name;
	}
	public String getCutoff_detup() {
		return Cutoff_detup;
	}
	public void setCutoff_detup(String cutoff_detup) {
		Cutoff_detup = cutoff_detup;
	}
	public String getCut_time() {
		return Cut_time;
	}
	public void setCut_time(String cut_time) {
		Cut_time = cut_time;
	}
	public String getPeriod_desc() {
		return Period_desc;
	}
	public void setPeriod_desc(String period_desc) {
		Period_desc = period_desc;
	}
	public String getDuration_days() {
		return Duration_days;
	}
	public void setDuration_days(String duration_days) {
		Duration_days = duration_days;
	}
	
	

}
