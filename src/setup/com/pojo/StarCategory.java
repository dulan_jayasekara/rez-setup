package setup.com.pojo;

public class StarCategory {

	public  String Hotel_star = "";
	public  String Hotel_star_order ="";
	
	
	public String getHotel_star() {
		return Hotel_star;
	}
	public void setHotel_star(String hotel_star) {
		Hotel_star = hotel_star;
	}
	public String getHotel_star_order() {
		return Hotel_star_order;
	}
	public void setHotel_star_order(String hotel_star_order) {
		Hotel_star_order = hotel_star_order;
	}
	
	
}
