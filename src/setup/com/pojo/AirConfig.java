package setup.com.pojo;

import setup.com.enumtypes.ConfigFareType;



public class AirConfig 
{
	int testNo						= 1;
	String AirLine					= "";
	
	ConfigFareType  PubFareType		= null;
	String PubBookingFee			= "";
	String PubProfitMarkup			= "";
	String PubBoth					= "";
	String PubNone					= "";
	
	ConfigFareType  PvtFareType		= null;
	String PvtBookingFee			= "";
	String PvtProfitMarkup			= "";
	String PvtBoth					= "";
	
	String FTypeConfgShopCart		= "";//1 value
	String FTypeConfgFplusH			= "";//2 value
	String FTypeConfgFixPack		= "";//2 value
	
	String FlightPayOptCartBooking				= "";//3 value
	String FlightPayOptPayfullFplusH			= "";//1 value
	
	boolean applyFareTypeConfigurations			= false;
	boolean applyPaymentOptionConfigurations	= false;
	boolean applyAirChargesConfigurations		= false;
	
	
	
	
	public boolean isApplyAirChargesConfigurations() {
		return applyAirChargesConfigurations;
	}
	public void setApplyAirChargesConfigurations(
			boolean applyAirChargesConfigurations) {
		this.applyAirChargesConfigurations = applyAirChargesConfigurations;
	}
	public String getPubBookingFee() {
		return PubBookingFee;
	}
	public void setPubBookingFee(String pubBookingFee) {
		PubBookingFee = pubBookingFee;
	}
	public String getPubProfitMarkup() {
		return PubProfitMarkup;
	}
	public void setPubProfitMarkup(String pubProfitMarkup) {
		PubProfitMarkup = pubProfitMarkup;
	}
	public String getPubBoth() {
		return PubBoth;
	}
	public void setPubBoth(String pubBoth) {
		PubBoth = pubBoth;
	}
	public String getPubNone() {
		return PubNone;
	}
	public void setPubNone(String pubNone) {
		PubNone = pubNone;
	}
	public String getPvtBookingFee() {
		return PvtBookingFee;
	}
	public void setPvtBookingFee(String pvtBookingFee) {
		PvtBookingFee = pvtBookingFee;
	}
	public String getPvtProfitMarkup() {
		return PvtProfitMarkup;
	}
	public void setPvtProfitMarkup(String pvtProfitMarkup) {
		PvtProfitMarkup = pvtProfitMarkup;
	}
	public String getPvtBoth() {
		return PvtBoth;
	}
	public void setPvtBoth(String pvtBoth) {
		PvtBoth = pvtBoth;
	}
	public boolean isApplyFareTypeConfigurations() {
		return applyFareTypeConfigurations;
	}
	public void setApplyFareTypeConfigurations(boolean applyFareTypeConfigurations) {
		this.applyFareTypeConfigurations = applyFareTypeConfigurations;
	}
	public boolean isApplyPaymentOptionConfigurations() {
		return applyPaymentOptionConfigurations;
	}
	public void setApplyPaymentOptionConfigurations(
			boolean applyPaymentOptionConfigurations) {
		this.applyPaymentOptionConfigurations = applyPaymentOptionConfigurations;
	}
	public int getTestNo() {
		return testNo;
	}
	public void setTestNo(int testNo) {
		this.testNo = testNo;
	}
	public String getAirLine() {
		return AirLine;
	}
	public void setAirLine(String airLine) {
		AirLine = airLine;
	}
	public ConfigFareType getPubFareType() {
		return PubFareType;
	}
	public void setPubFareType(ConfigFareType pubFareType) {
		PubFareType = pubFareType;
	}
	public ConfigFareType getPvtFareType() {
		return PvtFareType;
	}
	public void setPvtFareType(ConfigFareType pvtFareType) {
		PvtFareType = pvtFareType;
	}
	public String getFTypeConfgShopCart() {
		return FTypeConfgShopCart;
	}
	public void setFTypeConfgShopCart(String fTypeConfgShopCart) {
		FTypeConfgShopCart = fTypeConfgShopCart;
	}
	public String getFTypeConfgFplusH() {
		return FTypeConfgFplusH;
	}
	public void setFTypeConfgFplusH(String fTypeConfgFplusH) {
		FTypeConfgFplusH = fTypeConfgFplusH;
	}
	public String getFTypeConfgFixPack() {
		return FTypeConfgFixPack;
	}
	public void setFTypeConfgFixPack(String fTypeConfgFixPack) {
		FTypeConfgFixPack = fTypeConfgFixPack;
	}
	public String getFlightPayOptCartBooking() {
		return FlightPayOptCartBooking;
	}
	public void setFlightPayOptCartBooking(String flightPayOptCartBooking) {
		FlightPayOptCartBooking = flightPayOptCartBooking;
	}
	public String getFlightPayOptPayfullFplusH() {
		return FlightPayOptPayfullFplusH;
	}
	public void setFlightPayOptPayfullFplusH(String payfullFplusH) {
		FlightPayOptPayfullFplusH = payfullFplusH;
	}
	
}
