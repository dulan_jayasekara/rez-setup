package setup.com.pojo;

import java.util.ArrayList;


public class ActivityStandard {
	
	public String Activity_Program_Name;
	public String Sup_Name;
	public String Currency_Code;
	public String Country;
	public String City;
	public String Activity_P_Cat;
	public String Dis_Web;
	public String Dis_Call;
	public String Program_Active;
	public String Customer_Alert;
	public String Ticket_Name;
	public String Ticket_Body;
	public String Passport_req;	
	public String Voucher_Req;
	public String Contact_Name;
	public String Contact_Email;
	public String Contact_Type;
	
	public String Program_Available_Dates;
	public String Inventory_Obtained;
	public String RateContract;
	public String CommissionContractType;
	
	public String Prepay_Req;
	public String CreditCard_Enable;
	public String actDescription;
	public String actImagesPath;
	private boolean setUpNeeded = false;
	
	private ArrayList<Contract> 	ActivityContracts           = new ArrayList<Contract>();
	private ArrayList<Policies>   	ActivityPolicies            = new ArrayList<Policies>();
	private ArrayList<Tax>      	ActivityTax                 = new ArrayList<Tax>();
	private ArrayList<AssignActivity>      	 AssignActivity   	= new ArrayList<AssignActivity>();
	private ArrayList<ActivityInventory>      	ActivityInventory         = new ArrayList<ActivityInventory>();
	private ArrayList<ActivityRate>      	ActivityRates                 = new ArrayList<ActivityRate>();
	private ArrayList<ActivityProfit>      	ActivityProfits               = new ArrayList<ActivityProfit>();
	
	
	public void AddContract(Contract contract)
	{
		ActivityContracts.add(contract);
	}
	
	public void AddPolicies(Policies policies)
	{
		ActivityPolicies.add(policies);
	}
	
	public void AddTax(Tax tax)
	{
		ActivityTax.add(tax);
	}
	
	public void AddAssignActivity(AssignActivity assignActivity)
	{
		AssignActivity.add(assignActivity);
	}
	
	public void AddInventory(ActivityInventory activityInventory){
		
		ActivityInventory.add(activityInventory);
	}
	
	public void AddActivityRate(ActivityRate activityRate){
		
		ActivityRates.add(activityRate);
	}

	public void AddProfits(ActivityProfit activityProfit){
	
		ActivityProfits.add(activityProfit);
	}
	
	
	public String getActImagesPath() {
		return actImagesPath;
	}

	public void setActImagesPath(String actImagesPath) {
		this.actImagesPath = actImagesPath;
	}

	public String getActDescription() {
		return actDescription;
	}

	public void setActDescription(String actDescription) {
		this.actDescription = actDescription;
	}

	public String getPrepay_Req() {
		return Prepay_Req;
	}
	public void setPrepay_Req(String prepay_Req) {
		Prepay_Req = prepay_Req;
	}
	public String getCreditCard_Enable() {
		return CreditCard_Enable;
	}
	public void setCreditCard_Enable(String creditCard_Enable) {
		CreditCard_Enable = creditCard_Enable;
	}
	public String getProgram_Available_Dates() {
		return Program_Available_Dates;
	}
	public void setProgram_Available_Dates(String program_Available_Dates) {
		Program_Available_Dates = program_Available_Dates;
	}
	public String getInventory_Obtained() {
		return Inventory_Obtained;
	}
	public void setInventory_Obtained(String inventory_Obtained) {
		Inventory_Obtained = inventory_Obtained;
	}
	public String getRateContract() {
		return RateContract;
	}
	public void setRateContract(String rateContract) {
		RateContract = rateContract;
	}
	public String getCommissionContractType() {
		return CommissionContractType;
	}
	public void setCommissionContractType(String commissionContractType) {
		CommissionContractType = commissionContractType;
	}
	public ArrayList<Contract> getActivityContracts() {
		return ActivityContracts;
	}
	public void setActivityContracts(ArrayList<Contract> activityContracts) {
		ActivityContracts = activityContracts;
	}
	public ArrayList<Policies> getActivityPolicies() {
		return ActivityPolicies;
	}
	public void setActivityPolicies(ArrayList<Policies> activityPolicies) {
		ActivityPolicies = activityPolicies;
	}
	public ArrayList<Tax> getActivityTax() {
		return ActivityTax;
	}
	public void setActivityTax(ArrayList<Tax> activityTax) {
		ActivityTax = activityTax;
	}
	public ArrayList<AssignActivity> getAssignActivity() {
		return AssignActivity;
	}
	public void setAssignActivity(ArrayList<AssignActivity> assignActivity) {
		AssignActivity = assignActivity;
	}
	public String getActivity_Program_Name() {
		return Activity_Program_Name;
	}
	public void setActivity_Program_Name(String activity_Program_Name) {
		Activity_Program_Name = activity_Program_Name;
	}
	public String getSup_Name() {
		return Sup_Name;
	}
	public void setSup_Name(String sup_Name) {
		Sup_Name = sup_Name;
	}
	public String getCurrency_Code() {
		return Currency_Code;
	}
	public void setCurrency_Code(String currency_Code) {
		Currency_Code = currency_Code;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getActivity_P_Cat() {
		return Activity_P_Cat;
	}
	public void setActivity_P_Cat(String activity_P_Cat) {
		Activity_P_Cat = activity_P_Cat;
	}
	public String getDis_Web() {
		return Dis_Web;
	}
	public void setDis_Web(String dis_Web) {
		Dis_Web = dis_Web;
	}
	public String getDis_Call() {
		return Dis_Call;
	}
	public void setDis_Call(String dis_Call) {
		Dis_Call = dis_Call;
	}
	public String getProgram_Active() {
		return Program_Active;
	}
	public void setProgram_Active(String program_Active) {
		Program_Active = program_Active;
	}
	public String getCustomer_Alert() {
		return Customer_Alert;
	}
	public void setCustomer_Alert(String customer_Alert) {
		Customer_Alert = customer_Alert;
	}
	public String getTicket_Name() {
		return Ticket_Name;
	}
	public void setTicket_Name(String ticket_Name) {
		Ticket_Name = ticket_Name;
	}
	public String getTicket_Body() {
		return Ticket_Body;
	}
	public void setTicket_Body(String ticket_Body) {
		Ticket_Body = ticket_Body;
	}
	public String getPassport_req() {
		return Passport_req;
	}
	public void setPassport_req(String passport_req) {
		Passport_req = passport_req;
	}
	public String getVoucher_Req() {
		return Voucher_Req;
	}
	public void setVoucher_Req(String voucher_Req) {
		Voucher_Req = voucher_Req;
	}
	public String getContact_Name() {
		return Contact_Name;
	}
	public void setContact_Name(String contact_Name) {
		Contact_Name = contact_Name;
	}
	public String getContact_Email() {
		return Contact_Email;
	}
	public void setContact_Email(String contact_Email) {
		Contact_Email = contact_Email;
	}
	public String getContact_Type() {
		return Contact_Type;
	}
	public void setContact_Type(String contact_Type) {
		Contact_Type = contact_Type;
	}

	public ArrayList<ActivityInventory> getActivityInventory() {
		return ActivityInventory;
	}

	public void setActivityInventory(ArrayList<ActivityInventory> activityInventory) {
		ActivityInventory = activityInventory;
	}

	public ArrayList<ActivityRate> getActivityRates() {
		return ActivityRates;
	}

	public void setActivityRates(ArrayList<ActivityRate> activityRates) {
		ActivityRates = activityRates;
	}

	public ArrayList<ActivityProfit> getActivityProfits() {
		return ActivityProfits;
	}

	public void setActivityProfits(ArrayList<ActivityProfit> activityProfits) {
		ActivityProfits = activityProfits;
	}

	public void setActSetupNeeded(String string) {
         setUpNeeded	= string.equalsIgnoreCase("yes")? true : false;	
	}

	public boolean isSetUpNeeded() {
		return setUpNeeded;
	}
	
	
	
}
