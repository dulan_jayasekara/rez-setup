package setup.com.pojo;

public class ActivityType {
	
	public String Activity_type = "";
	public String Activiy_P_Cat = "";
	public String Activity_desc = "";
	public String Return_Availability = "";
	
	public String getActivity_type() {
		return Activity_type;
	}
	public void setActivity_type(String activity_type) {
		Activity_type = activity_type;
	}
	public String getActiviy_P_Cat() {
		return Activiy_P_Cat;
	}
	public void setActiviy_P_Cat(String activiy_P_Cat) {
		Activiy_P_Cat = activiy_P_Cat;
	}
	public String getActivity_desc() {
		return Activity_desc;
	}
	public void setActivity_desc(String activity_desc) {
		Activity_desc = activity_desc;
	}
	public String getReturn_Availability() {
		return Return_Availability;
	}
	public void setReturn_Availability(String return_Availability) {
		Return_Availability = return_Availability;
	}
	
	

}
