package setup.com.pojo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import setup.com.dataObjects.costObj;
import setup.com.utill.ExtentReportTemplate;
import setup.com.utill.Reportwriter;

public class ResultsLoader {

	int testCaseCount=0;
	public StringBuffer resultsGenerator(Map<String , Map<String, costObj>> Ratemaps,String Date) throws IOException {

		StringBuffer PrintWriter=new StringBuffer();
		String[] type={"Single","Double","Triple","Child"};
		Map<String, costObj> Generated=Ratemaps.get("Generated");
		Map<String, costObj> Saved=Ratemaps.get("Saved");
		Map<String, costObj> Expected=Ratemaps.get("Expected");

		PrintWriter.append("<p class='InfoSup'>Costing for "+Date+"</p>");
		
		
		PrintWriter.append("<p class='InfoSup'>Test Points</p>");
		PrintWriter.append("<p class='InfoSub'>* Generated Rate Validations </p>");


		PrintWriter.append("</br></br><table ><tr><th>Test Case #</th><th>Test Description</th><th>Expected Result</th><th>Actual Result</th><th>Test Status</th></tr>");


		for (int i = 0; i < type.length; i++) {
			costObj expctedobj=Expected.get(type[i]);
			costObj actualObj=Generated.get(type[i]);
			try {
				
				
				ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAir(), actualObj.getAir(), " Air rate Validation");
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getHotel(), actualObj.getHotel(), "Hotel rate Validation");

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				
				ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getProgram(), actualObj.getProgram(), " Program rate Validation");

			} catch (Exception e) {
				// TODO: handle exception
			}
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getTotalCost(), actualObj.getTotalCost(), type[i]+" Total Cost Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getTotalCost(), actualObj.getTotalCost(), type[i]+" Total Cost Validation");
			
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAdjustableTotalCost(), actualObj.getAdjustableTotalCost(), type[i]+" Adjusted Total Cost Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAdjustableTotalCost(), actualObj.getAdjustableTotalCost(), type[i]+" Adjusted Total Cost Validation");
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getTotalTaxValue(), actualObj.getTotalTaxValue(), type[i]+" Total tax value Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getTotalTaxValue(), actualObj.getTotalTaxValue(),type[i]+" Total tax value Validation");
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getProfitMarkupAmount(), actualObj.getProfitMarkupAmount(), type[i]+" Profit Mark up Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getProfitMarkupAmount(), actualObj.getProfitMarkupAmount(),type[i]+" Profit Mark up Validation");
			
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getProposedSellRate(), actualObj.getProposedSellRate(), type[i]+" Proposed sell Rate Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getProposedSellRate(), actualObj.getProposedSellRate(), type[i]+" Proposed sell Rate Validation");
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getFinalSellRate(), actualObj.getFinalSellRate(), type[i]+" Final Sell rate Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getFinalSellRate(), actualObj.getFinalSellRate(), type[i]+" Final Sell rate Validation");
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAgentCommissionableAmount(), actualObj.getAgentCommissionableAmount(), type[i]+" Agent Commissionable amount Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAgentCommissionableAmount(), actualObj.getAgentCommissionableAmount(), type[i]+" Agent Commissionable amount Validation");
			
			
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAgentCommissionAmount(), actualObj.getAgentCommissionAmount(), type[i]+" Agent Commission Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAgentCommissionAmount(), actualObj.getAgentCommissionAmount(), type[i]+" Agent Commission Validation");
			
			
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAgentNetProfit(), actualObj.getAgentNetProfit(), type[i]+" Agent Net profit Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAgentNetProfit(), actualObj.getAgentNetProfit(), type[i]+" Agent Net profit Validation");

			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAffiliateCommissionableAmount(), actualObj.getAffiliateCommissionableAmount(), type[i]+" Affiliate Commissionable amount Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAffiliateCommissionableAmount(), actualObj.getAffiliateCommissionableAmount(),type[i]+" Affiliate Commissionable amount Validation");

			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAffiliateCommissionAmount(), actualObj.getAffiliateCommissionAmount(), type[i]+" Affiliate Commission Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAffiliateCommissionAmount(), actualObj.getAffiliateCommissionAmount(), type[i]+" Affiliate Commission Validation");

			PrintWriter.append(verifyTrueNumeric(expctedobj.getAffiliateNetProfit(), actualObj.getAffiliateNetProfit(), type[i]+" Affiliate Net profit Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAffiliateNetProfit(), actualObj.getAffiliateNetProfit(), type[i]+" Affiliate Net profit Validation");


		}
//		PrintWriter.append("</br></br>");


		
//		PrintWriter.append("</table>");
		
//		PrintWriter.append("<p class='InfoSup'>Test Points</p>");
//		PrintWriter.append("<p class='InfoSub'>* Saved  Rate Validations </p>");


	//	PrintWriter.append("</br></br><table ><tr><th>Test Case #</th><th>Test Description</th><th>Expected Result</th><th>Actual Result</th><th>Test Status</th></tr>");


		for (int i = 0; i < type.length; i++) {
			costObj expctedobj=Expected.get(type[i]);
			costObj actualObj=Saved.get(type[i]);
			try {
	//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAir(), actualObj.getAir(), type[i]+" Air rate Validation"));
				ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAir(), actualObj.getAir(), type[i]+" Air rate Validation");

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
	//			PrintWriter.append(verifyTrueNumeric(expctedobj.getHotel(), actualObj.getHotel(), type[i]+" Hotel rate Validation"));
				ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getHotel(), actualObj.getHotel(), type[i]+" Hotel rate Validation");
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
	//			PrintWriter.append(verifyTrueNumeric(expctedobj.getProgram(), actualObj.getProgram(), type[i]+" Program rate Validation"));
				ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getProgram(), actualObj.getProgram(), type[i]+" Program rate Validation");

			} catch (Exception e) {
				// TODO: handle exception
			}
	//		PrintWriter.append(verifyTrueNumeric(expctedobj.getTotalCost(), actualObj.getTotalCost(), type[i]+" Total Cost Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getTotalCost(), actualObj.getTotalCost(), type[i]+" Total Cost Validation");
			
			
			
	//		PrintWriter.append(verifyTrueNumeric(expctedobj.getAdjustableTotalCost(), actualObj.getAdjustableTotalCost(), type[i]+" Adjusted Total Cost Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAdjustableTotalCost(), actualObj.getAdjustableTotalCost(), type[i]+" Adjusted Total Cost Validation");
			
			
			
	//		PrintWriter.append(verifyTrueNumeric(expctedobj.getTotalTaxValue(), actualObj.getTotalTaxValue(), type[i]+" Total tax value Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getTotalTaxValue(), actualObj.getTotalTaxValue(), type[i]+" Total tax value Validation");

			
	//		PrintWriter.append(verifyTrueNumeric(expctedobj.getProfitMarkupAmount(), actualObj.getProfitMarkupAmount(), type[i]+" Profit Mark up Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getProfitMarkupAmount(), actualObj.getProfitMarkupAmount(), type[i]+" Profit Mark up Validation");
			
	//		PrintWriter.append(verifyTrueNumeric(expctedobj.getProposedSellRate(), actualObj.getProposedSellRate(), type[i]+" Proposed sell Rate Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getProposedSellRate(), actualObj.getProposedSellRate(), type[i]+" Proposed sell Rate Validation");
			
			
	//		PrintWriter.append(verifyTrueNumeric(expctedobj.getFinalSellRate(), actualObj.getFinalSellRate(), type[i]+" Final Sell rate Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getFinalSellRate(), actualObj.getFinalSellRate(), type[i]+" Final Sell rate Validation");
					
					
	//		PrintWriter.append(verifyTrueNumeric(expctedobj.getAgentCommissionableAmount(), actualObj.getAgentCommissionableAmount(), type[i]+" Agent Commissionable amount Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAgentCommissionableAmount(), actualObj.getAgentCommissionableAmount(), type[i]+" Agent Commissionable amount Validation");
			
	//		PrintWriter.append(verifyTrueNumeric(expctedobj.getAgentCommissionAmount(), actualObj.getAgentCommissionAmount(), type[i]+" Agent Commission Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAgentCommissionAmount(), actualObj.getAgentCommissionAmount(), type[i]+" Agent Commission Validation");
			
	//		PrintWriter.append(verifyTrueNumeric(expctedobj.getAgentNetProfit(), actualObj.getAgentNetProfit(), type[i]+" Agent Net profit Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAgentNetProfit(), actualObj.getAgentNetProfit(), type[i]+" Agent Net profit Validation");
			
	//		PrintWriter.append(verifyTrueNumeric(expctedobj.getAffiliateCommissionableAmount(), actualObj.getAffiliateCommissionableAmount(), type[i]+" Affiliate Commissionable amount Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAffiliateCommissionableAmount(), actualObj.getAffiliateCommissionableAmount(), type[i]+" Affiliate Commissionable amount Validation");
			
	//		PrintWriter.append(verifyTrueNumeric(expctedobj.getAffiliateCommissionAmount(), actualObj.getAffiliateCommissionAmount(), type[i]+" Affiliate Commission Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAffiliateCommissionAmount(), actualObj.getAffiliateCommissionAmount(), type[i]+" Affiliate Commission Validation");
			
	//		PrintWriter.append(verifyTrueNumeric(expctedobj.getAffiliateNetProfit(), actualObj.getAffiliateNetProfit(), type[i]+" Affiliate Net profit Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAffiliateNetProfit(), actualObj.getAffiliateNetProfit(), type[i]+" Affiliate Net profit Validation");

		}

		PrintWriter.append("</table>");
		

return PrintWriter;


	}
	
	
	public StringBuffer AlertTextLoader(List<Map<String, String>>Expected,List<Map<String, String>> Actual) {
		
		StringBuffer PrintWriter=new StringBuffer();
//		PrintWriter.append("<p>Saving Alert Validation</p>");
//		PrintWriter.append("</br></br><table ><tr><th>Test Case #</th><th>Test Description</th><th>Actual Result</th><th>Expected Result</th><th>Test Status</th></tr>");
		
		
		for (int i = 0; i < Actual.size(); i++) {
	//		PrintWriter.append(verifyTrue(Expected.get(i).get("Alert"),Actual.get(i).get("Alert"),"Verifying the saving Alert for rate index "+(1+i)));
			ExtentReportTemplate.getInstance().verifyTrue(Expected.get(i).get("Alert"),Actual.get(i).get("Alert"),"Verifying the saving Alert for rate index "+(1+i));
			
			
	//		PrintWriter.append(verifyTrue(Expected.get(i).get("Overall"),Actual.get(i).get("Overall"),"Verifying the Overall saving Status for rate index "+(1+i)));
			ExtentReportTemplate.getInstance().verifyTrue(Expected.get(i).get("Overall"),Actual.get(i).get("Overall"),"Verifying the Overall saving Status for rate index "+(1+i));
			
			
		}
		PrintWriter.append("</table>");
		return PrintWriter;
	}

	public StringBuffer ReportBodyLoader(Map<String, costObj>Expected,Map<String, costObj> Actual) {

		StringBuffer PrintWriter=new StringBuffer();

//		PrintWriter.append("</br></br><table ><tr><th>Test Case #</th><th>Test Description</th><th>Actual Result</th><th>Expected Result</th><th>Test Status</th></tr>");
		String[] type={"Single","Double","Triple","Child"};

		for (int i = 0; i < type.length; i++) {
			costObj expctedobj=Expected.get(type[i]);
			costObj actualObj=Actual.get(type[i]);
			try {
		//		PrintWriter.append(verifyTrueNumeric(expctedobj.getAir(), actualObj.getAir(), type[i]+" Air rate Validation"));
				ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAir(), actualObj.getAir(), type[i]+" Air rate Validation");
				

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
//				PrintWriter.append(verifyTrueNumeric(expctedobj.getHotel(), actualObj.getHotel(), type[i]+" Hotel rate Validation"));
				ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getHotel(), actualObj.getHotel(), type[i]+" Hotel rate Validation");

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				PrintWriter.append(verifyTrueNumeric(expctedobj.getProgram(), actualObj.getProgram(), type[i]+" Program rate Validation"));
				ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getProgram(), actualObj.getProgram(), type[i]+" Program rate Validation");

			} catch (Exception e) {
				// TODO: handle exception
			}
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getTotalCost(), actualObj.getTotalCost(), type[i]+" Total Cost Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getTotalCost(), actualObj.getTotalCost(), type[i]+" Total Cost Validation");
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAdjustableTotalCost(), actualObj.getAdjustableTotalCost(), type[i]+" Adjusted Total Cost Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAdjustableTotalCost(), actualObj.getAdjustableTotalCost(), type[i]+" Adjusted Total Cost Validation");
					
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getTotalTaxValue(), actualObj.getTotalTaxValue(), type[i]+" Total tax value Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getTotalTaxValue(), actualObj.getTotalTaxValue(), type[i]+" Total tax value Validation");
					
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getProfitMarkupAmount(), actualObj.getProfitMarkupAmount(), type[i]+" Profit Mark up Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getProfitMarkupAmount(), actualObj.getProfitMarkupAmount(), type[i]+" Profit Mark up Validation");
					
					
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getProposedSellRate(), actualObj.getProposedSellRate(), type[i]+" Proposed sell Rate Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getProposedSellRate(), actualObj.getProposedSellRate(), type[i]+" Proposed sell Rate Validation");
					
					
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getFinalSellRate(), actualObj.getFinalSellRate(), type[i]+" Final Sell rate Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getFinalSellRate(), actualObj.getFinalSellRate(), type[i]+" Final Sell rate Validation");
			
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAgentCommissionableAmount(), actualObj.getAgentCommissionableAmount(), type[i]+" Agent Commissionable amount Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAgentCommissionableAmount(), actualObj.getAgentCommissionableAmount(), type[i]+" Agent Commissionable amount Validation");
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAgentCommissionAmount(), actualObj.getAgentCommissionAmount(), type[i]+" Agent Commission Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAgentCommissionAmount(), actualObj.getAgentCommissionAmount(), type[i]+" Agent Commission Validation");
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAgentNetProfit(), actualObj.getAgentNetProfit(), type[i]+" Agent Net profit Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAgentNetProfit(), actualObj.getAgentNetProfit(), type[i]+" Agent Net profit Validation");
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAffiliateCommissionableAmount(), actualObj.getAffiliateCommissionableAmount(), type[i]+" Affiliate Commissionable amount Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAffiliateCommissionableAmount(), actualObj.getAffiliateCommissionableAmount(), type[i]+" Affiliate Commissionable amount Validation");
			
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAffiliateCommissionAmount(), actualObj.getAffiliateCommissionAmount(), type[i]+" Affiliate Commission Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAffiliateCommissionAmount(), actualObj.getAffiliateCommissionAmount(), type[i]+" Affiliate Commission Validation");
			
//			PrintWriter.append(verifyTrueNumeric(expctedobj.getAffiliateNetProfit(), actualObj.getAffiliateNetProfit(), type[i]+" Affiliate Net profit Validation"));
			ExtentReportTemplate.getInstance().verifyTrue(expctedobj.getAffiliateCommissionAmount(), actualObj.getAffiliateCommissionAmount(), type[i]+" Affiliate Commission Validation");
			
			PrintWriter.append("</table>");
		}
		return PrintWriter;

	}

	public StringBuffer verifyTrueNumeric(int Expected,int Actual,String Message)
	{
		testCaseCount++;
		StringBuffer PrintWriter=new StringBuffer();
		PrintWriter.append("<tr><td>"+testCaseCount+"</td><td>"+Message+"</td>");
		//	TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {

			if(((Expected-1)<=Actual) && (Actual<=(Expected+1))){
				PrintWriter.append("<td class='Passed'>PASS</td>");
			}
			else{
				PrintWriter.append("<td class='Failed'>FAIL</td>");
			}

			PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}

		return PrintWriter;
	}
	
	public StringBuffer verifyTrue(String Expected,String Actual,String Message)
	{
		StringBuffer PrintWriter=new StringBuffer();
		testCaseCount++;
		System.out.println(testCaseCount);
		PrintWriter.append("<tr><td>"+testCaseCount+"</td><td>"+Message+"</td>");

		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {

			if(Actual.trim().equalsIgnoreCase(Expected.trim())){
				PrintWriter.append("<td class='Passed'>PASS</td>");
				//passcount++;
				}
			else{
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				//failcount++;
				}

			PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}

		return PrintWriter;
	}
}
