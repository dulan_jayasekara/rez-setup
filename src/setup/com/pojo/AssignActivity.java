package setup.com.pojo;

public class AssignActivity {
	
	public String ActivityProgramName_AssignActivity;
	public String Activity_Type;
	public String Activity_Period;
	public String Activity_RatePlan;
	public String Min_Max_Rate;
	public String Combination_Active;
	public String Display_Order;
	public String getActivityProgramName_AssignActivity() {
		return ActivityProgramName_AssignActivity;
	}
	public void setActivityProgramName_AssignActivity(
			String activityProgramName_AssignActivity) {
		ActivityProgramName_AssignActivity = activityProgramName_AssignActivity;
	}
	public String getActivity_Type() {
		return Activity_Type;
	}
	public void setActivity_Type(String activity_Type) {
		Activity_Type = activity_Type;
	}
	public String getActivity_Period() {
		return Activity_Period;
	}
	public void setActivity_Period(String activity_Period) {
		Activity_Period = activity_Period;
	}
	public String getActivity_RatePlan() {
		return Activity_RatePlan;
	}
	public void setActivity_RatePlan(String activity_RatePlan) {
		Activity_RatePlan = activity_RatePlan;
	}
	public String getMin_Max_Rate() {
		return Min_Max_Rate;
	}
	public void setMin_Max_Rate(String min_Max_Rate) {
		Min_Max_Rate = min_Max_Rate;
	}
	public String getCombination_Active() {
		return Combination_Active;
	}
	public void setCombination_Active(String combination_Active) {
		Combination_Active = combination_Active;
	}
	public String getDisplay_Order() {
		return Display_Order;
	}
	public void setDisplay_Order(String display_Order) {
		Display_Order = display_Order;
	}
	
	

}
