package setup.com.pojo;

import setup.com.enumtypes.ChargeByType;

public class ProfitMarkup {
	
private String BookingChannel                     = "ALL";
private String PartnerType                        = "Direct";
private ChargeByType ChargeType                   = null;
private String ApplyProfitMarkupTo                = "ALL";
private boolean OverriteSpecific                  = false;
private String AdultProfitMarkup                  = "";
private String AdditionalAdultProfitMarkup        = "";
private String ChildProfitMarkup                  = "";
private String From                               = "";
private String To                                 = "";
private String ApplicablePattern                  = "";



public String getApplicablePattern() {
	return ApplicablePattern;
}
public void setApplicablePattern(String applicablePattern) {
	ApplicablePattern = applicablePattern;
}
public String getBookingChannel() {
	return BookingChannel;
}
public void setBookingChannel(String bookingChannel) {
	BookingChannel = bookingChannel;
}
public String getPartnerType() {
	return PartnerType;
}
public void setPartnerType(String partnerType) {
	PartnerType = partnerType;
}
public ChargeByType getChargeType() {
	return ChargeType;
}
public void setChargeType(ChargeByType chargeType) {
	ChargeType = chargeType;
}
public String getApplyProfitMarkupTo() {
	return ApplyProfitMarkupTo;
}
public void setApplyProfitMarkupTo(String applyProfitMarkupTo) {
	ApplyProfitMarkupTo = applyProfitMarkupTo;
}
public boolean isOverriteSpecific() {
	return OverriteSpecific;
}
public void setOverriteSpecific(boolean overriteSpecific) {
	OverriteSpecific = overriteSpecific;
}
public String getAdultProfitMarkup() {
	return AdultProfitMarkup;
}
public void setAdultProfitMarkup(String adultProfitMarkup) {
	AdultProfitMarkup = adultProfitMarkup;
}
public String getAdditionalAdultProfitMarkup() {
	return AdditionalAdultProfitMarkup;
}
public void setAdditionalAdultProfitMarkup(String additionalAdultProfitMarkup) {
	AdditionalAdultProfitMarkup = additionalAdultProfitMarkup;
}
public String getChildProfitMarkup() {
	return ChildProfitMarkup;
}
public void setChildProfitMarkup(String childProfitMarkup) {
	ChildProfitMarkup = childProfitMarkup;
}
public String getFrom() {
	return From;
}
public void setFrom(String from) {
	From = from;
}
public String getTo() {
	return To;
}
public void setTo(String to) {
	To = to;
}




}
