package setup.com.loaders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import setup.com.pojo.*;

public class LoadBasicDetails {

	Map<Integer, String> LoginMap = null;
	Map<Integer, String> HotelBedMap = null;
	Map<Integer, String> HotelGroupMap = null;
	Map<Integer, String> HotelRateMap = null;
	Map<Integer, String> HotelRoomTypeMap = null;
	Map<Integer, String> HotelStarMap = null;

	public LoadBasicDetails(ArrayList<Map<Integer, String>> sheetlist) {
		LoginMap = sheetlist.get(0);
		HotelBedMap = sheetlist.get(1);
		HotelGroupMap = sheetlist.get(2);
		HotelRateMap = sheetlist.get(3);
		HotelRoomTypeMap = sheetlist.get(4);
		HotelStarMap = sheetlist.get(5);
	}

	// Bed Types
	public ArrayList<BedType> loadBedTypes() {

		Iterator<Map.Entry<Integer, String>> it = HotelBedMap.entrySet().iterator();
		ArrayList<BedType> BedTypeList = new ArrayList<BedType>();

		while (it.hasNext()) {
			BedType bedtype = new BedType();
			String[] values = it.next().getValue().split(",");

			bedtype.setBedTypeName(values[0]);
			bedtype.setBed_DefaultAdults(values[1]);
			bedtype.setBed_Desc(values[2]);
			BedTypeList.add(bedtype);

		}

		return BedTypeList;
	}

	// Hotel Group
	public ArrayList<HotelGroup> loadHotelGroups() {

		Iterator<Map.Entry<Integer, String>> it = HotelGroupMap.entrySet().iterator();
		ArrayList<HotelGroup> HotelGroupList = new ArrayList<HotelGroup>();

		while (it.hasNext()) {
			HotelGroup grouptype = new HotelGroup();
			String[] values = it.next().getValue().split(",");

			grouptype.setHotelGroup(values[0]);
			HotelGroupList.add(grouptype);

		}

		return HotelGroupList;
	}

	// Hotel rate plan
	public ArrayList<RatePlan> loadRateTypes() {

		Iterator<Map.Entry<Integer, String>> it = HotelRateMap.entrySet().iterator();
		ArrayList<RatePlan> loadRateList = new ArrayList<RatePlan>();

		while (it.hasNext()) {
			RatePlan ratetype = new RatePlan();
			String[] values = it.next().getValue().split(",");

			ratetype.setHotelRate(values[0]);
			loadRateList.add(ratetype);

		}

		return loadRateList;
	}

	// RoomType
	public ArrayList<RoomType> loadRoomTypes() {

		Iterator<Map.Entry<Integer, String>> it = HotelRoomTypeMap.entrySet().iterator();
		ArrayList<RoomType> RoomTypeList = new ArrayList<RoomType>();

		while (it.hasNext()) {
			RoomType roomtype = new RoomType();
			String[] values = it.next().getValue().split(",");
			try {
				roomtype.setRoom_type(values[0]);
				roomtype.setRoom_type_Desc(values[1]);
				roomtype.setRoom_Acc_Type(values[2]);
				RoomTypeList.add(roomtype);
			} catch (Exception e) {
				System.out.println(e.toString());
			}

		}

		return RoomTypeList;
	}

	// Hotel Star
	public ArrayList<StarCategory> loadStartTypes() {

		Iterator<Map.Entry<Integer, String>> it = HotelStarMap.entrySet().iterator();
		ArrayList<StarCategory> StarTypeList = new ArrayList<StarCategory>();

		while (it.hasNext()) {
			StarCategory starCategory = new StarCategory();
			String[] values = it.next().getValue().split(",");

			starCategory.setHotel_star(values[0]);
			starCategory.setHotel_star_order(values[1]);
			StarTypeList.add(starCategory);

		}

		return StarTypeList;

	}

}
