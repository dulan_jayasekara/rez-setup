package setup.com.loaders;

import setup.com.pojo.AirConfig;
import setup.com.pojo.Currency;
import setup.com.pojo.Discount;
import setup.com.pojo.Hotel;
import setup.com.pojo.HotelBookingFee;
import setup.com.pojo.HotelContract;
import setup.com.pojo.HotelPolicy;
import setup.com.pojo.HotelPromotion;
import setup.com.pojo.HotelRoom;
import setup.com.pojo.HotelSupplementary;
import setup.com.pojo.HotelTax;
import setup.com.pojo.ProfitMarkup;
import setup.com.readers.ReadExcel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import setup.com.dataObjects.ActivityDetails;
import setup.com.dataObjects.ContentObj;
import setup.com.dataObjects.HotelDetails;
import setup.com.dataObjects.ItineraryObject;
import setup.com.dataObjects.RateObjectSet;
import setup.com.dataObjects.RatesObject;
import setup.com.dataObjects.RegionRates;
import setup.com.dataObjects.assignAir;
import setup.com.dataObjects.standardinfoObject;
import setup.com.enumtypes.BookingChannelType;
import setup.com.enumtypes.ChargeByType;
import setup.com.enumtypes.ConfigFareType;
import setup.com.enumtypes.DiscountRateApplicableType;
import setup.com.enumtypes.HotelInventoryType;
import setup.com.enumtypes.InventoryActionType;
import setup.com.enumtypes.PartnerType;
import setup.com.enumtypes.PromotionApplicablePeriodType;
import setup.com.enumtypes.PromotionBasedOnType;
import setup.com.enumtypes.PromotionSpecialRateApplicableType;
import setup.com.enumtypes.PromotionType;
import setup.com.runners.Flow.ThirdpartySuplierPMSetupFlow;
import setup.com.pojo.ThirdpartySupplierPM;

public class DataLoader {

	org.apache.log4j.Logger dataLogger = null;

	public DataLoader() {
		dataLogger = org.apache.log4j.Logger.getLogger(this.getClass());
	}

	public TreeMap<String, Hotel> loadHotelDetails(Map<Integer, String> map) {
		TreeMap<String, Hotel> hotelList = new TreeMap<String, Hotel>();

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			Hotel CurrentHotel = new Hotel();
			String[] AllValues = it.next().getValue().split(",");

			System.out.println(AllValues[1]);

			CurrentHotel.setHotelName(AllValues[0]);
			CurrentHotel.setSupplier(AllValues[1]);
			CurrentHotel.setHotelCurrency(AllValues[2]);
			CurrentHotel.setAddressLine1(AllValues[3]);
			CurrentHotel.setAddressLine2(AllValues[4]);
			CurrentHotel.setCountry(AllValues[5]);
			CurrentHotel.setCity(AllValues[7]);
			CurrentHotel.setState(AllValues[6].trim());
			CurrentHotel.setHotelGroup(AllValues[8]);
			CurrentHotel.setStarCategory(AllValues[9]);
			CurrentHotel.setFeaturedStatus(AllValues[10]);
			CurrentHotel.setPassportrequired(AllValues[11]);
			CurrentHotel.setDisplayinCC(AllValues[12]);
			CurrentHotel.setDisplayinWeb(AllValues[13]);
			CurrentHotel.setChildrenAllowed(AllValues[14]);
			CurrentHotel.setChargesFreeAgeFrom(AllValues[15]);
			CurrentHotel.setChargesFreeAgeTo(AllValues[16]);
			CurrentHotel.setChildRateApplicableFrom(AllValues[17]);
			CurrentHotel.setChildRateApplicableTo(AllValues[18]);
			CurrentHotel.setStandardCheckIn(AllValues[19]);
			CurrentHotel.setStandardCheckOut(AllValues[20]);
			CurrentHotel
					.setInventoryObtainedType(setup.com.enumtypes.InventoryObtainedByType
							.getinveType(AllValues[21]));
			CurrentHotel.setRateSetUpByType(setup.com.enumtypes.RateSetupBy
					.getRateSetupType(AllValues[22]));
			CurrentHotel.setRateContract(setup.com.enumtypes.RateContractByType
					.getRateContractType(AllValues[23]));
			CurrentHotel.setHotelContractFromDate(AllValues[24]);
			CurrentHotel.setHotelContractToDate(AllValues[25]);
			CurrentHotel.setCommHotelType(AllValues[26].trim());
			CurrentHotel.setShortDescription(AllValues[27].trim());
			CurrentHotel.setLongDescription(AllValues[28].trim());
			CurrentHotel.setLatitude(AllValues[29].trim());
			CurrentHotel.setLongitude(AllValues[30].trim());
			CurrentHotel.setFilter(AllValues[31].trim());
			CurrentHotel.setPath(AllValues[32].trim());
			CurrentHotel.setSetupNeeded(AllValues[33].trim());

			System.out.println(AllValues[0] + "--->" + AllValues[32]);

			hotelList.put(AllValues[0], CurrentHotel);
		}
		return hotelList;

	}

	public TreeMap<String, Hotel> loadRoomDetails(Map<Integer, String> map,TreeMap<String, Hotel> HotelList) {

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			HotelRoom Room = new HotelRoom();
			String[] AllValues = it.next().getValue().split(",");
			Room.setRoomType(AllValues[1]);
			Room.setBedType(AllValues[2]);
			Room.setRatePlan(AllValues[3]);
			Room.setStdAdults(AllValues[4]);
			Room.setAAdults(AllValues[5]);
			Room.setChildren(AllValues[6]);

			if (AllValues[7].equalsIgnoreCase("yes"))
				Room.setMultipleChildRatesApplied(true);
			else
				Room.setMultipleChildRatesApplied(false);

			if (AllValues[8].equalsIgnoreCase("yes"))
				Room.setCombinationActive(true);
			else
				Room.setCombinationActive(false);

			Room.setRegion(AllValues[9]);
			Room.setTourOperator(AllValues[10]);
			Room.setTotalRooms(AllValues[11]);
			Room.setMinimumNightsStay(AllValues[12]);
			Room.setMaximumNightsStay(AllValues[13]);
			Room.setCutOff(AllValues[14]);
			Room.setNetRate(AllValues[15]);
			Room.setAdditionalAdultRate(AllValues[16]);
			Room.setChildNetRate(AllValues[17]);

			try {
				HotelList.get(AllValues[0]).AddRoom(Room);
			} catch (Exception e) {
				dataLogger.warn(AllValues[0] + " not found in the hotel list");
			}

		}
		return HotelList;

	}

	public TreeMap<String, Hotel> loadContractDetails(Map<Integer, String> map,
			TreeMap<String, Hotel> HotelList) throws ParseException {

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			HotelContract Contract = new HotelContract();
			String[] AllValues = it.next().getValue().split(",");
			/*
			 * Contract.setInventoryObtainedType(com.types.InventoryObtainedByType
			 * .getinveType(AllValues[1]));
			 * Contract.setRateSetUpByType(com.types
			 * .RateSetupBy.getRateSetupType(AllValues[2]));
			 * Contract.setRateContract
			 * (com.types.RateContractByType.getRateContractType(AllValues[3]));
			 */
			Contract.setInventoryType(HotelInventoryType
					.getInventoryType(AllValues[1]));
			Contract.setSearchSatistied(InventoryActionType
					.getInventoryActionType(AllValues[2]));
			Contract.setInventoryExhausted(InventoryActionType
					.getInventoryActionType(AllValues[3]));
			Contract.setCutoffApplied(InventoryActionType
					.getInventoryActionType(AllValues[4]));
			Contract.setMinNightRestriction(InventoryActionType
					.getInventoryActionType(AllValues[5]));
			Contract.setMAxNightRestriction(InventoryActionType
					.getInventoryActionType(AllValues[6]));
			Contract.setBlackout(InventoryActionType
					.getInventoryActionType(AllValues[7]));
			Contract.setNoArrival(InventoryActionType
					.getInventoryActionType(AllValues[8]));

			// Contract.setHotelCommission(AllValues[9]);
			Contract.setCommType(AllValues[12]);
			Contract.setCommvalue(AllValues[13]);

			Contract.setContractFrom(AllValues[10]);
			Contract.setContractTo(AllValues[11]);

			try {
				HotelList.get(AllValues[0]).AddContract(Contract);
			} catch (Exception e) {
				dataLogger.warn(AllValues[0] + " not found in the hotel list");
			}

		}

		return HotelList;

	}

	public TreeMap<String, Hotel> loadPolicyDetails(Map<Integer, String> map,
			TreeMap<String, Hotel> HotelList) {

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			HotelPolicy Policy = new HotelPolicy();
			String[] AllValues = it.next().getValue().split(",");

			Policy.setFrom(AllValues[1]);
			Policy.setTo(AllValues[2]);
			Policy.setArrivalLessThan(AllValues[3]);
			Policy.setCancellatrionBuffer(AllValues[4]);
			Policy.setStdChargeByType(setup.com.enumtypes.ChargeByType
					.getChargeType(AllValues[5].trim()));
			Policy.setStdValue(AllValues[6]);
			Policy.setNoShowChargeByType(setup.com.enumtypes.ChargeByType
					.getChargeType(AllValues[7].trim()));
			Policy.setNoshowValue(AllValues[8]);

			try {
				HotelList.get(AllValues[0]).AddPolicy(Policy);
			} catch (Exception e) {
				dataLogger.warn(AllValues[0] + " not found in the hotel list");
			}

		}
		return HotelList;

	}

	public TreeMap<String, Hotel> loadTaxDetails(Map<Integer, String> map,
			TreeMap<String, Hotel> HotelList) {

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			HotelTax tax = new HotelTax();
			String[] AllValues = it.next().getValue().split(",");

			tax.setFromT(AllValues[1]);
			tax.setToT(AllValues[2]);
			tax.setSalesTaxChargeType(setup.com.enumtypes.ChargeByType
					.getChargeType(AllValues[3].trim()));
			tax.setSalesTaxValue(AllValues[4]);
			tax.setOccupancyChargeType(setup.com.enumtypes.ChargeByType
					.getChargeType(AllValues[5].trim()));
			tax.setOccupancyTaxValue(AllValues[6]);
			tax.setEnergyChargeType(setup.com.enumtypes.ChargeByType
					.getChargeType(AllValues[7].trim()));
			tax.setEnergyTaxValue(AllValues[8]);
			tax.setMiscellaneousFees(AllValues[9]);

			try {
				HotelList.get(AllValues[0].trim()).AddTax(tax);
			} catch (Exception e) {
				dataLogger.warn(AllValues[0] + " not found in the hotel list");
			}

		}
		return HotelList;

	}

	public TreeMap<String, Hotel> loadMarkupDetails(Map<Integer, String> map,
			TreeMap<String, Hotel> HotelList) {

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			ProfitMarkup MarkUp = new ProfitMarkup();
			String[] AllValues = it.next().getValue().split(",");

			MarkUp.setBookingChannel(AllValues[1]);
			MarkUp.setPartnerType(AllValues[2]);
			MarkUp.setChargeType(ChargeByType.getChargeType(AllValues[3]));
			MarkUp.setApplyProfitMarkupTo(AllValues[4]);

			if (AllValues[5].equalsIgnoreCase("yes"))
				MarkUp.setOverriteSpecific(true);
			else
				MarkUp.setOverriteSpecific(false);

			MarkUp.setApplicablePattern(AllValues[6]);
			MarkUp.setAdultProfitMarkup(AllValues[7]);
			MarkUp.setAdditionalAdultProfitMarkup(AllValues[8]);
			MarkUp.setChildProfitMarkup(AllValues[9]);
			MarkUp.setFrom(AllValues[10]);
			MarkUp.setTo(AllValues[11]);

			try {
				HotelList.get(AllValues[0]).AddMarkup(MarkUp);
			} catch (Exception e) {
				dataLogger.warn(AllValues[0] + " not found in the hotel list");
			}

		}
		return HotelList;

	}

	public ArrayList<setup.com.pojo.Supplier> loadSupplierDetails(
			Map<Integer, String> map) {
		ArrayList<setup.com.pojo.Supplier> SupplierList = new ArrayList<setup.com.pojo.Supplier>();
		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			setup.com.pojo.Supplier sup = new setup.com.pojo.Supplier();
			String[] AllValues = it.next().getValue().split(",");

			sup.setSupplierCode(AllValues[0]);
			sup.setSupplierName(AllValues[1]);
			sup.setSupplierType(AllValues[2]);
			sup.setAddress(AllValues[3]);
			sup.setCountry(AllValues[4]);
			sup.setCity(AllValues[5]);
			sup.setActive(AllValues[6]);
			sup.setCurrency(AllValues[7]);
			sup.setContactName(AllValues[8]);
			sup.setEmail(AllValues[9]);
			sup.setContactMedia(AllValues[10]);
			sup.setContactType(AllValues[11]);

			SupplierList.add(sup);

		}
		return SupplierList;

	}

	public ArrayList<Currency> loadCurrencyDetails(Map<Integer, String> map) {

		ArrayList<Currency> currencySetupList = new ArrayList<Currency>();
		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			Currency currency = new Currency();
			String[] AllValues = it.next().getValue().split(",");

			currency.setCurrencyCode(AllValues[0]);
			currency.setCurrrencyName(AllValues[1]);
			currency.setBuyingRate(AllValues[2]);
			currency.setSellingRate(AllValues[3]);

			currencySetupList.add(currency);

		}
		return currencySetupList;

	}

	public ArrayList<ThirdpartySupplierPM> loadthirdpartysupplierpm(Map<Integer, String> map){
		
		
		ArrayList<ThirdpartySupplierPM> thirdparty_hotel_activitymarkuplist= new ArrayList<ThirdpartySupplierPM>();
		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();
		
		while (it.hasNext()) {
			
			ThirdpartySupplierPM thirdpartyPM = new ThirdpartySupplierPM();
			String[] allvalues = it.next().getValue().split(",");
			
			thirdpartyPM.setBookingChannel(allvalues[0]);
			thirdpartyPM.setPartner(allvalues[1]);
			thirdpartyPM.setRegion(allvalues[2]);
			thirdpartyPM.setProfitMarkupType(allvalues[3]);
			thirdpartyPM.setProfitMarkupPeriodFrom(allvalues[4]);
			thirdpartyPM.setProfitMarkupPeriodTo(allvalues[5]);
			thirdpartyPM.setThirdPartySupplier(allvalues[6]);
			thirdpartyPM.setCountry(allvalues[7]);
			thirdpartyPM.setCity(allvalues[8]);
			thirdpartyPM.setOverwriteSpecificMarkup(allvalues[9].trim().equalsIgnoreCase("Yes") ? true : false);
			thirdpartyPM.setFixProfitMarkupDayplan(allvalues[10]);
			thirdpartyPM.setProfitMarkup(allvalues[11]);
			
			thirdparty_hotel_activitymarkuplist.add(thirdpartyPM);
			
		}
		
		return thirdparty_hotel_activitymarkuplist;
	}
	
	public ArrayList<HotelBookingFee> loadbookingfeedetails(Map<Integer, String> bookingfeemap) {

		ArrayList<HotelBookingFee> bookingfeeSetupList = new ArrayList<HotelBookingFee>();
		Iterator<Map.Entry<Integer, String>> it = bookingfeemap.entrySet().iterator();

		

		while (it.hasNext()) {

			HotelBookingFee bookingfee = new HotelBookingFee();
			String[] AllValues = it.next().getValue().split(",");

			bookingfee.setCountryName(AllValues[0]);
			bookingfee.setCityName(AllValues[1]);
			bookingfee.setTax(AllValues[2]);

			bookingfee.setAmount(AllValues[3]);

			bookingfeeSetupList.add(bookingfee);

		}

		return bookingfeeSetupList;
	}
	
	
	public ArrayList<AirConfig> loadairconfigdetails(Map<Integer, String>  airconfigmap) {
		
		ArrayList<AirConfig> airconfigSetupList = new ArrayList<AirConfig>();
		
		Iterator<Map.Entry<Integer, String>> it = airconfigmap.entrySet().iterator();

		

		while (it.hasNext()) {

			AirConfig obj = new AirConfig();
			String[] data = it.next().getValue().split(",");

			obj.setTestNo(Integer.parseInt(data[0]));
			obj.setApplyFareTypeConfigurations(Boolean.parseBoolean(data[1]));
			obj.setApplyPaymentOptionConfigurations(Boolean.parseBoolean(data[2]));
			obj.setApplyAirChargesConfigurations(Boolean.parseBoolean(data[3]));
			
			obj.setAirLine(data[4]);
			
			if(data[5].equals(ConfigFareType.Booking_Fee.toString()))
			{
				obj.setPubFareType(ConfigFareType.Booking_Fee);
			}
			else if(data[5].equals(ConfigFareType.Profit_Markup.toString()))
			{
				obj.setPubFareType(ConfigFareType.Profit_Markup);
			}
			else if(data[5].equals(ConfigFareType.Both.toString()))
			{
				obj.setPubFareType(ConfigFareType.Both);
			}
			else if(data[5].equals(ConfigFareType.None.toString()))
			{
				obj.setPubFareType(ConfigFareType.None);
			}
			
			if(data[6].equals(ConfigFareType.Booking_Fee.toString()))
			{
				obj.setPvtFareType(ConfigFareType.Booking_Fee);
			}
			else if(data[6].equals(ConfigFareType.Profit_Markup.toString()))
			{
				obj.setPvtFareType(ConfigFareType.Profit_Markup);
			}
			else if(data[6].equals(ConfigFareType.Both.toString()))
			{
				obj.setPvtFareType(ConfigFareType.Both);
			}

			obj.setFTypeConfgShopCart(data[7]);
			obj.setFTypeConfgFplusH(data[8]);
			obj.setFTypeConfgFixPack(data[9]);
			obj.setFlightPayOptCartBooking(data[10]);
			obj.setFlightPayOptPayfullFplusH(data[11]);
			
			airconfigSetupList.add(obj);

		}
		
		return airconfigSetupList;
		
	}
	
	
    public TreeMap<String,standardinfoObject> loadFPObjects(Map<Integer, String>  infolist,String Url) {
    	
    	TreeMap<String, standardinfoObject> FpObjectList = new TreeMap<String, standardinfoObject>();
		
		Iterator<Map.Entry<Integer, String>> it = infolist.entrySet().iterator();

		while (it.hasNext()) {

			standardinfoObject obj=new standardinfoObject();
			String[] valueArray = it.next().getValue().split(",");
			
			
			//obj.url=valueArray[0];
		    obj.url= Url;
//TODO: Refactor this part properly
					
			obj.packageType=valueArray[1];
			obj.packageName=valueArray[2];
			obj.packageDays=valueArray[3];
			obj.ProductIncludes=valueArray[4];
			obj.CostBy=valueArray[5];
			obj.ChildAllowed=valueArray[6];
			obj.StayForm=valueArray[7];
			obj.StayTo=valueArray[8];
			obj.BookingFrom=valueArray[9];
			obj.BookingTO=valueArray[10];
			obj.OriginDefinition=valueArray[11];
			obj.SelectedOriginCountry=valueArray[12];
			obj.SelectedOriginCity=valueArray[13];
			obj.DestinationCountry=valueArray[14];
			obj.DestinationCity=valueArray[15];
			obj.Cancellationrefundtype=valueArray[16];
			obj.CancellationpolicyDateArray=valueArray[17].split("_");
			obj.Cancellationpolicytype=valueArray[18];
			obj.CancellationChargeArray=valueArray[19].split("_");
			obj.NoshowType=valueArray[20];
			obj.Noshowcharge=valueArray[21];
			obj.NoofGroupings=valueArray[22];
			obj.GroupingHeaders=valueArray[23];
			obj.DisplayRanking=valueArray[24];
			obj.DCPMType=valueArray[25];
			obj.DCPM=valueArray[26];
			obj.TOPMType=valueArray[27];
			obj.TOPM=valueArray[28];
			obj.AFFPMType	=valueArray[29];
			obj.AFFPM=valueArray[30];
			obj.TOComType=valueArray[31];
			obj.TOCom=valueArray[32];
			obj.AffComType=valueArray[33];
			obj.AffComType=valueArray[34];
			obj.DepartureBasedOn=valueArray[35];
			obj.SelectedDates=valueArray[36];
			//System.out.println(valueArray[37]);
			
			try {
				ArrayList<String> regionlist=new ArrayList<String>();
				int regionlength=valueArray[37].split("_").length;
							for (int i = 0; i < regionlength; i++) {
					regionlist.add(valueArray[37].split("_")[i]);
					
				}
				
				obj.setRegionArray(regionlist);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			

		    
			FpObjectList.put(obj.packageName,obj);

		}
		
		return FpObjectList;
		
	}
	

    public TreeMap<String,standardinfoObject> loadFPAirInfo(Map<Integer, String>  infolist,TreeMap<String, standardinfoObject> FpObjectList) {
		
 		Iterator<Map.Entry<Integer, String>> it = infolist.entrySet().iterator();
        
 		
		while(it.hasNext())
		{
		String[] allvalues=it.next().getValue().split(",");
		assignAir air=new assignAir();
			
			air.packageName=allvalues[0];
			air.originAir=allvalues[1];
			air.DestinationAir=allvalues[2];
			air.CurrencyCode=allvalues[3];
			air.AdultFare=allvalues[4];
			air.AdultTax=allvalues[5];
			air.childFare=allvalues[6];
			air.childTax=allvalues[7];
			
			FpObjectList.get(air.packageName).addToAirMap(air.originAir, air);
			
		}
		
 		
 		return FpObjectList;
 		
 	}
 	
    
    public TreeMap<String,standardinfoObject> loadFPHotelInfo(Map<Integer, String>  infolist,Map<Integer, String>  ratesList,TreeMap<String, standardinfoObject> FpObjectList) throws ParseException {
		
    	// Bringing the region rates
    	
    	TreeMap<String, Map<String, RegionRates>> RegionalRateMap = loadRegionRates(ratesList);
    	
    	//
    	
 		Iterator<Map.Entry<Integer, String>> it = infolist.entrySet().iterator();
   
	    while(it.hasNext())
		{
			String[]   AllValues     = it.next().getValue().split(",");
			
			HotelDetails obj=new HotelDetails();

			obj.setDay(AllValues[1]);
			System.out.println("******"+AllValues[2]);
			obj.setNoOfDays(AllValues[2]);
			obj.setHotel(AllValues[3]);
			obj.setRateType(AllValues[4]);
			
			
			
			
			obj.setSalesTax(AllValues[5]);
			obj.setOccupancyTax(AllValues[6]);
			obj.setEnergyTax(AllValues[7]);
			obj.setMiscellaneousFee(AllValues[8]);
			
			obj.setRoomType(AllValues[9]);
			obj.setBedType(AllValues[10]);
			obj.setRatePlan(AllValues[11]);
			System.out.println("setRoomType" +obj.getRoomType());
			
			
			obj.setStdAdultCount(AllValues[12]);	
			obj.setAddAdultCount(AllValues[13]);
			obj.setChildCount(AllValues[14]);	
			
			System.out.println("obj.getRoomType()"+obj.getRoomType().trim()+"*****");
		    String Key = obj.getHotel()+"_"+obj.getRoomType();
	        obj.setRegionRates(RegionalRateMap.get(Key));
		    FpObjectList.get(AllValues[0]).getHotellist().add(obj);
			
		}




	 //   FpObjectList.get(AllValues[0]).getHotellist().add(obj);
 		return FpObjectList;
 		
 	}
    
    
  public TreeMap<String, Map<String, RegionRates>> loadRegionRates(Map<Integer, String>  infolist) throws ParseException {
	  
	Iterator<Map.Entry<Integer, String>> it = infolist.entrySet().iterator();
    TreeMap<String,Map<String, RegionRates>> regionlist=new TreeMap<String,Map<String, RegionRates>>();

	while(it.hasNext())
	{
		String[]   AllValues     = it.next().getValue().split(",");
        String Key = AllValues[0]+"_"+AllValues[1];
        
        RatesObject obj = new RatesObject();
        obj.setContractFromDate(AllValues[3]);
        obj.setContractToDate(AllValues[4]);
        obj.setStdRate(AllValues[5]);
     
     
        Map<String, RegionRates>  RegionRateList  = regionlist.get(Key);
        if(null == RegionRateList){
        	RegionRateList = new HashMap<String, RegionRates>();
        	RegionRates rates = new RegionRates();
        	rates.setRegion(AllValues[2]);
        	rates.getRatelist().add(obj);
        	RegionRateList.put(rates.getRegion(), rates);
        	regionlist.put(Key, RegionRateList);
        	
        }else {
        	RegionRates regionrates = RegionRateList.get(AllValues[2]);
        	if(null == regionlist.get(Key)){
        		regionrates = new RegionRates();
        		regionrates.setRegion(AllValues[2]);
        		regionrates.getRatelist().add(obj);
            	RegionRateList.put(regionrates.getRegion(), regionrates);
        	 }else {
        		 RegionRateList.get(AllValues[2]).getRatelist().add(obj);
        	 }
        	
        }
        }
	
	return regionlist;
  }
    
  
  
  
  /////////////////////////////////////////////////////////////////
  
  
  public TreeMap<String,standardinfoObject> loadFPActivityInfo(Map<Integer, String>  infolist,Map<Integer, String>  ratesList,TreeMap<String, standardinfoObject> FpObjectList) throws ParseException {
		
  	// Bringing the region rates
  	
  	TreeMap<String, Map<String, RegionRates>> RegionalRateMap = loadRegionRates(ratesList);
  	
  	//
  	
		Iterator<Map.Entry<Integer, String>> it = infolist.entrySet().iterator();
 
	    while(it.hasNext())
		{
			String[]   AllValues     = it.next().getValue().split(",");
			
			ActivityDetails obj=new ActivityDetails();
			

			obj.setDay(AllValues[1]);
			obj.setProgram(AllValues[2]);
			obj.setActivityName(AllValues[3]);
			obj.setPeriod(AllValues[4]);
			obj.setRateplan(AllValues[5]);
			obj.setSalesTax(AllValues[6]);
			obj.setMiscellaneousTax(AllValues[7]);
			
		    System.out.println("obj.getProgram())"+obj.getProgram().trim());
		    String Key = obj.getProgram()+"_"+obj.getActivityName();
		    obj.setRegionRatemap(RegionalRateMap.get(Key));
	       
		    FpObjectList.get(AllValues[0]).getActivitylist().add(obj);
			
		}




	 //   FpObjectList.get(AllValues[0]).getHotellist().add(obj);
		return FpObjectList;
		
	}
  
  
public TreeMap<String, Map<String, RegionRates>> loadActRegionRates(Map<Integer, String>  infolist) throws ParseException {
	  
  Iterator<Map.Entry<Integer, String>> it = infolist.entrySet().iterator();
  TreeMap<String,Map<String, RegionRates>> regionlist=new TreeMap<String,Map<String, RegionRates>>();

	while(it.hasNext())
	{
	  String[]   AllValues     = it.next().getValue().split(",");
      String Key = AllValues[0]+"_"+AllValues[1];
      
      RatesObject obj = new RatesObject();
      obj.setContractFromDate(AllValues[3]);
      obj.setContractToDate(AllValues[4]);
      obj.setStdRate(AllValues[5]);
      obj.setAddRate(AllValues[6]);
      obj.setChildRate(AllValues[7]);
   
      Map<String, RegionRates>  RegionRateList  = regionlist.get(Key);
      if(null == RegionRateList){
      	RegionRateList = new HashMap<String, RegionRates>();
      	RegionRates rates = new RegionRates();
      	rates.setRegion(AllValues[2]);
      	rates.getRatelist().add(obj);
      	RegionRateList.put(rates.getRegion(), rates);
      	regionlist.put(Key, RegionRateList);
      	
      }else {
      	RegionRates regionrates = RegionRateList.get(AllValues[2]);
      	if(null == regionlist.get(Key)){
      		regionrates = new RegionRates();
      		regionrates.setRegion(AllValues[2]);
      		regionrates.getRatelist().add(obj);
          	RegionRateList.put(regionrates.getRegion(), regionrates);
      	 }else {
      		 RegionRateList.get(AllValues[2]).getRatelist().add(obj);
      	 }
      	
      }
      }
	
	return regionlist;
}
  
  
  
public Map<Integer, ItineraryObject> LoadItenaryDetails(Map<Integer, String> InfoSet) {

	Map<Integer, ItineraryObject> objMap=new TreeMap<Integer, ItineraryObject>();
    Iterator<Map.Entry<Integer, String>> it = InfoSet.entrySet().iterator();

	while(it.hasNext())
	{
		String[]   allvalues     = it.next().getValue().split(",");
        ItineraryObject object=new ItineraryObject();

		object.setDay(allvalues[0]);
		object.setImage(allvalues[1]);
		object.setDescription(allvalues[2]);
		
		objMap.put(Integer.parseInt(allvalues[0]), object);

	}




	return objMap;


}  
  

public TreeMap<String,standardinfoObject> LoadFPItenaryDetails(Map<Integer, String>  infolist,TreeMap<String, standardinfoObject> FpObjectList) throws ParseException {
	
  	// Bringing the region rates
  	
	    Map<Integer, ItineraryObject> ItenaryDetails = LoadItenaryDetails(infolist);
  	    Iterator<Map.Entry<String, standardinfoObject>> it = FpObjectList.entrySet().iterator();
	    
	    while (it.hasNext()) {
			Map.Entry<java.lang.String, setup.com.dataObjects.standardinfoObject> entry = (Map.Entry<java.lang.String, setup.com.dataObjects.standardinfoObject>) it.next();
			Map<Integer, ItineraryObject> ItenaryDetails1 = new HashMap<Integer, ItineraryObject>(); 
			standardinfoObject  obj = entry.getValue();
			
			int Days = Integer.parseInt(obj.packageDays);
			
			for (int i = 0; i < Days; i++) {
				ItenaryDetails1.put((i+1), ItenaryDetails.get(i+1));
				
			}
			obj.setItinerary(ItenaryDetails1);
		}
        

		return FpObjectList;
		
	}


public TreeMap<String,standardinfoObject> LoadFpContent(Map<Integer, String>  infolist,TreeMap<String, standardinfoObject> FpObjectList) throws ParseException {
	
  	// Bringing the region rates
  	
	 Iterator<Entry<Integer, String>> it = infolist.entrySet().iterator();
	    
	    while (it.hasNext()) {
	    String[]   AllValues     = it.next().getValue().split(",");
	    String PackageName = AllValues[0];
	     ContentObj obj = new ContentObj();
		/*if (PackageName.equalsIgnoreCase(contentValues[0])) {*/
		
			obj.setPackageName(AllValues[0]);
			obj.setShortDescription(AllValues[1]);
			obj.setPackageinclusions(AllValues[2]);
			obj.setPackageExclusions(AllValues[3]);
			obj.setPackagedescription(AllValues[4]);
			obj.setSpecialNotes(AllValues[5]);
			obj.setContacts(AllValues[6]);
			obj.setTermsandConditions(AllValues[7]);
			obj.setThumbnail(AllValues[8]);
			obj.setImageGallery(AllValues[9]);
			
			FpObjectList.get(PackageName).setContent(obj);
	    	
	    }
	

		return FpObjectList;
		
	}
  
  ///////////////////////////////////////////////////////////////////
/*  public TreeMap<String,standardinfoObject> loadFPHotelInfo(Map<Integer, String>  infolist,TreeMap<String, standardinfoObject> FpObjectList) {
		
 		Iterator<Map.Entry<Integer, String>> it = infolist.entrySet().iterator();
        RateObjectSet rateset=new RateObjectSet();
		
	    while(it.hasNext())
		{
			String[]   AllValues     = it.next().getValue().split(",");
			
			HotelDetails obj=new HotelDetails();

			obj.setDay(AllValues[1]);
			System.out.println("******"+AllValues[2]);
			obj.setNoOfDays(AllValues[2]);
			obj.setHotel(AllValues[3]);
			obj.setRateType(AllValues[4]);
			
			
			
			
			obj.setSalesTax(AllValues[5]);
			obj.setOccupancyTax(AllValues[6]);
			obj.setEnergyTax(AllValues[7]);
			obj.setMiscellaneousFee(AllValues[8]);
			
			obj.setRoomType(AllValues[9]);
			obj.setBedType(AllValues[10]);
			obj.setRatePlan(AllValues[11]);
			System.out.println("setRoomType" +obj.getRoomType());
			
			
			obj.setStdAdultCount(AllValues[12]);	
			obj.setAddAdultCount(AllValues[13]);
			obj.setChildCount(AllValues[14]);	
			
			System.out.println("obj.getRoomType()"+obj.getRoomType().trim()+"*****");
	//	    obj.setRegionRates(rateset.contractsRead(excelPath, obj.getHotel().trim(), obj.getRoomType().trim()));
			
			FpObjectList.get(AllValues[0]).getHotellist().add(obj);
			
		}





 		return FpObjectList;
 		
 	}*/
    
    public TreeMap<String,standardinfoObject> loadFPActivityInfo(Map<Integer, String>  infolist,TreeMap<String, standardinfoObject> FpObjectList) {
		
 		Iterator<Map.Entry<Integer, String>> it = infolist.entrySet().iterator();
        
 		
		while(it.hasNext())
		{
		String[] allvalues=it.next().getValue().split(",");
		assignAir air=new assignAir();
			
			air.packageName=allvalues[0];
			air.originAir=allvalues[1];
			air.DestinationAir=allvalues[2];
			air.CurrencyCode=allvalues[3];
			air.AdultFare=allvalues[4];
			air.AdultTax=allvalues[5];
			air.childFare=allvalues[6];
			air.childTax=allvalues[7];
			
			FpObjectList.get(air.packageName).addToAirMap(air.originAir, air);
			
		}
		
 		
 		return FpObjectList;
 		
 	}
 	

	public ArrayList<Discount> loadDiscountDetails(Map<Integer, String> map) {
		ArrayList<Discount> DiscountSetupList = new ArrayList<Discount>();
		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			Discount discount = new Discount();
			String[] AllValues = it.next().getValue().split(",");

			discount.setDiscountName(AllValues[0]);
			discount.setStatus(AllValues[1]);
			discount.setBookingChannel((AllValues[2]));

			discount.setPartner(AllValues[2]);
			discount.setDiscountType(AllValues[4]);
			discount.setDiscount(AllValues[5]);
			discount.setValidityPeriod(AllValues[6]);
			discount.setBookingDateFrom(AllValues[7]);
			discount.setBookingDateTo(AllValues[8]);
			discount.setCheckinFrom(AllValues[9]);
			discount.setCheckinTo(AllValues[10]);
			discount.setDiscountApplicableOn(AllValues[11]);
			discount.setDate(AllValues[12]);
			discount.setProductType(AllValues[13]);
			discount.setCouponType(AllValues[14]);
			discount.setComponent(AllValues[15]);
			// edited for picc
			// discount.setfullOrBookingFee = "N/A";
			discount.setCountry(AllValues[16]);
			discount.setCountry2(AllValues[17]);
			discount.setAllOrSelected(AllValues[18]);
			discount.setCity(AllValues[19]);
			discount.setSingleCoupon(AllValues[20]);
			discount.setSequenceCoupon(AllValues[21]);
			discount.setHotels(AllValues[22]);
			discount.setActivities(AllValues[23]);
			discount.setCar(AllValues[24]);
			discount.setNumOfCoupons(AllValues[25]);
			discount.setReusability(AllValues[26]);
			discount.setNumOfTimes(AllValues[27]);
			discount.setExecute(AllValues[28]);
			discount.setDiactivate(AllValues[29]);
			discount.setCheckAvailability(AllValues[30]);
			discount.setCount(AllValues[31]);
			discount.setModify(AllValues[32]);

			DiscountSetupList.add(discount);

		}
		return DiscountSetupList;

	}

	public TreeMap<String, Hotel> loadSupplimentaryDetails(
			Map<Integer, String> map, TreeMap<String, Hotel> HotelList) {

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {

			HotelSupplementary hotelSupplementary = new HotelSupplementary();
			String[] values = it.next().getValue().split(",");

			hotelSupplementary.setSupplementary_Name(values[1]);
		//	hotelSupplementary.setSupplier_Name(values[2]);
			hotelSupplementary.setHotel_Name(values[0]);
			hotelSupplementary.setRoom_Type(values[2]);
			hotelSupplementary.setRegion(values[3]);
			hotelSupplementary.setTour_Operator(values[4]);
			hotelSupplementary.setDate_From(values[5]);
			hotelSupplementary.setDate_To(values[6]);
			hotelSupplementary.setSupplementary_Applicable(values[7]);
			hotelSupplementary.setMendatory(values[8]);

			hotelSupplementary.setActive(values[9]);
			hotelSupplementary.setRateBased_On(values[10]);
			hotelSupplementary.setNetRate(values[11]);
			hotelSupplementary.setChild_NetRate(values[12]);
			hotelSupplementary.setProfit_By(values[13]);
			hotelSupplementary.setProfit_Value(values[14]);
			hotelSupplementary.setChild_Profit_Value(values[15]);
			hotelSupplementary.setMin_Child_Age(values[16]);
			hotelSupplementary.setMax_Child_Age(values[17]);
			hotelSupplementary.setPercentage_Room_Rate(values[18]);

			try {
				HotelList.get(values[0]).addHotelSupplimentary(values[1],
						hotelSupplementary);
			} catch (Exception e) {
				dataLogger.warn(values[0] + " not found in the hotel list");
			}
		}
		return HotelList;

	}

	public TreeMap<String, Hotel> loadPromoDetails(Map<Integer, String> map,
			TreeMap<String, Hotel> HotelList) {

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {

			HotelPromotion hotelPromotion = new HotelPromotion();
			String[] values = it.next().getValue().split(",");

		//	hotelPromotion.setSupp_Name(values[1]);
			hotelPromotion.setHotel_Name(values[0]);
			hotelPromotion.setRoom_Type(values[1]);
			hotelPromotion.setBed_Type(values[2]);
			hotelPromotion.setRate_Plan(values[3]);
			hotelPromotion.setCalculation_Logic(values[4]);
			hotelPromotion.setPromotion_BasedOn(PromotionBasedOnType
					.getInventoryType(values[5]));
			hotelPromotion.setNights_Book(values[6]);
			hotelPromotion.setPrior_Arrival(values[7]);
			hotelPromotion.setPromotionType(PromotionType
					.getInventoryType(values[8]));

			hotelPromotion.setFN_FreeNights(values[9]);
			hotelPromotion.setFN_MaxNumberofNights(values[10]);
			hotelPromotion.setFN_Surchage_Fee(values[11]);
			hotelPromotion
					.setSpecialRateApplicableType(PromotionSpecialRateApplicableType
							.getInventoryType(values[12]));
			hotelPromotion
					.setDiscountrateapplicabletype(DiscountRateApplicableType
							.getChargeType(values[13]));
			hotelPromotion.setPromo_Value(values[14]);
			hotelPromotion.setNote(values[15]);
			hotelPromotion.setPromoCode(values[16]);
			hotelPromotion.setIsbestrateguarantee(values[17]);
			hotelPromotion.setApplicablePeriod(PromotionApplicablePeriodType
					.getChargeType(values[18]));
			hotelPromotion.setBooking_Date_From(values[19]);
			hotelPromotion.setBooking_Date_To(values[20]);
			hotelPromotion.setStay_Date_From(values[21]);
			hotelPromotion.setStay_Date_To(values[22]);
			hotelPromotion.setBooking_Channel(BookingChannelType
					.getChargeType(values[23]));
			hotelPromotion
					.setPartnerType(PartnerType.getChargeType(values[24]));
			hotelPromotion.setAgentRegion(values[25]);
			hotelPromotion.setAgentName(values[26]);
			hotelPromotion.setIs_Active(values[27]);

			try {
				HotelList.get(values[0]).addHotelPromotions(hotelPromotion);
			} catch (Exception e) {
				dataLogger.warn(values[0] + " not found in the hotel list");
			}

		}
		return HotelList;

	}

}
