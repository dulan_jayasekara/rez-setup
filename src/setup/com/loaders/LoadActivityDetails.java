package setup.com.loaders;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import setup.com.pojo.ActivityInventory;
import setup.com.pojo.ActivityProfit;
import setup.com.pojo.ActivityRate;
import setup.com.pojo.ActivityStandard;
import setup.com.pojo.AssignActivity;
import setup.com.pojo.Contract;
import setup.com.pojo.Policies;
import setup.com.pojo.Tax;




public class LoadActivityDetails {
	
		
	public TreeMap<String, ActivityStandard> loadActivityStandard(Map<Integer, String> ActivityStandardMap){
			
			Iterator<Map.Entry<Integer, String>> it = ActivityStandardMap.entrySet().iterator();
			TreeMap<String, ActivityStandard> ActivityStandardList = new TreeMap<String, ActivityStandard>();
			
			while(it.hasNext()) {
				ActivityStandard activityStandard = new ActivityStandard();
				String[] values = it.next().getValue().split(",");
				
				activityStandard.setActivity_Program_Name(values[0]);
				activityStandard.setSup_Name(values[1]);
				activityStandard.setActivity_P_Cat(values[2]);
				activityStandard.setDis_Web(values[3]);
				activityStandard.setDis_Call(values[4]);
				activityStandard.setProgram_Active(values[5]);
				activityStandard.setCustomer_Alert(values[6]);			
				activityStandard.setTicket_Name(values[7]);
				activityStandard.setTicket_Body(values[8]);
				activityStandard.setPassport_req(values[9]);
				activityStandard.setVoucher_Req(values[10]);				
				activityStandard.setContact_Name(values[11]);
				activityStandard.setContact_Email(values[12]);
				activityStandard.setContact_Type(values[13]);
				activityStandard.setProgram_Available_Dates(values[14]);
				activityStandard.setInventory_Obtained(values[15]);
				activityStandard.setRateContract(values[16]);
				activityStandard.setCommissionContractType(values[17]);
				activityStandard.setPrepay_Req(values[18]);
				activityStandard.setCreditCard_Enable(values[19]);
				activityStandard.setActDescription(values[20]);
				activityStandard.setActImagesPath(values[21]);
				activityStandard.setActSetupNeeded(values[22]);
				
				ActivityStandardList.put(values[0], activityStandard);
				
				
			}
		
			System.out.println(ActivityStandardList);
			
			return ActivityStandardList;		
		}
			
		
	public TreeMap<String,ActivityStandard> loadActivityContract(Map<Integer, String> ContractMap, TreeMap<String, ActivityStandard> ActivityStandardList){
		
		Iterator<Map.Entry<Integer, String>> it = ContractMap.entrySet().iterator();
		
		while(it.hasNext()) {
			Contract contract = new Contract();
			String[] values = it.next().getValue().split(",");
			
			//contract.setActivityProgramName_Contract(values[0]);	
			
			contract.setRegion(values[1]);			
			contract.setContract_From(values[2]);
			contract.setContract_To(values[3]);
			contract.setInventoryType(values[4]);	
			
			try {
				ActivityStandardList.get(values[0]).AddContract(contract);
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}	
		}
		
		System.out.println(ActivityStandardList);
	
		return ActivityStandardList;		
			
	}
		
		
	public TreeMap<String,ActivityStandard> loadActivityPolicies(Map<Integer, String> PoliciesMap, TreeMap<String, ActivityStandard> ActivityStandardList){
			
			Iterator<Map.Entry<Integer, String>> it = PoliciesMap.entrySet().iterator();
			
			while(it.hasNext()) {
				Policies policies = new Policies();
				String[] values = it.next().getValue().split(",");
				
				//policies.setActivityProgramName_Policies(values[0]);			
				policies.setTravelD_from(values[1]);
				policies.setTravelD_to(values[2]);
				policies.setCan_Apply_By(values[3]);
				policies.setCan_Less_than(values[4]);
				policies.setCan_buffer(values[5]);
				policies.setStandard_Can_BasedOn(values[6]);
				policies.setStandard_Can_Value(values[7]);				
				policies.setNo_Show_Can_BasedOn(values[8]);
				policies.setNo_Show_Can_Value(values[9]);
				
				
				try {
					ActivityStandardList.get(values[0]).AddPolicies(policies);
					
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		
			return ActivityStandardList;		
		}
				
				
	public TreeMap<String,ActivityStandard> loadActivityTax(Map<Integer, String> TaxMap, TreeMap<String, ActivityStandard> ActivityStandardList){
		
		Iterator<Map.Entry<Integer, String>> it = TaxMap.entrySet().iterator();
		
		
		while(it.hasNext()) {
			Tax tax = new Tax();
			String[] values = it.next().getValue().split(",");
			
			
			//tax.setActivityProgramName_Tax(values[0]);
			tax.setTaxes_From(values[1]);
			tax.setTaxes_To(values[2]);
			tax.setSales_Tax(values[3]);
			tax.setSales_Tax_Value(values[4]);
			tax.setMis_Tax(values[5]);
			tax.setMis_Tax_Value(values[6]);
			
			try {
				ActivityStandardList.get(values[0]).AddTax(tax);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	
		return ActivityStandardList;		
	}				
				
				
	public TreeMap<String,ActivityStandard> loadActivityAssignActivity(Map<Integer, String> AssignActivityMap, TreeMap<String, ActivityStandard> ActivityStandardList){
		
		Iterator<Map.Entry<Integer, String>> it = AssignActivityMap.entrySet().iterator();
		
		while(it.hasNext()) {
			AssignActivity assignActivity = new AssignActivity();
			String[] values = it.next().getValue().split(",");
			
			//assignActivity.setActivityProgramName_AssignActivity(values[0]);
			assignActivity.setActivity_Type(values[1]);
			assignActivity.setActivity_Period(values[2]);
			assignActivity.setActivity_RatePlan(values[3]);
			assignActivity.setMin_Max_Rate(values[4]);
			assignActivity.setCombination_Active(values[5]);
			assignActivity.setDisplay_Order(values[6]);
			
			try {
				
				ActivityStandardList.get(values[0]).AddAssignActivity(assignActivity);
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	
		return ActivityStandardList;		
	}			
				
				
	public TreeMap<String,ActivityStandard> loadActivityInventories(Map<Integer, String> ActivityInventoryMap, TreeMap<String, ActivityStandard> ActivityStandardList){
		
		Iterator<Map.Entry<Integer, String>> it = ActivityInventoryMap.entrySet().iterator();
		
		while(it.hasNext()) {
			ActivityInventory activityInventory = new ActivityInventory();
			String[] values = it.next().getValue().split(",");
			
			//activityInventory.setActivity_Program(values[0]);
			activityInventory.setRegion(values[1]);
			activityInventory.setTour_Operator(values[2]);
			activityInventory.setContract_from(values[3]);
			activityInventory.setContract_to(values[4]);
			activityInventory.setActivity_Period(values[5]);
			activityInventory.setInventory_By(values[6]);
			activityInventory.setTotal_Inventory(values[7]);
			activityInventory.setCut_off(values[8]);
			activityInventory.setFriday_Inventory(values[9]);
			activityInventory.setSaturday_Inventory(values[10]);
			activityInventory.setSunday_Inventory(values[11]);
						
			try {
				ActivityStandardList.get(values[0]).AddInventory(activityInventory);
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}			
		}
		
		return ActivityStandardList;
		
	}
		
	
	public TreeMap<String,ActivityStandard> loadActivityRates(Map<Integer, String> ActivityRatesMap, TreeMap<String, ActivityStandard> ActivityStandardList){
		
		Iterator<Map.Entry<Integer, String>> it = ActivityRatesMap.entrySet().iterator();
		
		while(it.hasNext()) {
			ActivityRate activityRate = new ActivityRate();
			String[] values = it.next().getValue().split(",");
			
			//activityRate.setRate_ActivityProgramName(values[0]);
			activityRate.setRate_Region(values[1]);
			activityRate.setRate_Contract_from(values[2]);
			activityRate.setRate_Contract_to(values[3]);
			activityRate.setRate_ActivityRatePeriod(values[4]);
			activityRate.setRate_FixRate(values[5]);
			activityRate.setRate_NetRate(values[6]);
			activityRate.setRate_SellRate(values[7]);
			activityRate.setRate_Friday_Inventory(values[8]);
			activityRate.setRate_Saturday_Inventory(values[9]);
			activityRate.setRate_Sunday_Inventory(values[10]);
									
			try {
				ActivityStandardList.get(values[0]).AddActivityRate(activityRate);
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}	
		}
		
		return ActivityStandardList;	
	}			
	
	
	public TreeMap<String,ActivityStandard> loadActivityProfits(Map<Integer, String> ActivityProfitsMap, TreeMap<String, ActivityStandard> ActivityStandardList){
		
		Iterator<Map.Entry<Integer, String>> it = ActivityProfitsMap.entrySet().iterator();
		
		while(it.hasNext()) {
			ActivityProfit activityProfit = new ActivityProfit();
			String[] values = it.next().getValue().split(",");
			
			//activityProfit.setActivityProgramName(values[0]);
			activityProfit.setBooking_Channel(values[1]);
			activityProfit.setPartner(values[2]);
			activityProfit.setRate_Region(values[3]);
			activityProfit.setProfitMarkup_Type(values[4]);
			activityProfit.setProfit_Period_From(values[5]);
			activityProfit.setProfit_Period_To(values[6]);
			activityProfit.setCountry(values[7]);
			activityProfit.setCity(values[8]);
			activityProfit.setSupplier(values[9]);
			activityProfit.setProfit_ApplyTo(values[10]);			
			activityProfit.setSelect_Program(values[11]);
			activityProfit.setMarkup_Rate(values[12]);
			activityProfit.setSpecific_Markup(values[13]);
			activityProfit.setSelectProfitActivity(values[14]);
			activityProfit.setFix_Profit_Markup(values[15]);
			activityProfit.setProfit_Markup_V(values[16]);
			activityProfit.setFriday_Profit(values[17]);
			activityProfit.setSaturday_Profit(values[18]);
			
			
			try {
				ActivityStandardList.get(values[0]).AddProfits(activityProfit);
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}	
		}
		
		return ActivityStandardList;	
		
	}
		
		
}
