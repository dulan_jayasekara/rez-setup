package setup.com.enumtypes;

public enum PromotionBasedOnType {
	MINIMUMNIGHTSBOOKED,PROMOPERIOD,EARLYBIRD,NONE;
	
	public static PromotionBasedOnType getInventoryType(String PromoType)
	{
		if(PromoType.equalsIgnoreCase("Minimum Nights Booked"))return MINIMUMNIGHTSBOOKED;
		else if(PromoType.equalsIgnoreCase("Promotional Period"))return PROMOPERIOD;
		else if(PromoType.equalsIgnoreCase("Early Bird"))return EARLYBIRD;
		else return NONE;
		
	}

}
