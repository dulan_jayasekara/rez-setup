package setup.com.enumtypes;

public enum BookingChannelType {
	
	ALL,WEB,CALLCENTER,NONE;
	
	public static BookingChannelType getChargeType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("ALL"))return ALL;
		else if(ChargeType.equalsIgnoreCase("WEB"))return WEB;
		else if(ChargeType.equalsIgnoreCase("CALLCENTER"))return CALLCENTER;
		else return NONE;
		
	}

	
}
