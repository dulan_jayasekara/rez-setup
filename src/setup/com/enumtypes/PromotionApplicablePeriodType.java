package setup.com.enumtypes;

public enum PromotionApplicablePeriodType {
	
	STAYDATE,BOOKINGDATE,BOTH,NONE;
	
	public static PromotionApplicablePeriodType getChargeType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("Stay Date Period"))return STAYDATE;
		else if(ChargeType.equalsIgnoreCase("Booking Date"))return BOOKINGDATE;
		else if(ChargeType.equalsIgnoreCase("Both"))return BOTH;
		else return NONE;
		
	}

	
}
