package setup.com.enumtypes;

public enum InventoryActionType {
	
CONFIRMATION,CALLTOBOOK,FREESELL,REQUEST,NONE;

public static InventoryActionType getInventoryActionType(String Action)
{
	if(Action.equalsIgnoreCase("Confirmation"))
    return CONFIRMATION;
	else if(Action.equalsIgnoreCase("Call to Book"))
	return CALLTOBOOK;
	else if(Action.equalsIgnoreCase("Request"))
	return REQUEST;
	else if(Action.equalsIgnoreCase("Free Sell") || Action.equalsIgnoreCase("FreeSell"))
	return FREESELL;
	else return NONE;
}

}
