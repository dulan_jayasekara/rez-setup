package setup.com.enumtypes;

public enum PromotionType {
	FREENIGHTS,DISCOUNT,RATE,FREENIGHTSANDARATE,NONE;
	
	public static PromotionType getInventoryType(String PromoType)
	{
		if(PromoType.equalsIgnoreCase("Free Nights"))return FREENIGHTS;
		else if(PromoType.equalsIgnoreCase("Discount"))return DISCOUNT;
		else if(PromoType.equalsIgnoreCase("Rate"))return RATE;
		else if(PromoType.equalsIgnoreCase("Free Nights and Rate"))return FREENIGHTSANDARATE;
		else return NONE;
		
	}

}
