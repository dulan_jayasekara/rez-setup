package setup.com.enumtypes;

public enum DiscountRateApplicableType {
	
	NETRATE,SELLRATE,PERCENTAGE,VALUE,NONE;
	
	public static DiscountRateApplicableType getChargeType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("Percentage"))return PERCENTAGE;
		else if(ChargeType.equalsIgnoreCase("Value"))return VALUE;
		else if(ChargeType.equalsIgnoreCase("Net Rate"))return NETRATE;
		else if(ChargeType.equalsIgnoreCase("Sell Rate"))return SELLRATE;
		else return NONE;
		
	}

	
}
