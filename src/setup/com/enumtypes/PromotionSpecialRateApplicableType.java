package setup.com.enumtypes;

public enum PromotionSpecialRateApplicableType {
	FOREXTRANIGHT,PROMOPERIOD,BOTH,NONE;
	
	public static PromotionSpecialRateApplicableType getInventoryType(String PromoType)
	{
		if(PromoType.equalsIgnoreCase("For Extra Night"))return FOREXTRANIGHT;
		else if(PromoType.equalsIgnoreCase("For Promotion Period"))return PROMOPERIOD;
		else if(PromoType.equalsIgnoreCase("Both"))return BOTH;
		else return NONE;
		
	}

}
