package setup.com.readers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.apache.log4j.Logger;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

public class ReadExcel { 
	
	
	private static Logger PromoLog = Logger.getLogger(ReadExcel.class.getName());
	
	

	public ArrayList<Map<Integer,String>> init(String filePath) {
        
		FileInputStream fs = null;
		ArrayList<Map<Integer,String>> returnlist=null;
		try {
			PromoLog.debug("Creating new file stream..");
			
			PromoLog.debug("File path :"+ filePath);
			
			fs = new FileInputStream(new File(filePath));
	        
			PromoLog.info("Filestream created successfully");
	       
			returnlist = contentReading(fs);
		} catch (IOException e) {
			
			PromoLog.fatal(e.toString());
		} catch (Exception e) {
		  
			PromoLog.fatal(e.toString());
			
		} finally {
			try {
				fs.close();
			} catch (IOException e) {
			
			}
		}
		return returnlist;
	}



	public  ArrayList<Map<Integer, String>>  contentReading(InputStream fileInputStream) {
		
		
		ArrayList<Map<Integer, String>> list = null;
		WorkbookSettings                  ws = null;
		Workbook workbook                    = null;
	    int totalSheet                       = 0;
       
		try {
			PromoLog.info("Initializing the workspace..");
			ws = new WorkbookSettings();
			ws.setLocale(new Locale("en", "EN"));
			workbook = Workbook.getWorkbook(fileInputStream, ws);

			totalSheet = workbook.getNumberOfSheets();
			PromoLog.info("Number of sheets found :"+ totalSheet);
		
			list = new ArrayList<Map<Integer,String>>(totalSheet);
            
			for (int sheet = 0 ; sheet<totalSheet ; sheet++) {
				PromoLog.info(" Sheet Number :"+ sheet);
			    list.add(getSheetData(workbook.getSheet(sheet) ));	
			    PromoLog.info(list.get(sheet).toString());
			}
			
		
			workbook.close();
		} catch (IOException e) {
		
		} catch (BiffException e) {
		
		}
		
		return list;
	}

	public static void main(String[] args) {
		try {
			System.out.println("begin");
			ReadExcel xlReader = new ReadExcel();
			ArrayList<Map<Integer, String>> list1 = xlReader.init("Excel/SetUp_Details.xls");
			
			System.out.println(list1);
			
			System.out.println("end");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Map<Integer, String> getSheetData(Sheet s)
	{
		int rawCount = s.getRows();
		
		int columnCount = s.getColumns();
		Map<Integer, String> map = new HashMap<Integer, String>();
		for(int count=1 ; count<rawCount ; count++)
		{
			PromoLog.info("Raw---> "+count);
			Cell[] rawdata = s.getRow(count);
			
			String CellContent = rawdata[0].getContents().trim();
			for(int index = 1 ; index<columnCount ; index++)
			{
				PromoLog.info("Cell---> "+index);
				CellContent = CellContent.concat(",").concat(rawdata[index].getContents()).trim();
			}
			
			map.put(count-1, CellContent);
			
		}
		
	  return map;

	}
	
}