package setup.com.runners.Flow;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import setup.com.pojo.ActivityRatePlan;
import setup.com.pojo.ActivityType;
import setup.com.pojo.PeriodSetup;
import setup.com.pojo.ProgramType;
import setup.com.pojo.RatePlan;
import setup.com.readers.PG_Properties;
import setup.com.utill.ExtentReportTemplate;



public class ActivityBasicInfo {
	
	private WebDriver driver = null;
	private FirefoxProfile profile;
	JavascriptExecutor javaScriptExe = (JavascriptExecutor) driver;
	
	//WebDriverWait wait = new WebDriverWait(driver, 10);
	
	public ActivityBasicInfo(WebDriver Driver){
		this.driver = Driver;
	}	
	
	public void setUp() {
		profile = new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));

		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); ////entire flow
				
	}
	
	
	public WebDriver createProgramType(ProgramType protype) throws InterruptedException{
		
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/ProgramTypePage.do?module=contract");
		
		driver.findElement(
				By.xpath(".//*[@id='screenaction_create']"))
				.click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("programTypeName")).sendKeys(protype.getActiviy_P_Cat());
		driver.findElement(By.id("rank")).sendKeys(protype.getRank());
		
		if (protype.getPick_drop_info().equals("Yes")){
			
			driver.findElement(
					By.xpath(".//*[@id='pickupDropOff_Y']"))
					.click();						
		} 
		
		if (protype.getEnable_location().equals("Yes")) {
			
			driver.findElement(
					By.xpath(".//*[@id='enableLocation_Y']"))
					.click();			
		}
		
		if (protype.getEnable_Act_Type().equals("Yes")) {
			
			driver.findElement(
					By.xpath(".//*[@id='enableactivityType_Y']"))
					.click();
		}
		
		driver.findElement(By.id("saveButId")).click();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		//

		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
			// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

			if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
				String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
				// System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("Programtype creation function "+protype.getActiviy_P_Cat(), " Programtype should be saved", TextMessage, true);
				
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				
			} else {

				String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				// System.out.println(TextMessage);
			
				ExtentReportTemplate.getInstance().addtoReport("Programtype creation function "+protype.getActiviy_P_Cat(), " Programtype should be saved", TextMessage, false);
				driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				
			}

		} catch (Exception e) {
			
			ExtentReportTemplate.getInstance().addtoReport("Programtype creation function "+protype.getActiviy_P_Cat(), " Programtype should be saved", e.toString(), false);
			
			// System.out.println(e.toString());

		}
		
		
		
		
		
		Thread.sleep(1500);
				
		return driver;
	}
	
	public WebDriver createActivityType(ActivityType activityType) throws InterruptedException{
		
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/ActivityTypeDetailsPage.do?module=contract");
		Thread.sleep(2000);
		driver.findElement(
				By.xpath(".//*[@id='screenaction_create']"))
				.click();
		Thread.sleep(2000);
		
		driver.findElement(By.id("activityTypeName")).sendKeys(activityType.getActivity_type());
		
		driver.findElement(By.id("programType")).sendKeys(activityType.getActiviy_P_Cat());
		driver.findElement(By.id("programType_lkup")).click();
		
		driver.switchTo().frame("lookup");
		Thread.sleep(1500);
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		driver.switchTo().defaultContent();
		Thread.sleep(1500);
		
		driver.findElement(By.xpath(".//*[@id='activityTypeDesc']")).sendKeys(activityType.getActivity_desc());
		
		if (activityType.getReturn_Availability().equals("Yes")) {
			
			driver.findElement(
					By.xpath(".//*[@id='isReturnAvailable_Y']"))
					.click();
		}
				
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(3000);
		
        WebDriverWait wait = new WebDriverWait(driver, 30);
		

		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
			// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

			if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
				String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
				// System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("ActivityType creation function "+activityType.getActivity_type(), " ActivityType should be saved", TextMessage, true);
				
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				
			} else {

				String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				// System.out.println(TextMessage);
			
				ExtentReportTemplate.getInstance().addtoReport("ActivityType creation function "+activityType.getActivity_type(), " ActivityType should be saved", TextMessage, false);
				driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				
			}

		} catch (Exception e) {
			
			ExtentReportTemplate.getInstance().addtoReport("ActivityType creation function "+activityType.getActivity_type(), " ActivityType should be saved", e.toString(), false);			
			// System.out.println(e.toString());

		}
		
		
		
		
		
		
		
		Thread.sleep(4500);
		
		return driver;
	}
	
	public WebDriver createSetupPeriod(PeriodSetup period) throws InterruptedException{
		
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/PeriodPage.do?module=contract");
		
		driver.findElement(
				By.xpath(".//*[@id='screenaction_create']"))
				.click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("periodName")).sendKeys(period.getPeriod_Name());
		
		if (period.getCutoff_detup().equals("Hours")) {
			
			driver.findElement(
					By.xpath(".//*[@id='cutoffSetupBy_H']"))
					.click();
		}
		
		driver.findElement(By.name("cutoffTime")).sendKeys(period.getCut_time());
		driver.findElement(By.id("periodDescription")).sendKeys(period.getPeriod_desc());
		driver.findElement(By.name("duration")).sendKeys(period.getDuration_days());
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		

		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
			// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

			if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
				String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
				// System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("PeriodSetup creation function "+period.getPeriod_Name(), " PeriodSetup should be saved", TextMessage, true);
				
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				
			} else {

				String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				// System.out.println(TextMessage);
			
				ExtentReportTemplate.getInstance().addtoReport("PeriodSetup creation function "+period.getPeriod_Name(), " PeriodSetup should be saved", TextMessage, false);
				driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				
			}

		} catch (Exception e) {
			
			ExtentReportTemplate.getInstance().addtoReport("PeriodSetup creation function "+period.getPeriod_Name(), " PeriodSetup should be saved", e.toString(), false);			
			// System.out.println(e.toString());

		}
		
		
		
		
		
		
		Thread.sleep(1500);
		
		
		return driver;
	}
	
	
	public WebDriver createRatePlan(ActivityRatePlan rateplan) throws InterruptedException{
		
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/SetupRatePlanPage.do?module=contract");
		
		driver.findElement(
				By.xpath(".//*[@id='screenaction_create']"))
				.click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("ratePlanName")).sendKeys(rateplan.getRate_Plan());
		driver.findElement(By.id("minPersons")).sendKeys(rateplan.getMin_person());
		driver.findElement(By.id("maxPersons")).sendKeys(rateplan.getMax_person());
		
		if (rateplan.getRate_calculation().equals("Yes")) {
			
			driver.findElement(
					By.xpath(".//*[@id='optminRatecalc_Y']"))
					.click();
		}
		
		driver.findElement(By.id("ratePlanDesc")).sendKeys(rateplan.getRatePlan_desc());
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		

		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
			// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

			if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
				String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
				// System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("RatePlan creation function "+rateplan.getRate_Plan(), " RatePlan should be saved", TextMessage, true);
				
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				
			} else {

				String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				// System.out.println(TextMessage);
			
				ExtentReportTemplate.getInstance().addtoReport("RatePlan creation function "+rateplan.getRate_Plan(), " RatePlan should be saved", TextMessage, false);
				driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				
			}

		} catch (Exception e) {
			
			ExtentReportTemplate.getInstance().addtoReport("RatePlan creation function "+rateplan.getRate_Plan(), " RatePlan should be saved", e.toString(), false);			
			// System.out.println(e.toString());

		}
		
		
		
		
		Thread.sleep(1500);
				
		return driver;
	}
	
	
	//delete
	public WebDriver deleteActivityType(ActivityType activitytype) throws InterruptedException{
		
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/ActivityTypeDetailsPage.do?module=contract");
		
		driver.findElement(
				By.xpath(".//*[@id='screenaction_delete']"))
				.click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("activityTypeName")).sendKeys(activitytype.getActivity_type());
		driver.findElement(By.id("activityTypeName_lkup")).click();
		
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
		Thread.sleep(1500);
				
		return driver;
	}
	
	
	
	public WebDriver deleteProgramType(ProgramType protype) throws InterruptedException{
		
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/ProgramTypePage.do?module=contract");
		
		driver.findElement(
				By.xpath(".//*[@id='screenaction_delete']"))
				.click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("programTypeName")).sendKeys(protype.getActiviy_P_Cat());		
		driver.findElement(By.id("programTypeName_lkup")).click();
		
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
		Thread.sleep(1500);
				
		return driver;
	}
	
	
	
	
	public WebDriver deleteSetupPeriod(PeriodSetup period) throws InterruptedException{
		
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/PeriodPage.do?module=contract");
		
		driver.findElement(
				By.xpath(".//*[@id='screenaction_delete']"))
				.click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("periodName")).sendKeys(period.getPeriod_Name());
		driver.findElement(By.id("periodName_lkup")).click();
		
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
		Thread.sleep(1500);
				
		return driver;
	}
	
	
	
	public WebDriver deleteRatePlan(ActivityRatePlan rateplan) throws InterruptedException{
		
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/SetupRatePlanPage.do?module=contract");
		
		driver.findElement(
				By.xpath(".//*[@id='screenaction_delete']"))
				.click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("ratePlanName")).sendKeys(rateplan.getRate_Plan());
		driver.findElement(By.id("ratePlanName_lkup")).click();
		
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
		Thread.sleep(1500);
				
		return driver;
		
	}
	
	
	
}
