package setup.com.runners.Flow;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import setup.com.enumtypes.ConfigFareType;
import setup.com.pojo.AirConfig;
import setup.com.readers.PG_Properties;
import setup.com.utill.ExtentReportTemplate;


public class AirConfigurationSetUpFlow {
	
	
	boolean found								= false;
	String airline								= "";

	public WebDriver setAirconfigSetup(WebDriver driver, AirConfig airConfig,String Base) {

		
		
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/air/setup/AirConfigurationSetupPage.do?module=contract");
		
		
			
			
			
			
			try
			{
				driver.findElement(By.id("airLineName")).sendKeys(airConfig.getAirLine().trim());
				driver.findElement(By.id("airLineName_lkup")).click();
				driver.switchTo().frame("lookup");
				airline = driver.findElement(By.className("rezgLook0")).getText().trim();
				if(airConfig.getAirLine().trim().equalsIgnoreCase(airline))
				{
					found = true;
					
					driver.findElement(By.id("lookupDataArea")).findElement(By.className("rezgLook0")).click();
				}
			
				driver.switchTo().defaultContent();
			
			} catch (Exception e) {
	
			}
			
			if(found)
			{
				boolean set = false;
				
				while(!set)
				{
					if( !airConfig.getPubFareType().equals("") )
					{
						ArrayList<WebElement> publishedRadio = new ArrayList<WebElement>(driver.findElements(By.id("publishedFareOption")));
						if( airConfig.getPubFareType().equals(ConfigFareType.Booking_Fee) )
						{
							if( !publishedRadio.get(0).isSelected() )
							{	
								publishedRadio.get(0).click();
								
							}
						}
						
						else if( airConfig.getPubFareType().equals(ConfigFareType.Profit_Markup) )
						{
							if( !publishedRadio.get(1).isSelected() )
							{
								publishedRadio.get(1).click();
								
							}
						}
						
						else if( airConfig.getPubFareType().equals(ConfigFareType.Both) )
						{
							if( !publishedRadio.get(2).isSelected() )
							{
								publishedRadio.get(2).click();
								
							}
						}
						
						else if( airConfig.getPubFareType().equals(ConfigFareType.None) )
						{
							if( !publishedRadio.get(3).isSelected() )
							{
								publishedRadio.get(3).click();
								
							}
						}
						
						driver.findElement(By.id("addAirConfigurationMapping")).click();
						
						if(driver.findElement(By.id("dialogMsgText")).isDisplayed())
						{
							
							((JavascriptExecutor)driver).executeScript("javascript:closeDialogMsg(dialogMsgBox);");
							set = false;
							ArrayList<WebElement> table = new ArrayList<WebElement>(driver.findElements(By.xpath(".//*[@id='air_configuration_mapping_data']/table/tbody")));
							ArrayList<WebElement> tabletr = new ArrayList<WebElement>(table.get(0).findElements(By.className("reportDataRows1")));
							
							for(int y=0; y<tabletr.size(); y++)
							{
								String text = tabletr.get(y).findElements(By.className("tablegridcell")).get(0).getText();
								
								if(airConfig.getAirLine().trim().equalsIgnoreCase(text))
								{
									((JavascriptExecutor)driver).executeScript("removeAirConfigurationMapping("+(y+1)+")");
									
									break;
								}
							}
						}
						else
						{
						
							set = true;
						}
					}
				
				}
				
				
				if( !airConfig.getPvtFareType().equals("") )
				{
					ArrayList<WebElement> privateRadio = new ArrayList<WebElement>(driver.findElements(By.id("privateFareOption")) );
					
					if(airConfig.getPvtFareType().equals(ConfigFareType.Booking_Fee))
					{
						if( !privateRadio.get(0).isSelected())
						{
							privateRadio.get(0).click();
							
						}
					}
					
					else if( airConfig.getPvtFareType().equals(ConfigFareType.Profit_Markup) )
					{
						System.out.println();
						if( !privateRadio.get(1).isSelected() )
						{
							privateRadio.get(1).click();
							
						}
					}
					
					else if( airConfig.getPvtFareType().equals(ConfigFareType.Both) )
					{
						if( !privateRadio.get(2).isSelected())
						{
							privateRadio.get(2).click();
							
						}
					}
				}
				
				if(!airConfig.getFTypeConfgShopCart().equals(""))
				{
					if(airConfig.getFTypeConfgShopCart().equals("Published_Fares_Only"))
					{
						driver.findElement(By.id("fareTypeShoppingCart_PRP")).click();
						
					}
				}
				
				if( !airConfig.getFlightPayOptCartBooking().equals("") )
				{
					if(airConfig.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Full_Amount_at_Booking"))
					{
						driver.findElement(By.id("payementOptionShoppingCart_PF")).click();
						
					}
					else if(airConfig.getFlightPayOptCartBooking().equalsIgnoreCase("Pay_Booking_Fee_as_Deposit"))
					{
						driver.findElement(By.id("payementOptionShoppingCart_PB")).click();
						;
					}
					else if(airConfig.getFlightPayOptCartBooking().equalsIgnoreCase("Pass_to_Air_Line_to_Charge"))
					{
						driver.findElement(By.id("payementOptionShoppingCart_AC")).click();
						
					}
				}
			}//End if(found)
			
			driver.findElement(By.id("saveButId")).click();
			
			WebDriverWait wait = new WebDriverWait(driver, 30);
			

			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

				if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
					String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
					// System.out.println(TextMessage);
					
					ExtentReportTemplate.getInstance().addtoReport("Air configurations Assign function ", " air config saving Passed", TextMessage, true);
					
					driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
					
				} else {

					String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
					// System.out.println(TextMessage);
				
					ExtentReportTemplate.getInstance().addtoReport("Air configurations Assign function ", " air config saving Passed", TextMessage, false);
					driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
					
				}

			} catch (Exception e) {
				
				ExtentReportTemplate.getInstance().addtoReport("Air configurations Assign function ", " air config saving Passed", e.toString(), false);
				
				// System.out.println(e.toString());

			}
			
			return driver;
		}
		
	
	

}
