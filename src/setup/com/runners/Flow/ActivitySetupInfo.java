package setup.com.runners.Flow;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import setup.com.pojo.ActivityInventory;
import setup.com.pojo.ActivityProfit;
import setup.com.pojo.ActivityRate;
import setup.com.pojo.ActivityStandard;
import setup.com.pojo.AssignActivity;
import setup.com.pojo.Contract;
import setup.com.pojo.Policies;
import setup.com.pojo.Tax;
import setup.com.readers.PG_Properties;
import setup.com.utill.ExtentReportTemplate;



public class ActivitySetupInfo {

	private WebDriver driver = null;
	private FirefoxProfile profile;
	JavascriptExecutor javaScriptExe = (JavascriptExecutor) driver;
	private ActivityStandard activityStandard;
	private String activityratetype;
	
	public void setUp() {
		profile = new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));

		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); ////entire flow
		
				
	}
	
	public ActivitySetupInfo(WebDriver Driver){
		this.driver = Driver;
	}
	
	public void addActivity(ActivityStandard activity){
		
		activityStandard = activity;
	}
	
	public boolean login(WebDriver Driver){

		driver = Driver;
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/admin/common/LoginPage.do");
		driver.findElement(By.id("user_id")).clear();
		driver.findElement(By.id("user_id")).sendKeys(PG_Properties.getProperty("Hotel.UserName"));
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(PG_Properties.getProperty("Hotel.Password"));
		driver.findElement(By.id("loginbutton")).click();
		
		try {			
			driver.findElement(By.id("mainmenurezglogo"));
			return true;
		} catch (Exception e) {
			System.out.println("Login Fail...");
			return false;
		}		
	}
	
	
	public WebDriver createActivityStandard(WebDriver Driver) throws InterruptedException{
			
			WebDriverWait wait = new WebDriverWait(driver, 60);
			driver.get(PG_Properties.getProperty("Hotel.BaseUrl")+"/activities/setup/ProgramSetupStandardPage.do?module=contract");
			Thread.sleep(3500);	
			
			driver.findElement(
					By.xpath(".//*[@id='screenaction_create']"))
					.click();
			Thread.sleep(1000);
			
			driver.findElement(By.id("programname")).sendKeys(activityStandard.getActivity_Program_Name());
			
			driver.findElement(By.id("supplierName")).sendKeys(activityStandard.getSup_Name());
			driver.findElement(By.id("supplierName_lkup")).click();		
			driver.switchTo().frame("lookup");
			Thread.sleep(1000);
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			Thread.sleep(1000);
			driver.switchTo().defaultContent();
			
			driver.findElement(By.id("programType")).sendKeys(activityStandard.getActivity_P_Cat());
			driver.findElement(By.id("programType_lkup")).click();		
			driver.switchTo().frame("lookup");
			Thread.sleep(1000);
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			Thread.sleep(1000);
		    driver.switchTo().defaultContent();
			if (activityStandard.getDis_Web().equals("No")) {
				
				driver.findElement(
						By.xpath(".//*[@id='displayinwebsite_N']"))
						.click();
			}
			
			if (activityStandard.getDis_Call().equals("No")) {
				
				driver.findElement(
						By.xpath(".//*[@id='displayincallcenter_N']"))
						.click();
			}
			
			driver.findElement(By.xpath(".//*[@id='resultsrank']")).sendKeys("1");
			
			if (activityStandard.getProgram_Active().equals("No")) {
				
				driver.findElement(
						By.xpath(".//*[@id='programactive_N']"))
						.click();
			}
			
			if (activityStandard.getCustomer_Alert().equals("Send Booking Acknowledgement")) {
				
				driver.findElement(
						By.xpath(".//*[@id='alertNotificationStatus_A']"))
						.click();
			}
			
			
			if (activityStandard.getTicket_Name().equals("Yes")) {
				
				driver.findElement(
						By.xpath(".//*[@id='requireNamesEachResTicket_Y']"))
						.click();
			}
			
			if (activityStandard.getTicket_Body().equals("Yes")) {
				
				driver.findElement(
						By.xpath(".//*[@id='requireBodyWeightEachResTicket_Y']"))
						.click();
			}

			if (activityStandard.getPassport_req().equals("Yes")) {
				
				driver.findElement(
						By.xpath(".//*[@id='requirePassport_Y']"))
						.click();
			}
			if (activityStandard.getVoucher_Req().equals("Yes")) {
				
				driver.findElement(
						By.xpath(".//*[@id='voucher_Y']"))
						.click();
			}
			
	
			driver.findElement(By.name("submit")).click();

			
			// driver.findElement(By.xpath(".//*[@id='saveButId']")).click();
			

			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

				if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
					String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
					// System.out.println(TextMessage);
					
					ExtentReportTemplate.getInstance().addtoReport("Activity_Program Standard info creation function "+activityStandard.getActivity_Program_Name(), " Activity_Program_Name page should be saved", TextMessage, true);
					
					driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
					
				} else {

					String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
					// System.out.println(TextMessage);
				
					ExtentReportTemplate.getInstance().addtoReport("Activity_Program Standard info creation function "+activityStandard.getActivity_Program_Name(), " Activity_Program_Name page should be saved", TextMessage, false);
					
					driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
					
				}

			} catch (Exception e) {
				
				ExtentReportTemplate.getInstance().addtoReport("Activity_Program Standard info creation function "+activityStandard.getActivity_Program_Name(), " Activity_Program_Name page should be saved", e.toString(), false);
				// System.out.println(e.toString());

			}

			
			
			
	//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));
//			driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
			Thread.sleep(2000);
		
		return driver;
	}
		
		
		
	public WebDriver addContracts(WebDriver Driver) throws InterruptedException{
		
			WebDriverWait wait = new WebDriverWait(driver, 120);
		
			driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/ProgramSetupContractPage.do");
			Thread.sleep(500);	
			
			driver.findElement(
					By.xpath(".//*[@id='scrollerAllID']/table[2]/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]/table/tbody/tr/td/table/tbody/tr/td[1]/input"))
					.click();
			
			if (activityStandard.getInventory_Obtained().equals("Program")) {
				
				driver.findElement(
						By.xpath(".//*[@id='inventoryby_H']"))
						.click();
			}
			if (activityStandard.getInventory_Obtained().equals("Activity and Period")) {
				
				driver.findElement(
						By.xpath(".//*[@id='inventoryby_B']"))
						.click();
			}
			
			if (activityStandard.getRateContract().equals("Commissionable Rate")) {
				
				driver.findElement(
						By.xpath(".//*[@id='contractType_C']"))
						.click();
				Thread.sleep(500);
				
				if(activityStandard.getCommissionContractType().equals("Pay at Site")){
					
					driver.findElement(
							By.xpath(".//*[@id='commissionableContractType_S']"))
							.click();
				}
				
				if(activityStandard.getCommissionContractType().equals("Pay at Check In")){
					
					driver.findElement(
							By.xpath(".//*[@id='commissionableContractType_C']"))
							.click();
				}
				
			}
			
			ArrayList<Contract> ContractList = activityStandard.getActivityContracts();
			
			for (Contract actContracts : ContractList) {
				
				driver.switchTo().frame("programcontractdetails");
				driver.findElement(By.id("addContractImg")).click();		
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");
				
				String region = actContracts.getRegion();
				driver.findElement(By.xpath(".//*[@id='regionArray_"+region+"']")).click();
				
				String contractFDate = actContracts.getContract_From();
				String[] partsCF = contractFDate.split("-");
				String ConFDay = partsCF[0]; 
				String ConFMonth = partsCF[1]; 
				String ConFYear = partsCF[2]; //14-Apr-2014
				
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("contractfrom_Month_ID"))).selectByVisibleText(ConFMonth);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("contractfrom_Year_ID"))).selectByVisibleText(ConFYear);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("contractfrom_Day_ID"))).selectByVisibleText(ConFDay);
				
				String contractToDate = actContracts.getContract_To();
				String[] partsCT = contractToDate.split("-");
				String ConToDay = partsCT[0]; 
				String ConToMonth = partsCT[1]; 
				String ConToYear = partsCT[2]; 
				
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath(".//*[@id='contractto_Month_ID']"))).selectByVisibleText(ConToMonth);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath(".//*[@id='contractto_Year_ID']"))).selectByVisibleText(ConToYear);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath(".//*[@id='contractto_Day_ID']"))).selectByVisibleText(ConToDay);
				
				
				if (actContracts.getInventoryType().equals("Free Sell")) {
					
					driver.findElement(
							By.xpath(".//*[@id='inventorytype_F']"))
							.click();
				}
				
				if (actContracts.getInventoryType().equals("On Request")) {
					
					driver.findElement(
							By.xpath(".//*[@id='inventorytype_R']"))
							.click();
				}
				
				Thread.sleep(500);
				driver.findElement(
						By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input"))
						.click();
						
				driver.switchTo().defaultContent();
				Thread.sleep(2500);
				
				Thread.sleep(6000);
				
			}
			
			driver.findElement(By.id("saveButId")).click();
			Thread.sleep(3000);
			
			
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

				if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
					String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
					// System.out.println(TextMessage);
					
					ExtentReportTemplate.getInstance().addtoReport("Activity Contracts creation function ", " Activity Contracts page should be saved", TextMessage, true);
					
					driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
					
				} else {

					String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
					// System.out.println(TextMessage);
				
					ExtentReportTemplate.getInstance().addtoReport("Activity Contracts creation function "+activityStandard.getActivity_Program_Name(), "Activity Contracts page should be saved", TextMessage, false);
					
					driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
					
				}

			} catch (Exception e) {
				
				ExtentReportTemplate.getInstance().addtoReport("Activity Contracts info creation function "+activityStandard.getActivity_Program_Name(), "Activity Contracts page should be saved", e.toString(), false);
				// System.out.println(e.toString());

			}
			
			
			
	//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));			
//			driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
			Thread.sleep(2000);
			
			
			
		
		return driver;
	}
		
	
	
	public WebDriver addPolicies(WebDriver Driver) throws InterruptedException{	
		
			WebDriverWait wait = new WebDriverWait(driver, 120);
	
			driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/ProgramSetupPoliciesPage.do");
			Thread.sleep(1500);	
			
			if (activityStandard.getPrepay_Req().equals("Yes")) {
				
				driver.findElement(
						By.xpath(".//*[@id='prepaymentrequired_Y']"))
						.click();
			}
			
			if (activityStandard.getCreditCard_Enable().equals("Yes")) {
				
				driver.findElement(
						By.xpath(".//*[@id='creditcardprocessing_Y']"))
						.click();
			}
			
			driver.findElement(
					By.xpath(".//*[@id='programAcceptCreditCards_Visa']"))
					.click();
			
			ArrayList<Policies> policyList = activityStandard.getActivityPolicies();
			
			for(Policies actPolicies : policyList){
				
				//cancel policy
				driver.switchTo().frame("programcancellationdetails");
				driver.findElement(By.id("addCanPolicyImg")).click();		
				
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");
				
				String datePolicyFrom = actPolicies.getTravelD_from();
				String[] partsPF = datePolicyFrom.split("-");
				String TFDay = partsPF[0]; 
				String TFMonth = partsPF[1]; 
				String TFYear = partsPF[2]; 
				
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("policyfrom_Month_ID"))).selectByVisibleText(TFMonth);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("policyfrom_Year_ID"))).selectByVisibleText(TFYear);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("policyfrom_Day_ID"))).selectByVisibleText(TFDay);
				
				String datePolicyTo = actPolicies.getTravelD_to();
				String[] partsPT = datePolicyTo.split("-");
				String TTDay = partsPT[0]; 
				String TTMonth = partsPT[1]; 
				String TTYear = partsPT[2]; 
				
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("policyto_Month_ID"))).selectByVisibleText(TTMonth);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("policyto_Year_ID"))).selectByVisibleText(TTYear);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("policyto_Day_ID"))).selectByVisibleText(TTDay);
				
				if (actPolicies.getCan_Apply_By().equals("Hours")) {
					
					driver.findElement(
							By.xpath(".//*[@id='cancellationApplicablType_H']"))
							.click();
				}
				
				driver.findElement(By.id("applicablearrivalpriod")).clear();
				driver.findElement(By.id("applicablearrivalpriod")).sendKeys(actPolicies.getCan_Less_than());
				driver.findElement(By.id("cancellationbuffer")).clear();
				driver.findElement(By.id("cancellationbuffer")).sendKeys(actPolicies.getCan_buffer());
				
				if (actPolicies.getStandard_Can_BasedOn().equals("percentage")) {
					
					driver.findElement(
							By.xpath(".//*[@id='cancellationfeebasedon_per']"))
							.click();
				}
						
				driver.findElement(By.id("cancellationamount")).sendKeys(actPolicies.getStandard_Can_Value());
				
				
				
				if (actPolicies.getNo_Show_Can_BasedOn().equals("percentage")) {
					
					driver.findElement(
							By.xpath(".//*[@id='noshowcancellationfeebasedon_per']"))
							.click();
				}
					
				driver.findElement(By.id("noshowcancellationamount")).sendKeys(actPolicies.getNo_Show_Can_Value());
				
				driver.findElement(
						By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input"))
						.click();
				
				driver.switchTo().defaultContent();
				Thread.sleep(2500);
			
			}
			
			
			ArrayList<Tax> taxList = activityStandard.getActivityTax();
			
			for(Tax actTaxes : taxList)
			{		
				//Tax
				driver.switchTo().frame("programtaxesdetails");
				driver.findElement(
						By.xpath("html/body/table/tbody/tr[1]/td/table/tbody/tr/td/img"))
						.click();
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");
				
				String TaxFromdate = actTaxes.getTaxes_From();
				String[] TFparts1 = TaxFromdate.split("-");
				String TFpart11 = TFparts1[0]; 
				String TFpart21 = TFparts1[1]; 
				String TFpart31 = TFparts1[2]; 
				
				Thread.sleep(1000);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("taxesfrom_Month_ID"))).selectByVisibleText(TFpart21);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("taxesfrom_Year_ID"))).selectByVisibleText(TFpart31);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("taxesfrom_Day_ID"))).selectByVisibleText(TFpart11);
				
				String TaxTodate = actTaxes.getTaxes_To();
				String[] TTparts1 = TaxTodate.split("-");
				String TTpart11 = TTparts1[0]; 
				String TTpart21 = TTparts1[1]; 
				String TTpart31 = TTparts1[2]; 
				
				Thread.sleep(1000);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("taxesto_Month_ID"))).selectByVisibleText(TTpart21);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("taxesto_Year_ID"))).selectByVisibleText(TTpart31);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("taxesto_Day_ID"))).selectByVisibleText(TTpart11);
				
				if (actTaxes.getSales_Tax().equals("Fixed Value")) {
					
					driver.findElement(
							By.xpath(".//*[@id='salesTaxType_F']"))
							.click();
				}
				
				driver.findElement(By.id("salestax")).clear();
				driver.findElement(By.id("salestax")).sendKeys(actTaxes.getSales_Tax_Value());
				
				
				if (actTaxes.getMis_Tax().equals("Fixed Value")) {
					
					driver.findElement(
							By.xpath(".//*[@id='miscFeeType_F']"))
							.click();
				}
				
				driver.findElement(By.id("miscfees")).clear();
				driver.findElement(By.id("miscfees")).sendKeys(actTaxes.getMis_Tax_Value());
				
				driver.findElement(
						By.xpath(".//*[@id='dialogframetable']/tbody/tr[2]/td/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input"))
						.click();
				
				driver.switchTo().defaultContent();
				Thread.sleep(2000);
				
			}
			
			driver.findElement(By.id("saveButId")).click();
			Thread.sleep(4000);
			
			
			
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

				if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
					String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
					// System.out.println(TextMessage);
					
					ExtentReportTemplate.getInstance().addtoReport("Activity addPolicies creation function ", " Activity addPolicies page should be saved", TextMessage, true);
					
					driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
					
				} else {

					String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
					// System.out.println(TextMessage);
				
					ExtentReportTemplate.getInstance().addtoReport("Activity addPolicies creation function "+activityStandard.getActivity_Program_Name(), "Activity addPolicies page should be saved", TextMessage, false);
					
					driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
					
				}

			} catch (Exception e) {
				
				ExtentReportTemplate.getInstance().addtoReport("Activity addPolicies creation function "+activityStandard.getActivity_Program_Name(), "Activity addPolicies page should be saved", e.toString(), false);
				// System.out.println(e.toString());

			}
				
			
			
	//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));
	//		driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
			Thread.sleep(2000);
			
		return driver;
		
	}
	
	
	
	public WebDriver addContent(WebDriver webDriver) throws InterruptedException{
		
		
	//	driver.get(PG_Properties.getProperty("Hotel.Baseurl") + "/activities/setup/ProgramSetupContentPage.do");
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/ProgramSetupContentPage.do");
		Thread.sleep(2500);	
		
		driver.findElement(By.id("programname")).clear();
		driver.findElement(By.id("programname")).sendKeys(activityStandard.getActivity_Program_Name());
		driver.findElement(By.id("programname_lkup")).click();
		driver.switchTo().frame("lookup");
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath(".//*[@id='scrollerAllID']/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();	
		Thread.sleep(7000);
		driver.switchTo().frame("texteditor");
		driver.switchTo().frame("mce_editor_0");
		
		driver.findElement(By.xpath("/html")).sendKeys(activityStandard.getActDescription());
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[2]/img")).click();
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		
		//ProgramNotes
		
		driver.findElement(By.xpath(".//*[@id='scrollerAllID']/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td[3]/a/img")).click();	
		Thread.sleep(7000);  
		driver.switchTo().frame("texteditor");
		driver.switchTo().frame("mce_editor_0");
		
		driver.findElement(By.xpath("/html")).sendKeys("The program note is a standard element of a concert where contemporary or classical music is being performed.");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("texteditor");
		driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[2]/img")).click();
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		
		
		Thread.sleep(2000);
		driver.findElement(By.id("saveButId")).click();
		Thread.sleep(1000);
		
		WebDriverWait wait = new WebDriverWait(driver, 120);	
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
			// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

			if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
				String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
				// System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("Activity addContent creation function ", " Activity addContent page should be saved", TextMessage, true);
				
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				
			} else {

				String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				// System.out.println(TextMessage);
			
				ExtentReportTemplate.getInstance().addtoReport("Activity addContent creation function "+activityStandard.getActivity_Program_Name(), "Activity addContent page should be saved", TextMessage, false);
				
				driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				
			}

		} catch (Exception e) {
			
			ExtentReportTemplate.getInstance().addtoReport("Activity addContent creation function "+activityStandard.getActivity_Program_Name(), "Activity addContent page should be saved", e.toString(), false);
			// System.out.println(e.toString());

		}
			
		
		
	//	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));
	//	driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
		Thread.sleep(1000);
		
	
	return driver;
}
	
		
	
	public WebDriver addImages(WebDriver webDriver) throws InterruptedException{
			
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/ProgramSetupImagesPage.do");
		Thread.sleep(1000);	
		
		driver.findElement(By.id("programname")).clear();
		driver.findElement(By.id("programname")).sendKeys(activityStandard.getActivity_Program_Name());
		driver.findElement(By.id("programname_lkup")).click();
		driver.switchTo().frame("lookup");
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		
		// Image Uploader
		for (int i = 1; i < 4; i++) {

			Thread.sleep(4500);
			driver.findElement(By.xpath(".//*[@id='scrollerAllID']/table[2]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td/table/tbody/tr/td/img")).click();
			Thread.sleep(4000);
			driver.switchTo().frame("dialogwindow");
			

			try {
				if (i == 1)
					driver.findElement(By.id("thumbnailimage_Y")).click();
				else
					driver.findElement(By.id("caption")).sendKeys(""+i);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			String FullPath = new File(activityStandard.getActImagesPath()).getAbsolutePath();
			driver.findElement(By.id("theFile")).sendKeys(FullPath + System.getProperty("file.separator") + i + ".jpg");
			
			driver.findElement(By.id("saveButId")).click();
			Thread.sleep(2000);
			
			
			WebDriverWait wait = new WebDriverWait(driver, 120);	
			try {
			//	wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
                driver.switchTo().defaultContent();
				if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
					String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
					// System.out.println(TextMessage);
					
					ExtentReportTemplate.getInstance().addtoReport("Activity addImages creation function "+activityStandard.getActivity_Program_Name(), " Activity addImages page should be saved", TextMessage, true);
					
					driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
					
				} else {

					String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
					// System.out.println(TextMessage);
				
					ExtentReportTemplate.getInstance().addtoReport("Activity addImages creation function "+activityStandard.getActivity_Program_Name(), "Activity addImages page should be saved", TextMessage, false);
					
					driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
					
				}

			} catch (Exception e) {
				driver.switchTo().frame("dialogwindow");
				driver.findElement(By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img")).click();
				ExtentReportTemplate.getInstance().addtoReport("Activity addImages creation function "+activityStandard.getActivity_Program_Name(), "Activity addImages page should be saved", e.toString(), false);
				// System.out.println(e.toString());

			}
				
			
			driver.switchTo().defaultContent();
			
			
	//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));
	//		driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
			Thread.sleep(3500);
		}
		
	

	return driver;
	
}
	
	
	
	public WebDriver assignActivities(WebDriver Driver) throws InterruptedException{	
		
			driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/AssignProgramActivityPeriodPage.do?module=contract");
			Thread.sleep(2000);	
			
			
			ArrayList<AssignActivity> assignActivityList = activityStandard.getAssignActivity();
			
			for(AssignActivity actAssignActivity : assignActivityList)
			{
				Thread.sleep(4000);
				driver.findElement(By.xpath(".//*[@id='newCombinationButton']")).click();
				driver.switchTo().frame("dialogwindow");
				
				driver.findElement(By.id("activityType")).sendKeys(actAssignActivity.getActivity_Type());
				driver.findElement(By.id("activityType_lkup")).click();	
				driver.switchTo().defaultContent();
				driver.switchTo().frame("lookup");
				Thread.sleep(2000);	
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();		
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");
				Thread.sleep(3000);
				
				driver.findElement(By.id("period")).sendKeys(actAssignActivity.getActivity_Period());
				driver.findElement(By.id("period_lkup")).click();	
				driver.switchTo().defaultContent();
				driver.switchTo().frame("lookup");
				Thread.sleep(2000);	
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");
				Thread.sleep(3000);
				
				driver.findElement(By.id("ratePlan")).sendKeys(actAssignActivity.getActivity_RatePlan());
				driver.findElement(By.id("ratePlan_lkup")).click();	
				driver.switchTo().defaultContent();
				driver.switchTo().frame("lookup");
				Thread.sleep(2000);	
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				driver.switchTo().defaultContent();
				driver.switchTo().frame("dialogwindow");
				Thread.sleep(3000);
				
				if (actAssignActivity.getMin_Max_Rate().equals("No")) {
					
					driver.findElement(
							By.xpath(".//*[@id='minmaxdisplay_N']"))
							.click();
				}
				
				if (actAssignActivity.getCombination_Active().equals("No")) {
					
					driver.findElement(
							By.xpath(".//*[@id='combinationactive_no']"))
							.click();
				}
				
				Thread.sleep(2000);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.name("activityOrder"))).selectByVisibleText(actAssignActivity.getDisplay_Order());
				
				Thread.sleep(3000);
				driver.findElement(By.xpath(".//*[@id='saveButId']")).click();	
				

				WebDriverWait wait = new WebDriverWait(driver, 120);	
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
					// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

					if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
						String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						// System.out.println(TextMessage);
						
						ExtentReportTemplate.getInstance().addtoReport("Activity assignActivities creation function ", " Activity assignActivities page should be saved", TextMessage, true);
						
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
						
					} else {

						String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						// System.out.println(TextMessage);
					
						ExtentReportTemplate.getInstance().addtoReport("Activity assignActivities creation function "+activityStandard.getActivity_Program_Name(), "Activity assignActivities page should be saved", TextMessage, false);
						
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
						
					}

				} catch (Exception e) {
					
					ExtentReportTemplate.getInstance().addtoReport("Activity assignActivities creation function "+activityStandard.getActivity_Program_Name(), "Activity assignActivities page should be saved", e.toString(), false);
					// System.out.println(e.toString());

				}
				
				
				driver.switchTo().defaultContent();
				Thread.sleep(4000);
				
			}
			
			
		return driver;
	}	
	
	
	
	public WebDriver addActivityInventories(WebDriver Driver) throws InterruptedException {
		
 			WebDriverWait wait = new WebDriverWait(driver, 120);
			driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/admin/inventoryrate/mainOperationalAction.do?screenModuleId=11");
			Thread.sleep(1500);	
			
			ArrayList<ActivityInventory> inventoryList = activityStandard.getActivityInventory();
			
			driver.switchTo().frame("inventoryRateIframe");
			
			for (ActivityInventory actInventory : inventoryList) {
			
				Thread.sleep(3000);
				//driver.findElement(By.id("programName")).sendKeys(activityStandard.getActivity_Program_Name());
				try {
					
					driver.findElement(By.id("programName_lkup")).click();		
					driver.switchTo().frame("lookup");
					Thread.sleep(2000);	
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame("inventoryRateIframe");
					
				//	driver.findElement(By.id("toRegionName")).clear();
				//	driver.findElement(By.id("toRegionName")).sendKeys(actInventory.getRegion());
				//	driver.findElement(By.id("toRegionName_lkup")).click();		
				//	driver.switchTo().frame("lookup");
				//	Thread.sleep(1000);	
				//	driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				//	Thread.sleep(1000);
				//	driver.switchTo().defaultContent();
					
					
			//		driver.findElement(By.id("customerName")).clear();
			//		driver.findElement(By.id("customerName")).sendKeys(actInventory.getTour_Operator());
			//		driver.findElement(By.id("customerName_lkup")).click();		
			//		driver.switchTo().frame("lookup");
			//		Thread.sleep(1000);	
			//		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			//		Thread.sleep(1000);
			//		driver.switchTo().defaultContent();
					
				} catch (Exception e1) {
					
					e1.printStackTrace();
				}
				
				Thread.sleep(5500);
				String profitFDate = actInventory.getContract_from();
				String[] partsPF = profitFDate.split("-");
				String ConFDay = partsPF[0]; 
				String ConFMonth = partsPF[1]; 
				String ConFYear = partsPF[2]; 
				Thread.sleep(5500);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodFrom_Month_ID"))).selectByVisibleText(ConFMonth);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodFrom_Year_ID"))).selectByVisibleText(ConFYear);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodFrom_Day_ID"))).selectByVisibleText(ConFDay);
				
				String profitToDate = actInventory.getContract_to();
				String[] partsPT = profitToDate.split("-");
				String ConToDay = partsPT[0]; 
				String ConToMonth = partsPT[1]; 
				String ConToYear = partsPT[2]; 
				Thread.sleep(1500);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodTo_Month_ID"))).selectByVisibleText(ConToMonth);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodTo_Year_ID"))).selectByVisibleText(ConToYear);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodTo_Day_ID"))).selectByVisibleText(ConToDay);
				
				driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/a/img")).click();
				Thread.sleep(3500);
				
				
				if(driver.getPageSource().contains("Activity / Period")){
							
					///////
					String activityperiod = actInventory.getActivity_Period();
					
					int[] activity = new int[100];		
					for (int i = 2; i < activity.length; i=i+2){
						
						WebElement ele = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[5]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]" +
								"/table/tbody/tr/td[2]/table/tbody/tr[3]/td[3]" +
								"/table/tbody/tr/td[" +i+ "]"));
												
						String Check = ele.findElement(By.tagName("label")).getText();
										
						if(Check.contains(activityperiod)){					
							
							String idact = ele.findElement(By.tagName("label")).getAttribute("id");
							driver.findElement(By.id(idact)).click();
							
							break;
						}				
					}	
					
				}
				
				Thread.sleep(2000);
				
				
				if (actInventory.getInventory_By().equals("Each day")) {
					
					driver.findElement(By.xpath(".//*[@id='fixInvType_eachDay']")).click();
					Thread.sleep(2000);
								
					try {
						if(driver.getPageSource().contains("Total inventory (units/day)")){
							
							Thread.sleep(2000);
							driver.findElement(By.id("totActivityArray[0]")).sendKeys(actInventory.getTotal_Inventory());
							driver.findElement(By.id("cutOffArray[0]_label_text")).click();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				
				if (actInventory.getInventory_By().equals("Mon to Thu / Fri to Sun")) {
					
					driver.findElement(By.xpath(".//*[@id='fixInvType_monToThu']")).click();
					Thread.sleep(2000);
								
					try {
						if(driver.getPageSource().contains("Total inventory (units/day)")){
							
							Thread.sleep(2000);
							driver.findElement(By.id("totActivityArray[1]")).sendKeys(actInventory.getTotal_Inventory());
							driver.findElement(By.id("cutOffArray[0]_label_text")).click();
											
							driver.findElement(By.id("totActivityArray[0]")).sendKeys(actInventory.getSunday_Inventory());
							
							driver.findElement(By.id("totActivityArray[5]")).clear();
							driver.findElement(By.id("totActivityArray[5]")).sendKeys(actInventory.getFriday_Inventory());
							driver.findElement(By.id("totActivityArray[6]")).clear();
							driver.findElement(By.id("totActivityArray[6]")).sendKeys(actInventory.getSaturday_Inventory());
								
						}
					} catch (Exception e) {
						
						e.printStackTrace();
					}					
				}
				
				
				Thread.sleep(2000);
				driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
				Thread.sleep(3000);
				
//				WebDriverWait wait = new WebDriverWait(driver, 120);	
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
					// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

					if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
						String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						// System.out.println(TextMessage);
						
						ExtentReportTemplate.getInstance().addtoReport("Activity addActivityInventories creation function ", " Activity addActivityInventories page should be saved", TextMessage, true);
						
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
						
					} else {

						String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						// System.out.println(TextMessage);
					
						ExtentReportTemplate.getInstance().addtoReport("Activity addActivityInventories creation function "+activityStandard.getActivity_Program_Name(), "Activity addActivityInventories page should be saved", TextMessage, false);
						
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
						
					}

				} catch (Exception e) {
					
					ExtentReportTemplate.getInstance().addtoReport("Activity addActivityInventories creation function "+activityStandard.getActivity_Program_Name(), "Activity addActivityInventories page should be saved", e.toString(), false);
					// System.out.println(e.toString());

				}
				
				
				
	//			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));
	//			driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();				
				Thread.sleep(3000);
			
			}
				
			
		return driver;
	}
	
	
	
	public WebDriver addActivityRates(WebDriver Driver) throws InterruptedException{
		
			WebDriverWait wait = new WebDriverWait(driver, 120);
			driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/admin/inventoryrate/mainOperationalAction.do?screenModuleId=11");
			
			driver.switchTo().frame("inventoryRateIframe");
			driver.findElement(By.xpath(".//*[@id='_lkinid']")).click();
   			Thread.sleep(2000);
			
			
			
  			ArrayList<ActivityRate> rateList = activityStandard.getActivityRates();
			
			for (ActivityRate actRate : rateList) {
			
				Thread.sleep(2000);
				//driver.findElement(By.id("programName")).sendKeys(activityStandard.getActivity_Program_Name());
				driver.findElement(By.id("programName_lkup")).click();		
				driver.switchTo().frame("lookup");
				Thread.sleep(1000);	
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame("inventoryRateIframe");
				
				//Select Region
				
				driver.findElement(By.id("toRegionName")).clear();
				driver.findElement(By.id("toRegionName")).sendKeys(actRate.getRate_Region());
				driver.findElement(By.xpath(".//*[@id='toRegionName_lkup']")).click();		
				driver.switchTo().frame("lookup");
				Thread.sleep(1000);	
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame("inventoryRateIframe");
			
				String profitFDate = actRate.getRate_Contract_from();
				String[] partsPF = profitFDate.split("-");
				String ConFDay = partsPF[0]; 
				String ConFMonth = partsPF[1]; 
				String ConFYear = partsPF[2]; 
				
				Thread.sleep(2000);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodFrom_Month_ID"))).selectByVisibleText(ConFMonth);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodFrom_Year_ID"))).selectByVisibleText(ConFYear);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodFrom_Day_ID"))).selectByVisibleText(ConFDay);
				
				String profitToDate = actRate.getRate_Contract_to();
				String[] partsPT = profitToDate.split("-");
				String ConToDay = partsPT[0]; 
				String ConToMonth = partsPT[1]; 
				String ConToYear = partsPT[2]; 
				
				Thread.sleep(2000);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodTo_Month_ID"))).selectByVisibleText(ConToMonth);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodTo_Year_ID"))).selectByVisibleText(ConToYear);
				new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodTo_Day_ID"))).selectByVisibleText(ConToDay);
				
				driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/a/img")).click();
				Thread.sleep(2000);
				
				///////
				activityratetype = actRate.getRate_ActivityRatePeriod();
				
				int[] activity = new int[100];		
				for (int i = 1; i < activity.length; i++){
					
					WebElement ele = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[5]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table" +
							"/tbody/tr[3]/td[2]/table/tbody/tr/td[2]/table/tbody/tr[3]/td[3]/table/tbody" +
							"/tr[" +i+ "]/td[2]"));
					
					String Check = ele.findElement(By.tagName("label")).getText().trim();
					
					if(Check.contains(activityratetype)){					
						
						String idactrate = ele.findElement(By.tagName("label")).getAttribute("id");
						driver.findElement(By.id(idactrate)).click();
						
						break;
					}				
				}	
					
				///////
				
				if (actRate.getRate_FixRate().equals("Each day")) {
					
					driver.findElement(By.xpath(".//*[@id='fixRateType_eachDay']")).click();
					
					if (driver.getPageSource().contains("Net Rate")) {
						
						driver.findElement(By.id("netRateArray[0]")).sendKeys(actRate.getRate_NetRate());
						driver.findElement(By.id("netRateArray[0]_label_text")).click();
						
					}
					
					if (driver.getPageSource().contains("Sell Rate")) {
						
						driver.findElement(By.id("sellRateArray[0]")).clear();				
						driver.findElement(By.id("sellRateArray[0]")).sendKeys(actRate.getRate_SellRate());
						driver.findElement(By.id("sellRateArray[0]_label_text")).click();
					}			
				}
				
				if (actRate.getRate_FixRate().equals("Mon to Thu / Fri to Sun")) {
					
					driver.findElement(By.xpath(".//*[@id='fixRateType_monToThu']")).click();	
					
					//
					if (driver.getPageSource().contains("Net Rate")) {
						
						Thread.sleep(2000);
						driver.findElement(By.id("netRateArray[1]")).sendKeys(actRate.getRate_NetRate());
						driver.findElement(By.id("netRateArray[0]_label_text")).click();
						
						driver.findElement(By.id("netRateArray[0]")).sendKeys(actRate.getRate_Sunday_Inventory());
						driver.findElement(By.id("netRateArray[5]")).sendKeys(actRate.getRate_Friday_Inventory());
						driver.findElement(By.id("netRateArray[6]")).sendKeys(actRate.getRate_Saturday_Inventory());				
					}
					
					if (driver.getPageSource().contains("Sell Rate")) {
						
						Thread.sleep(2000);
						driver.findElement(By.id("sellRateArray[1]")).clear();
						driver.findElement(By.id("sellRateArray[1]")).sendKeys(actRate.getRate_SellRate());
						driver.findElement(By.id("sellRateArray[0]_label_text")).click();
						
						driver.findElement(By.id("sellRateArray[0]")).clear();
						driver.findElement(By.id("sellRateArray[0]")).sendKeys(actRate.getRate_Sunday_Inventory());
						driver.findElement(By.id("sellRateArray[5]")).clear();
						driver.findElement(By.id("sellRateArray[5]")).sendKeys(actRate.getRate_Friday_Inventory());
						driver.findElement(By.id("sellRateArray[6]")).clear();
						driver.findElement(By.id("sellRateArray[6]")).sendKeys(actRate.getRate_Saturday_Inventory());
						
					}
					
				}
				
				
				Thread.sleep(1000);
				driver.findElement(By.xpath(".//*[@id='ActivitiesRatesSetupPeriodBasisForm']/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
				
	//			WebDriverWait wait = new WebDriverWait(driver, 120);	
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
					// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

					if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
						String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
						// System.out.println(TextMessage);
						
						ExtentReportTemplate.getInstance().addtoReport("Activity addActivityRates creation function ", " Activity addActivityRates page should be saved", TextMessage, true);
						
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
						
					} else {

						String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
						// System.out.println(TextMessage);
					
						ExtentReportTemplate.getInstance().addtoReport("Activity addActivityRates creation function "+activityStandard.getActivity_Program_Name(), "Activity addActivityRates page should be saved", TextMessage, false);
						
						driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
						
					}

				} catch (Exception e) {
					
					ExtentReportTemplate.getInstance().addtoReport("Activity addActivityRates creation function "+activityStandard.getActivity_Program_Name(), "Activity addActivityRates page should be saved", e.toString(), false);
					// System.out.println(e.toString());

				}
				System.out.println();
				
				Thread.sleep(3000);
				
	//			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));
	//			driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				Thread.sleep(3000);
			}
				
		
		return driver;
	}
	
	
	
	public WebDriver addProfitMarkups(WebDriver Driver) throws InterruptedException{
		
			WebDriverWait wait = new WebDriverWait(driver, 120);
		
			ArrayList<ActivityProfit> activityProfitLists = activityStandard.getActivityProfits();
			
			for(ActivityProfit actProfits : activityProfitLists){
				
				driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/inventoryandrates/ProfitMarkupPeriodBasisPage.do?module=contract");
				Thread.sleep(5000);
				
					if (actProfits.getBooking_Channel().equals("Website")) {
						
						driver.findElement(
								By.xpath(".//*[@id='bookingChannel_WEB']"))
								.click();
					}
					
					if (actProfits.getBooking_Channel().equals("Call")) {
						
						driver.findElement(
								By.xpath(".//*[@id='bookingChannel_CC']"))
								.click();
					}
					
					
					
					if (actProfits.getPartner().equals("Affiliate Name")) {
								
								driver.findElement(
										By.xpath(".//*[@id='customerType_AFF']"))
										.click();
					}
					
					if (actProfits.getPartner().equals("DC")) {
						
						driver.findElement(By.xpath(".//*[@id='customerType_DC']")).click();
					}
					
					if (actProfits.getPartner().equals("B2B")) {
						
						driver.findElement(By.xpath(".//*[@id='customerType_TO']")).click();
					}
	
					
					
					
					driver.findElement(By.id("rateRegionName")).clear();
					driver.findElement(By.id("rateRegionName")).sendKeys(actProfits.getRate_Region());
					driver.findElement(By.id("rateRegionName_lkup")).click();		
					driver.switchTo().frame("lookup");
					Thread.sleep(1000);
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
					Thread.sleep(1000);
					driver.switchTo().defaultContent();
					
					if (actProfits.getProfitMarkup_Type().equals("Fixed Value")) {
						
						driver.findElement(
								By.xpath(".//*[@id='markupType_VAL']"))
								.click();
					}
					
					String profitFDate = actProfits.getProfit_Period_From();
					String[] partsPF = profitFDate.split("-");
					String ConFDay = partsPF[0]; 
					String ConFMonth = partsPF[1]; 
					String ConFYear = partsPF[2]; 
					
					Thread.sleep(2000);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodFrom_Month_ID"))).selectByVisibleText(ConFMonth);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodFrom_Year_ID"))).selectByVisibleText(ConFYear);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodFrom_Day_ID"))).selectByVisibleText(ConFDay);
					
					String profitToDate = actProfits.getProfit_Period_To();
					String[] partsPT = profitToDate.split("-");
					String ConToDay = partsPT[0]; 
					String ConToMonth = partsPT[1]; 
					String ConToYear = partsPT[2]; 
					
					Thread.sleep(2000);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodTo_Month_ID"))).selectByVisibleText(ConToMonth);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodTo_Year_ID"))).selectByVisibleText(ConToYear);
					new org.openqa.selenium.support.ui.Select(driver.findElement(By.id("periodTo_Day_ID"))).selectByVisibleText(ConToDay);
					
					driver.findElement(By.id("countryName")).clear();
					driver.findElement(By.id("countryName")).sendKeys(actProfits.getCountry());
					driver.findElement(By.id("countryName_lkup")).click();		
					driver.switchTo().frame("lookup");
					Thread.sleep(1000);
					driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();		
					driver.switchTo().defaultContent();
					Thread.sleep(2000);
					
					//Changed here
					
					driver.findElement(By.id("cityName")).clear();
					driver.findElement(By.id("cityName")).sendKeys(actProfits.getCity());
					driver.findElement(By.id("cityName_lkup")).click();		
					driver.switchTo().frame("lookup");
					Thread.sleep(1000);
					driver.findElement(By.xpath(".//*[@id='row-1']/td")).click();
					driver.switchTo().defaultContent();
					Thread.sleep(2000);
					
					driver.findElement(By.id("supplierName")).clear();
					driver.findElement(By.id("supplierName")).sendKeys(actProfits.getSupplier());
					driver.findElement(By.id("supplierName_lkup")).click();		
					driver.switchTo().frame("lookup");
					Thread.sleep(1000);
					driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
					driver.switchTo().defaultContent();
					Thread.sleep(2000);
					
					
					
					//All programs
					if (actProfits.getProfit_ApplyTo().equals("All Programs")) {
						
						driver.findElement(By.xpath(".//*[@id='programMode_H']")).click();
						Thread.sleep(500);
						
						if (actProfits.getSpecific_Markup().equals("Yes")) {
							
							driver.findElement(By.xpath(".//*[@id='overwriteMarkup_YES']")).click();
							Thread.sleep(1000);
							
							driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a[1]/img")).click();
							Thread.sleep(1500);
						}
						
						////////////
						
						if (actProfits.getFix_Profit_Markup().equals("Each day")) {
							
							driver.findElement(By.xpath(".//*[@id='fixMarkupType_eachDay']")).click();
							Thread.sleep(500);
							
							driver.findElement(By.id("profitMarkupArray[0]")).sendKeys(actProfits.getProfit_Markup_V());
							driver.findElement(By.xpath(".//*[@id='profitMarkupArray[0]_label_text']")).click();
							
						}
												
						if (actProfits.getFix_Profit_Markup().equals("Sun to Thu / Fri to Sat")) {
							
							driver.findElement(By.xpath(".//*[@id='fixMarkupType_sunToThu']")).click();
							Thread.sleep(500);
							
							driver.findElement(By.id("profitMarkupArray[0]")).sendKeys(actProfits.getProfit_Markup_V());
							driver.findElement(By.xpath(".//*[@id='profitMarkupArray[0]_label_text']")).click();
							
							driver.findElement(By.id("profitMarkupArray[5]")).sendKeys(actProfits.getFriday_Profit());
							driver.findElement(By.id("profitMarkupArray[6]")).sendKeys(actProfits.getSaturday_Profit());
							
						}
						
					}
					
					
					//Selected Programs
					if (actProfits.getProfit_ApplyTo().equals("Selected Programs")) {
						
						driver.findElement(By.xpath(".//*[@id='programMode_H']")).click();
						Thread.sleep(500);
						
						if (actProfits.getSpecific_Markup().equals("Yes")) {
							
							driver.findElement(By.xpath(".//*[@id='overwriteMarkup_YES']")).click();
							Thread.sleep(3000);
							
							driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a[1]/img")).click();
							Thread.sleep(1500);
						}
						
						driver.findElement(By.xpath(".//*[@id='idImgProgram']/a/img")).click();
						Thread.sleep(1500);
						
						///////////
						
						driver.switchTo().frame("profitmarkupprograms");
						
						String activityProgramme = activityStandard.getActivity_Program_Name();
						
						int[] activity = new int[100];			
						outerloop : for (int i = 1; i < activity.length; i++){
							
							for (int j = 1; j <= 2; j++) {
								
								try {
									
									String webActivityPeriod = driver.findElement(By.xpath(".//*[@id='idLoadedPrograms']/td/table/tbody/tr["+i+"]/td["+j+"]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]")).getText();
									Thread.sleep(1500);
									
									if(webActivityPeriod.replaceAll(" ", "").contains(activityProgramme.replaceAll(" ", ""))){					
										
										driver.findElement(By.xpath(".//*[@id='idLoadedPrograms']/td/table/tbody/tr["+i+"]/td["+j+"]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input")).click();
										Thread.sleep(1500);
										break outerloop;
									}
									
								} catch (Exception e) {
									e.printStackTrace();
								}	
								
							}			
										
						}	
						
						driver.switchTo().defaultContent();
						
						////////////
						
						if (actProfits.getFix_Profit_Markup().equals("Each day")) {
							
							driver.findElement(By.xpath(".//*[@id='fixMarkupType_eachDay']")).click();
							Thread.sleep(500);
							
							driver.findElement(By.id("profitMarkupArray[0]"))
														.sendKeys(actProfits.getProfit_Markup_V());
							driver.findElement(By.xpath(".//*[@id='profitMarkupArray[0]_label_text']")).click();
							
						}
						
						
						
						if (actProfits.getFix_Profit_Markup().equals("Sun to Thu / Fri to Sat")) {
							
							driver.findElement(By.xpath(".//*[@id='fixMarkupType_sunToThu']")).click();
							Thread.sleep(500);
							
							driver.findElement(By.id("profitMarkupArray[0]")).sendKeys(actProfits.getProfit_Markup_V());
							driver.findElement(By.xpath(".//*[@id='profitMarkupArray[0]_label_text']")).click();
							driver.findElement(By.id("profitMarkupArray[5]")).sendKeys(actProfits.getFriday_Profit());
							driver.findElement(By.id("profitMarkupArray[6]")).sendKeys(actProfits.getSaturday_Profit());
							
						}
						
					}
					
					
					//Selected Activity / Period
					if (actProfits.getProfit_ApplyTo().equals("Selected Activity Period")) {
						
						driver.findElement(By.xpath(".//*[@id='programMode_R']")).click();
						Thread.sleep(500);
						
							driver.findElement(By.id("activityProgramName")).clear();
							driver.findElement(By.id("activityProgramName")).sendKeys(actProfits.getSelect_Program());
							driver.findElement(By.id("activityProgramName_lkup")).click();		
							driver.switchTo().frame("lookup");
							Thread.sleep(1000);
							driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
							driver.switchTo().defaultContent();
							Thread.sleep(2000);
						
							if (actProfits.getMarkup_Rate().equals("No")) {
								
								driver.findElement(By.xpath(".//*[@id='needRateplan_N']")).click();
								Thread.sleep(500);
							}
								
							if (actProfits.getMarkup_Rate().equals("Yes")) {
								
								driver.findElement(By.xpath(".//*[@id='needRateplan_Y']")).click();
								Thread.sleep(500);
							}
						
						if (actProfits.getSpecific_Markup().equals("Yes")) {
							
							driver.findElement(By.xpath(".//*[@id='overwriteMarkup_YES']")).click();
							Thread.sleep(1000);
							
							driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a[1]/img")).click();
							Thread.sleep(1500);
						}
									
						driver.findElement(By.xpath(".//*[@id='idImgActivityPeriod']/a/img")).click();
						Thread.sleep(1500);
						
						//////////////
						
						driver.switchTo().frame("profitmarkupactivityperiod");
						
						String selectProfitActivity = actProfits.getSelectProfitActivity();
						
						
						int[] activity = new int[100];			
						outerloop : for (int i = 1; i < activity.length; i++){
							
							for (int j = 1; j <= 2; j++) {
								
								try {
									
									String webActivityPeriod = driver.findElement(By.xpath(".//*[@id='idLoadedPrograms']/td/table/tbody/tr["+i+"]/td["+j+"]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]")).getText();
									Thread.sleep(1500);
									
									if(webActivityPeriod.replaceAll(" ", "").contains(selectProfitActivity.replaceAll(" ", ""))){					
										
										driver.findElement(By.xpath(".//*[@id='idLoadedPrograms']/td/table/tbody/tr["+i+"]/td["+j+"]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/input")).click();
										
										break outerloop;
									}
									
								} catch (Exception e) {
									e.printStackTrace();
								}	
								
							}			
										
						}	
						
						driver.switchTo().defaultContent();
						
						///////////////
						
						if (actProfits.getFix_Profit_Markup().equals("Each day")) {
							
							driver.findElement(By.xpath(".//*[@id='fixMarkupType_eachDay']")).click();
							Thread.sleep(1000);
							
							driver.findElement(By.id("profitMarkupArray[0]")).sendKeys(actProfits.getProfit_Markup_V());
							driver.findElement(By.xpath(".//*[@id='profitMarkupArray[0]_label_text']")).click();
							
						}
						
						
						
						if (actProfits.getFix_Profit_Markup().equals("Sun to Thu / Fri to Sat")) {
							
							driver.findElement(By.xpath(".//*[@id='fixMarkupType_sunToThu']")).click();
							Thread.sleep(500);
							
							driver.findElement(By.id("profitMarkupArray[0]")).sendKeys(actProfits.getProfit_Markup_V());
							driver.findElement(By.xpath(".//*[@id='profitMarkupArray[0]_label_text']")).click();
							
							driver.findElement(By.id("profitMarkupArray[5]")).sendKeys(actProfits.getFriday_Profit());
							driver.findElement(By.id("profitMarkupArray[6]")).sendKeys(actProfits.getSaturday_Profit());
							
						}
						
						
					}
				
					Thread.sleep(2000);
					driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[1]/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/a/img")).click();
					Thread.sleep(3000);
					
					try {
						wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
						// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

						if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
							String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
							// System.out.println(TextMessage);
							
							ExtentReportTemplate.getInstance().addtoReport("Activity addProfitMarkups creation function ", " Activity addProfitMarkups page should be saved", TextMessage, true);
							
							driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
							
						} else {

							String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
							// System.out.println(TextMessage);
						
							ExtentReportTemplate.getInstance().addtoReport("Activity addProfitMarkups creation function "+activityStandard.getActivity_Program_Name(), "Activity addProfitMarkups page should be saved", TextMessage, false);
							
							driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
							
						}

					} catch (Exception e) {
						
						ExtentReportTemplate.getInstance().addtoReport("Activity addProfitMarkups creation function "+activityStandard.getActivity_Program_Name(), "Activity addProfitMarkups page should be saved", e.toString(), false);
						// System.out.println(e.toString());

					}
					
		//			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));
		//			driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
					Thread.sleep(5000);
			}
			
			
		return driver;
	}
	
	
	
	//Delete
	public WebDriver deleteActivityStandard(ActivityStandard activitystandard) throws InterruptedException{
		
			driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/activities/setup/ProgramSetupStandardPage.do?module=contract");
			Thread.sleep(1500);	
			
			driver.findElement(
					By.xpath(".//*[@id='screenaction_delete']"))
					.click();
			Thread.sleep(1000);
			
			driver.findElement(By.id("programname")).sendKeys(activitystandard.getActivity_Program_Name());
			
			driver.findElement(By.id("programname_lkup")).click();		
			driver.switchTo().frame("lookup");
			Thread.sleep(1000);
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			Thread.sleep(1000);
			driver.switchTo().defaultContent();
			
			driver.findElement(By.id("saveButId")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
			Thread.sleep(2000);
			
			
		return driver;
	}
	
	
	
	
}
