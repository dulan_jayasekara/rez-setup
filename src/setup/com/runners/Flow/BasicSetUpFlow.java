package setup.com.runners.Flow;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import setup.com.loaders.*;
import setup.com.pojo.*;
import setup.com.utill.ExtentReportTemplate;

public class BasicSetUpFlow {
		
	private WebDriver      driver         = null;
	JavascriptExecutor     javaScriptExe = (JavascriptExecutor) driver;	
	
	private LoadBasicDetails detailsLoder;
	HotelBasicInfoSetupFlow hotelBasicInfo = null;
	
	ArrayList<Map<Integer, String>> Sheetlist = new ArrayList<Map<Integer, String>>(); 
				
	ArrayList<BedType>      BedTypes          = new ArrayList<BedType>();
	ArrayList<HotelGroup>   HotelGroupTypes   = new ArrayList<HotelGroup>();	
	ArrayList<RatePlan>     RateTypes         = new ArrayList<RatePlan>();
	ArrayList<RoomType>     RoomTypes         = new ArrayList<RoomType>();
	ArrayList<StarCategory> StarCTypes        = new ArrayList<StarCategory>();
	private Map<String, String>   Propertymap = null;;
	
	
	private StringBuffer PrintWriter;
	private int TestCaseCount;
	
	
	@Before
	public void setUp(Map<String, String> propertymap,ArrayList<Map<Integer, String>> BasicList){
		
		Propertymap   = propertymap;
		
		detailsLoder            = new LoadBasicDetails(BasicList);
		BedTypes   			 	= detailsLoder.loadBedTypes();
		HotelGroupTypes 		= detailsLoder.loadHotelGroups();
		RateTypes 				= detailsLoder.loadRateTypes();
		RoomTypes 				= detailsLoder.loadRoomTypes();
		StarCTypes 				= detailsLoder.loadStartTypes();
		PrintWriter      = new StringBuffer();
		
		
		 		
	}
	
	@Test
	public void hotelSetup(WebDriver Driver) throws InterruptedException{
		this.driver  = Driver;		
		hotelBasicInfo   = new HotelBasicInfoSetupFlow(Propertymap.get("Hotel.BaseUrl"),driver);
		PrintWriter.append("<HTML><HEAD><LINK href=\"Text.css\" rel=\"stylesheet\" type=\"text/css\"></HEAD>");
		PrintWriter.append("<body>");
		PrintWriter.append("<P class=\"special\">Html report for hotel Basic informations</p>");
		PrintWriter.append("<br><br>");
		
		
		
		PrintWriter.append("<table class=\"tg\"> <tr> <th class=\"tg-oqg4\">TestCaseID</th> <th class=\"tg-oqg4\">TestDescription</th> <th class=\"tg-oqg4\">Expected Results</th> <th class=\"tg-oqg4\">Actual Results</th> <th class=\"tg-oqg4\">Pass/Fail</th> </tr>");
		TestCaseCount = 1;

		
	    if(Propertymap.get("Details.Flow").trim().equals("Create")){	
		
			Iterator<BedType>  bedIterator  = BedTypes.iterator();			
			while(bedIterator.hasNext())
			{
				TestCaseCount++;
				PrintWriter.append("<tr> <td class=\"tg-e3zv\">"+TestCaseCount+"</td>  <td  class=\"tg-e3zv\">Create Hotel bed</td>");
				PrintWriter.append("<td class=\"tg-e3zv\">Hotel beds should be created</td>");
				
				try {
					hotelBasicInfo.createHotelBeds(bedIterator.next());
					PrintWriter.append("<td class=\"tg-e3zv\">Success..!!!</td>");
					PrintWriter.append("<td class=\"tg-6wb1\">Pass</td>  </tr>");
					
				} catch (Exception e) {
					PrintWriter.append("<td class=\"tg-e3zv\">Errorrr..!!!!</td>");
					PrintWriter.append("<td class=\"tg-fzjz\">Fail</td>  </tr>");
				}				
			}
	 		
						
			Iterator<HotelGroup>  groupIterator  = HotelGroupTypes.iterator();
			while(groupIterator.hasNext())
			{	
				TestCaseCount++;
				PrintWriter.append("<tr> <td class=\"tg-e3zv\">"+TestCaseCount+"</td>  <td  class=\"tg-e3zv\">Create Hotel group</td>");
				PrintWriter.append("<td class=\"tg-e3zv\">Hotel groups should be created</td>");
				
				try {
					hotelBasicInfo.createHotelGroup(groupIterator.next());
					PrintWriter.append("<td class=\"tg-e3zv\">Success..!!!</td>");
					PrintWriter.append("<td class=\"tg-6wb1\">Pass</td>  </tr>");
					
				} catch (Exception e) {
					PrintWriter.append("<td class=\"tg-e3zv\">Errorrr..!!!!</td>");
					PrintWriter.append("<td class=\"tg-fzjz\">Fail</td>  </tr>");
				}											
			}
						
			
			Iterator<RatePlan>  rateIterator  = RateTypes.iterator();
			while(rateIterator.hasNext())
			{		
				TestCaseCount++;
				PrintWriter.append("<tr> <td class=\"tg-e3zv\">"+TestCaseCount+"</td>  <td  class=\"tg-e3zv\">Create Rate plan</td>");
				PrintWriter.append("<td class=\"tg-e3zv\">Hotel groups should be created</td>");
				
				try {
					hotelBasicInfo.createRatePlan(rateIterator.next());
					PrintWriter.append("<td class=\"tg-e3zv\">Success..!!!</td>");
					PrintWriter.append("<td class=\"tg-6wb1\">Pass</td>  </tr>");
					
				} catch (Exception e) {
					PrintWriter.append("<td class=\"tg-e3zv\">Errorrr..!!!!</td>");
					PrintWriter.append("<td class=\"tg-fzjz\">Fail</td>  </tr>");
				}								
			}
			
			
			Iterator<RoomType>  roomIterator  = RoomTypes.iterator();
			while(roomIterator.hasNext())
			{			
				TestCaseCount++;
				PrintWriter.append("<tr> <td class=\"tg-e3zv\">"+TestCaseCount+"</td>  <td  class=\"tg-e3zv\">Create room type</td>");
				PrintWriter.append("<td class=\"tg-e3zv\">Hotel rooms should be created</td>");
				
				try {
					hotelBasicInfo.createRoomType(roomIterator.next());
					PrintWriter.append("<td class=\"tg-e3zv\">Success..!!!</td>");
					PrintWriter.append("<td class=\"tg-6wb1\">Pass</td>  </tr>");
					
				} catch (Exception e) {
					PrintWriter.append("<td class=\"tg-e3zv\">Errorrr..!!!!</td>");
					PrintWriter.append("<td class=\"tg-fzjz\">Fail</td>  </tr>");
				}	
								
			}
			
/*			Iterator<StarCategory>  starIterator  = StarCTypes.iterator();
			while(starIterator.hasNext())
			{			
				TestCaseCount++;
				PrintWriter.append("<tr> <td class=\"tg-e3zv\">"+TestCaseCount+"</td>  <td  class=\"tg-e3zv\">Create room type</td>");
				PrintWriter.append("<td class=\"tg-e3zv\">Hotel StarCat should be created</td>");
				
				try {
					hotelBasicInfo.createStarCategory(starIterator.next());
					PrintWriter.append("<td class=\"tg-e3zv\">Success..!!!</td>");
					PrintWriter.append("<td class=\"tg-6wb1\">Pass</td>  </tr>");
					
				} catch (Exception e) {
					PrintWriter.append("<td class=\"tg-e3zv\">Errorrr..!!!!</td>");
					PrintWriter.append("<td class=\"tg-fzjz\">Fail</td>  </tr>");
				}	
								
			}
			*/
		}
		
		
		//Modification		
		if(Propertymap.get("Details.Flow").equals("Modify")){
			
			Iterator<BedType>  bedIterator  = BedTypes.iterator();
			while(bedIterator.hasNext())
			{			
				hotelBasicInfo.modifyHotelBeds(bedIterator.next());
				
			}
			
			Iterator<RoomType>  roomIterator  = RoomTypes.iterator();
			while(roomIterator.hasNext())
			{			
				hotelBasicInfo.modifyRoomType(roomIterator.next());
				
			}
			
		
		}
		
		
		
		//Delete		
		if(Propertymap.get("Details.Flow").equals("Delete")){
			
			Iterator<BedType>  bedIterator  = BedTypes.iterator();
			while(bedIterator.hasNext())
			{			
				TestCaseCount++;
				PrintWriter.append("<tr> <td class=\"tg-e3zv\">"+TestCaseCount+"</td>  <td  class=\"tg-e3zv\">Delete Hotel bed</td>");
				PrintWriter.append("<td class=\"tg-e3zv\">Hotel bed should be removed from the system.</td>");
				
				try {
					hotelBasicInfo.deleteHotelBeds(bedIterator.next());
					PrintWriter.append("<td class=\"tg-e3zv\">successfully removed..!!!</td>");
					PrintWriter.append("<td class=\"tg-6wb1\">Pass</td>  </tr>");
					
				} catch (Exception e) {
					PrintWriter.append("<td class=\"tg-e3zv\">Errorrr..!!!!</td>");
					PrintWriter.append("<td class=\"tg-fzjz\">Fail</td>  </tr>");
				}				
								
			}
	 		
			Iterator<HotelGroup>  groupIterator  = HotelGroupTypes.iterator();
			while(groupIterator.hasNext())
			{			
				TestCaseCount++;
				PrintWriter.append("<tr> <td class=\"tg-e3zv\">"+TestCaseCount+"</td>  <td  class=\"tg-e3zv\">Delete Hotel group</td>");
				PrintWriter.append("<td class=\"tg-e3zv\">Hotel group should be removed from the system.</td>");
				
				try {
					hotelBasicInfo.deleteHotelGroup(groupIterator.next());
					PrintWriter.append("<td class=\"tg-e3zv\">successfully removed..!!!</td>");
					PrintWriter.append("<td class=\"tg-6wb1\">Pass</td>  </tr>");
					
				} catch (Exception e) {
					PrintWriter.append("<td class=\"tg-e3zv\">Errorrr..!!!!</td>");
					PrintWriter.append("<td class=\"tg-fzjz\">Fail</td>  </tr>");
				}	
								
			}
			
			
			Iterator<RatePlan>  rateIterator  = RateTypes.iterator();
			while(rateIterator.hasNext())
			{			
				TestCaseCount++;
				PrintWriter.append("<tr> <td class=\"tg-e3zv\">"+TestCaseCount+"</td>  <td  class=\"tg-e3zv\">Delete Hotel rate</td>");
				PrintWriter.append("<td class=\"tg-e3zv\">Hotel rate should be removed.</td>");
				
				try {
					hotelBasicInfo.deleteHotelRatePlan(rateIterator.next());
					PrintWriter.append("<td class=\"tg-e3zv\">successfully deleted..!!!</td>");
					PrintWriter.append("<td class=\"tg-6wb1\">Pass</td>  </tr>");
					
				} catch (Exception e) {
					PrintWriter.append("<td class=\"tg-e3zv\">Errorrr..!!!!</td>");
					PrintWriter.append("<td class=\"tg-fzjz\">Fail</td>  </tr>");
				}					
				
			}
			
			
			Iterator<RoomType>  roomIterator  = RoomTypes.iterator();
			while(roomIterator.hasNext())
			{			
				TestCaseCount++;
				PrintWriter.append("<tr> <td class=\"tg-e3zv\">"+TestCaseCount+"</td>  <td  class=\"tg-e3zv\">Delete Room type</td>");
				PrintWriter.append("<td class=\"tg-e3zv\">Hotel room type should be removed.</td>");
				
				try {
					hotelBasicInfo.deleteHotelRoom(roomIterator.next());
					PrintWriter.append("<td class=\"tg-e3zv\">successfully removed..!!!</td>");
					PrintWriter.append("<td class=\"tg-6wb1\">Pass</td>  </tr>");
					
				} catch (Exception e) {
					PrintWriter.append("<td class=\"tg-e3zv\">Errorrr..!!!!</td>");
					PrintWriter.append("<td class=\"tg-fzjz\">Fail</td>  </tr>");
				}									
			}
			
		}
		
		PrintWriter.append("</table>");
				
	}
	
	
	
	@After
	public void tearDown() throws IOException{
		PrintWriter.append("</body></html>");
		  
		  BufferedWriter bwr = new BufferedWriter(new FileWriter(new File("HotelInfo.html")));
		  bwr.write(PrintWriter.toString());
		  bwr.flush();
		  bwr.close();
	      driver.quit();
	}
		
}

	
