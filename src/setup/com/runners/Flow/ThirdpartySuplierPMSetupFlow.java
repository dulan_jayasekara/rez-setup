package setup.com.runners.Flow;

import java.sql.Driver;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import setup.com.pojo.ThirdpartySupplierPM;
import setup.com.utill.ExtentReportTemplate;

public class ThirdpartySuplierPMSetupFlow {

	public void setThirdpartysupplierprofitmarkup(WebDriver driver,
			ThirdpartySupplierPM thirdpartysupplierpm, String url)
			throws InterruptedException {
		
		
		WebDriverWait wait = new WebDriverWait(driver, 120);

		driver.get(url
				+ "/admin/setup/RideSupplierProfitMarkupPage.do?module=contract");

		System.out.println("ckckck");

		if (thirdpartysupplierpm.getBookingChannel().trim()
				.equalsIgnoreCase("All")) {

			driver.findElement(By.id("bookingChannel_ALL")).click();
		}

		else if (thirdpartysupplierpm.getBookingChannel().trim()
				.equalsIgnoreCase("Web")) {

			driver.findElement(By.id("bookingChannel_WEB")).click();

		} else if (thirdpartysupplierpm.getBookingChannel().trim()
				.equalsIgnoreCase("Call Center")) {

			driver.findElement(By.id("bookingChannel_CC")).click();

		}

		if (thirdpartysupplierpm.getPartner().trim().equalsIgnoreCase("Direct")) {

			driver.findElement(By.id("customerType_DC")).click();
			driver.findElement(By.id("regionname")).clear();
			driver.findElement(By.id("regionname")).sendKeys(
					thirdpartysupplierpm.getRegion());

			driver.findElement(By.id("regionname_lkup")).click();

			driver.switchTo().frame("lookup");

			if (driver.findElements(By.className("rezgLook0")).get(0).getText()
					.equalsIgnoreCase("all")) {

				System.out.println("All - Record existed  on lookup");
				driver.findElements(By.className("rezgLook0")).get(1).click();

			}

			else {
				driver.findElements(By.className("rezgLook0")).get(0).click();

			}

			driver.switchTo().defaultContent();

		}

		else if (thirdpartysupplierpm.getPartner().trim()
				.equalsIgnoreCase("Affiliate Name")) {

			driver.findElement(By.id("customerType_AFF")).click();
		}

		else if (thirdpartysupplierpm.getPartner().trim()
				.equalsIgnoreCase("B2B")) {
			driver.findElement(By.id("customerType_TO")).click();
			driver.findElement(By.id("regionname")).clear();
			driver.findElement(By.id("regionname")).sendKeys(
					thirdpartysupplierpm.getRegion());
			driver.findElement(By.id("regionname_lkup")).click();

			driver.switchTo().frame("lookup");

			if (driver.findElements(By.className("rezgLook0")).get(0).getText()
					.equalsIgnoreCase("all")) {

				System.out.println("All Record existed  on lookup");
				driver.findElements(By.className("rezgLook0")).get(1).click();

			}

			else {
				driver.findElements(By.className("rezgLook0")).get(0).click();

			}

			

			driver.switchTo().defaultContent();

			driver.findElement(By.id("tourOperatorName")).clear();
			driver.findElement(By.id("tourOperatorName")).sendKeys("ALL");

			driver.findElement(By.xpath(".//*[@id='tourOperatorName_lkup']"))
					.click();
			wait.until(ExpectedConditions.presenceOfElementLocated(By
					.id("lookup")));
			driver.switchTo().frame("lookup");
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			driver.switchTo().defaultContent();
			System.out.println("success");

		}
		
		
		
		if(thirdpartysupplierpm.getProfitMarkupType().trim().equalsIgnoreCase("Percentage")){
			
			driver.findElement(By.id("markupType_PCT")).click();		
		}
		else if (thirdpartysupplierpm.getProfitMarkupType().trim().equalsIgnoreCase("Fix Value")) {
			driver.findElement(By.id("markupType_VAL")).click();
			
		}
		
		
		
		Select fromcombo = new Select(driver.findElement(By.id("periodFrom_Day_ID")));
		fromcombo.selectByVisibleText(thirdpartysupplierpm.getProfitMarkupPeriodFrom().split("-")[0]);
		
		
		Select fromcombo1 = new Select(driver.findElement(By.id("periodFrom_Month_ID")));
		fromcombo1.selectByVisibleText(thirdpartysupplierpm.getProfitMarkupPeriodFrom().split("-")[1]);
		
		Select fromcombo2 = new Select(driver.findElement(By.id("periodFrom_Year_ID")));
		fromcombo2.selectByVisibleText(thirdpartysupplierpm.getProfitMarkupPeriodFrom().split("-")[2]);
		
		
		
		
		
		Select Tocombo= new Select(driver.findElement(By.id("periodTo_Day_ID")));
		Tocombo.selectByVisibleText(thirdpartysupplierpm.getProfitMarkupPeriodTo().split("-")[0]);
		
		
		Select Tocombo1= new Select(driver.findElement(By.id("periodTo_Month_ID")));
		Tocombo1.selectByVisibleText(thirdpartysupplierpm.getProfitMarkupPeriodTo().split("-")[1]);
		
		Select Tocombo2= new Select(driver.findElement(By.id("periodTo_Year_ID")));
		Tocombo2.selectByVisibleText(thirdpartysupplierpm.getProfitMarkupPeriodTo().split("-")[2]);
		
		
		
		driver.findElement(By.id("supplierName")).clear();
		driver.findElement(By.id("supplierName")).sendKeys(thirdpartysupplierpm.getThirdPartySupplier());
		driver.findElement(By.id("supplierName_lkup")).click();
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.id("lookup")));
	   
		
		driver.switchTo().frame("lookup");
		
		if (driver.findElements(By.className("rezgLook0")).get(0).getText().trim().equalsIgnoreCase(thirdpartysupplierpm.getThirdPartySupplier())) {

			
			driver.findElements(By.className("rezgLook0")).get(0).click();

		}

		else {
			driver.findElements(By.className("rezgLook1")).get(0).click();

		}

		driver.switchTo().defaultContent();
		
		
		
		
		driver.findElement(By.id("countryName")).clear();
		driver.findElement(By.id("countryName")).sendKeys(thirdpartysupplierpm.getCountry());
		driver.findElement(By.id("countryName_lkup")).click();
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.id("lookup")));
	   
		
		driver.switchTo().frame("lookup");
		
		if (driver.findElements(By.className("rezgLook0")).get(0).getText().trim().equalsIgnoreCase(thirdpartysupplierpm.getCountry())) {

			driver.findElements(By.className("rezgLook0")).get(0).click();

		}

		else {
			driver.findElements(By.className("rezgLook1")).get(0).click();

		}

		driver.switchTo().defaultContent();
		
		
		
		if(!(thirdpartysupplierpm.getCountry().equalsIgnoreCase("All"))){
		
		driver.findElement(By.id("cityName")).clear();
		driver.findElement(By.id("cityName")).sendKeys(thirdpartysupplierpm.getCity());
		driver.findElement(By.id("cityName_lkup")).click();
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.id("lookup")));
	   
		
		driver.switchTo().frame("lookup");
		
		if (driver.findElements(By.className("rezgLook0")).get(0).getText().trim().equalsIgnoreCase(thirdpartysupplierpm.getCity())) {

			driver.findElements(By.className("rezgLook0")).get(0).click();

		}

		else {
			driver.findElements(By.className("rezgLook1")).get(0).click();

		}

		driver.switchTo().defaultContent();
		
		}
		
		
		
		
		if(thirdpartysupplierpm.isOverwriteSpecificMarkup()){
			
			driver.findElement(By.id("overwriteMarkup_YES")).click();
			
		}
		else    {
			
			driver.findElement(By.id("overwriteMarkup_NO")).click();
			
		}
		
		
		
		if(thirdpartysupplierpm.getFixProfitMarkupDayplan().equalsIgnoreCase("Each day of the week")){
			
			driver.findElement(By.id("fixMarkupType_eachDay")).click();
			
			for (int i = 0; i < 6; i++) {
				
				driver.findElement(By.id("profitMarkupArray["+ i +"]")).clear();
				
				driver.findElement(By.id("profitMarkupArray["+ i +"]")).sendKeys(thirdpartysupplierpm.getProfitMarkup());
				
			}
				
		}
		
		else if(thirdpartysupplierpm.getFixProfitMarkupDayplan().equalsIgnoreCase("Sun to Thu / Fri to Sat")) {
			
			for (int i = 0; i < 4; i++) {
				
				driver.findElement(By.id("profitMarkupArray["+ i +"]")).clear();
				
				driver.findElement(By.id("profitMarkupArray["+ i +"]")).sendKeys(thirdpartysupplierpm.getProfitMarkup().split("/")[0]);
				
			}
			driver.findElement(By.id("profitMarkupArray[4]")).clear();
			driver.findElement(By.id("profitMarkupArray[4]")).sendKeys(thirdpartysupplierpm.getProfitMarkup().split("/")[1]);
			driver.findElement(By.id("profitMarkupArray[5]")).clear();
			driver.findElement(By.id("profitMarkupArray[5]")).sendKeys(thirdpartysupplierpm.getProfitMarkup().split("/")[1]);
			driver.findElement(By.id("profitMarkupArray[6]")).clear();
			driver.findElement(By.id("profitMarkupArray[6]")).sendKeys(thirdpartysupplierpm.getProfitMarkup().split("/")[1]);
		}
		
		
		((JavascriptExecutor)driver).executeScript("javascript: saveData(1);");
		
		
		
		
		try {
		    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
		    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
		   
		    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
		    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
				System.out.println(TextMessage);
			
				ExtentReportTemplate.getInstance().addtoReport("Third Party Supplier Profit Markup Hotel/Activities saving Function " + thirdpartysupplierpm.getThirdPartySupplier()+" "+thirdpartysupplierpm.getFixProfitMarkupDayplan()+" "+ thirdpartysupplierpm.getProfitMarkup(), "Third Party Supplier Profit Markup Hotel/Activities should be saved", TextMessage, true);
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				
				
		    }
		    else
		    {
		    	
		    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				System.out.println(TextMessage);
				
				ExtentReportTemplate.getInstance().addtoReport("Third Party Supplier Profit Markup Hotel/Activities saving Function " + thirdpartysupplierpm.getThirdPartySupplier()+" "+thirdpartysupplierpm.getFixProfitMarkupDayplan()+" "+ thirdpartysupplierpm.getProfitMarkup(), "Third Party Supplier Profit Markup Hotel/Activities should be saved", TextMessage, false);
				driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				
				
		    }
		    
		} catch (Exception e) {
			
			ExtentReportTemplate.getInstance().addtoReport("Third Party Supplier Profit Markup Hotel/Activities saving Function " + thirdpartysupplierpm.getThirdPartySupplier()+" "+thirdpartysupplierpm.getFixProfitMarkupDayplan()+" "+ thirdpartysupplierpm.getProfitMarkup(), "Third Party Supplier Profit Markup Hotel/Activities should be saved", e.toString(), false);
			
			System.out.println(e.toString());
			
			
		}
		
		
	

	}

}
