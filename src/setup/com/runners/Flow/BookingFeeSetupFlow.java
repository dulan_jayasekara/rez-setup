package setup.com.runners.Flow;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import setup.com.pojo.HotelBookingFee;
import setup.com.readers.PG_Properties;
import setup.com.utill.ExtentReportTemplate;

import org.apache.log4j.Logger;

public class BookingFeeSetupFlow {
	
	private static Logger logger = null;
	
	
	public WebDriver setBookingfeeSetup(WebDriver driver,HotelBookingFee bookingfee, String Base) throws InterruptedException {
		
		driver.get(PG_Properties.getProperty("Hotel.BaseUrl") + "/admin/setup/BookingFeeSetupPage.do?module=contract");
		
		System.out.println("Succussfully loaded Hotel Booking Fee");
		
		
		//Select Country
		
		driver.findElement(By.id("countryName")).clear();
		driver.findElement(By.id("countryName")).sendKeys(bookingfee.getCountryName());
		driver.findElement(By.id("countryName_lkup")).click();
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		driver.switchTo().defaultContent();
		
		//Select City
		
		driver.findElement(By.id("cityName")).clear();
		driver.findElement(By.id("cityName")).sendKeys(bookingfee.getCityName());
		driver.findElement(By.id("cityName_lkup")).click();
		driver.switchTo().frame("lookup");
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		driver.switchTo().defaultContent();
		
		//Tax & Fee Type
		
		if (bookingfee.getTax().equals("Percentage")) {

			driver.findElement(By.id("feeType_P")).click();
		} else {
			driver.findElement(By.id("feeType_V")).click();
		}
		
		//Amount
		
		driver.findElement(By.id("amount")).clear();
		driver.findElement(By.id("amount")).sendKeys(bookingfee.getAmount());
		
		driver.findElement(By.id("saveButId")).click();
		
		
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		

		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
			// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

			if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
				String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
				// System.out.println(TextMessage);
				logger.info(bookingfee.getCountryName()+"-"+bookingfee.getAmount() + " Booking fee saving Status - DONE  Message Out :----->" + TextMessage);
				ExtentReportTemplate.getInstance().addtoReport("Booking fee Assign function "+bookingfee.getCountryName()+bookingfee.getAmount(), " Booking fee saving Passed", TextMessage, true);
				
				driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
				
			} else {

				String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
				// System.out.println(TextMessage);
				logger.fatal(bookingfee.getCountryName()+"-"+bookingfee.getAmount() +  "Booking fee saving Status - Failed  Message Out :----->" + TextMessage);
				ExtentReportTemplate.getInstance().addtoReport("Booking fee Assign function "+bookingfee.getCountryName()+bookingfee.getAmount(), " Booking fee saving Passed", TextMessage, false);
				driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
				
			}

		} catch (Exception e) {
			logger.fatal(bookingfee.getCountryName()+"-"+bookingfee.getAmount() + " Basic Details Saving Status - Failed  Message Out :----->" + e.toString());
			ExtentReportTemplate.getInstance().addtoReport("Booking fee Assign function "+bookingfee.getCountryName()+bookingfee.getAmount(), " Booking fee saving Passed", e.toString(), false);
			
			// System.out.println(e.toString());

		}
		
		
		
		return driver;
	}
	}
	
	
	

