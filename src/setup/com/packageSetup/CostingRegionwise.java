package setup.com.packageSetup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import setup.com.dataObjects.ActivityDetails;
import setup.com.dataObjects.HotelDetails;
import setup.com.dataObjects.RateObjectSet;
import setup.com.dataObjects.RatesObject;
import setup.com.dataObjects.RegionRates;
import setup.com.dataObjects.assignAir;
import setup.com.dataObjects.costObj;
import setup.com.dataObjects.standardinfoObject;
import setup.com.dataObjects.PredefineddataObjects.selectedDateObject;

public class CostingRegionwise {

	public void generateRates(standardinfoObject obj) {

		int NumberofOrigins;
		Map<String, String> OriginMap=new HashMap<String, String>();

		if (obj.ProductIncludes.contains("F")) {

			NumberofOrigins=obj.getAirMap().size();


			for (int i = 0; i < NumberofOrigins; i++) {

				obj.getAirMap().get(i).getOriginAir();





			}




		}else {
			NumberofOrigins=0;
		}

	}

	public Map<String, Integer> hotelRateGenerateSelectedDates(List<HotelDetails>hotellist,String Region,Date selectedDate) {

		Map<String, Integer>hotelmap=new HashMap<String, Integer>();

		String GroupID="";

		int totalSingle=0;
		int totalDouble=0;
		int totalTriple=0;
		int totalChild=0;

		int maxSingleRateforGroup=0;
		int maxDoubleRateforGroup=0;
		int maxTripleRateforGroup=0;
		int maxChildRateforGroup=0;

		Map<String, Integer	> GroupMap=new HashMap<String, Integer>();
		Map<String,Map<String, Integer	>> maxGroupRateMap=new HashMap<String, Map<String,Integer>>();
		System.out.println("hotellist.size()"+hotellist.size());

		for (int i = 0; i < hotellist.size(); i++) {





			List<RatesObject> ratelist=hotellist.get(i).getRegionRates().get(Region).getRatelist();
			int ratelistsize=ratelist.size();


			if (hotellist.get(i).getDay().equalsIgnoreCase(GroupID)) {


				int maxSingleRateforHotel=0;
				int maxDoubleRateforHotel=0;
				int maxTripleRateforHotel=0;
				int maxChildRateforHotel=0;

				for (int j = 0; j < ratelistsize; j++) {

					Map<String, Integer	> generatedhotelMap=new HashMap<String, Integer>();

					if (CheckingValidityPeriod(ratelist.get(j), selectedDate)) {


						generatedhotelMap=hotelRate(ratelist.get(j), hotellist.get(i));

						maxSingleRateforHotel=maximumRateCalculator(maxSingleRateforHotel, generatedhotelMap.get("Single"));
						maxDoubleRateforHotel=maximumRateCalculator(maxDoubleRateforHotel, generatedhotelMap.get("Double"));
						maxTripleRateforHotel=maximumRateCalculator(maxTripleRateforHotel, generatedhotelMap.get("Triple"));
						maxChildRateforHotel=maximumRateCalculator(maxChildRateforHotel, generatedhotelMap.get("Child"));

					} 


				}

				Map<String, Integer	> maxHotelRateMap=new HashMap<String, Integer>();

				maxHotelRateMap.put("Single", maxSingleRateforHotel);
				maxHotelRateMap.put("Double", maxDoubleRateforHotel);
				maxHotelRateMap.put("Triple", maxTripleRateforHotel);
				maxHotelRateMap.put("Child", maxChildRateforHotel);



				maxSingleRateforGroup=maximumRateCalculator(maxSingleRateforGroup,maxSingleRateforHotel);
				maxDoubleRateforGroup=maximumRateCalculator(maxDoubleRateforGroup,maxDoubleRateforHotel);
				maxTripleRateforGroup=maximumRateCalculator(maxTripleRateforGroup,maxTripleRateforHotel);
				maxChildRateforGroup=maximumRateCalculator(maxChildRateforGroup,maxChildRateforHotel);


				GroupMap.put("Single", maxSingleRateforGroup);
				GroupMap.put("Double", maxDoubleRateforGroup);
				GroupMap.put("Triple", maxTripleRateforGroup);
				GroupMap.put("Triple", maxChildRateforGroup);



			}else {
				GroupID=hotellist.get(i).getDay();

				maxSingleRateforGroup=0;
				maxDoubleRateforGroup=0;
				maxTripleRateforGroup=0;
				maxChildRateforGroup=0;

				int maxSingleRateforHotel=0;
				int maxDoubleRateforHotel=0;
				int maxTripleRateforHotel=0;
				int maxChildRateforHotel=0;

				for (int j = 0; j < ratelistsize; j++) {

					Map<String, Integer	> generatedhotelMap=new HashMap<String, Integer>();

					if (CheckingValidityPeriod(ratelist.get(j), selectedDate)) {
						generatedhotelMap=hotelRate(ratelist.get(j), hotellist.get(i));

						maxSingleRateforHotel=maximumRateCalculator(maxSingleRateforHotel, generatedhotelMap.get("Single"));
						maxDoubleRateforHotel=maximumRateCalculator(maxDoubleRateforHotel, generatedhotelMap.get("Double"));
						maxTripleRateforHotel=maximumRateCalculator(maxTripleRateforHotel, generatedhotelMap.get("Triple"));
						maxChildRateforHotel=maximumRateCalculator(maxChildRateforHotel, generatedhotelMap.get("Child"));

					} 


				}

				Map<String, Integer	> maxHotelRateMap=new HashMap<String, Integer>();

				maxHotelRateMap.put("Single", maxSingleRateforHotel);
				maxHotelRateMap.put("Double", maxDoubleRateforHotel);
				maxHotelRateMap.put("Triple", maxTripleRateforHotel);
				maxHotelRateMap.put("Child", maxChildRateforHotel);



				maxSingleRateforGroup=maximumRateCalculator(maxSingleRateforGroup,maxSingleRateforHotel);
				maxDoubleRateforGroup=maximumRateCalculator(maxDoubleRateforGroup,maxDoubleRateforHotel);
				maxTripleRateforGroup=maximumRateCalculator(maxTripleRateforGroup,maxTripleRateforHotel);
				maxChildRateforGroup=maximumRateCalculator(maxChildRateforGroup,maxChildRateforHotel);


				GroupMap.put("Single", maxSingleRateforGroup);
				GroupMap.put("Double", maxDoubleRateforGroup);
				GroupMap.put("Triple", maxTripleRateforGroup);
				GroupMap.put("Child", maxChildRateforGroup);


			}

			maxGroupRateMap.put(GroupID, GroupMap);



		}


		for (Entry<String, Map<String, Integer>> entryval:maxGroupRateMap.entrySet()) {

			totalSingle=totalSingle+entryval.getValue().get("Single");
			totalDouble=totalDouble+entryval.getValue().get("Double");
			totalTriple=totalTriple+entryval.getValue().get("Triple");
			totalChild=totalChild+entryval.getValue().get("Child");


		}

		hotelmap.put("Single", totalSingle);
		hotelmap.put("Double", totalDouble);
		hotelmap.put("Triple", totalTriple);
		hotelmap.put("Child", totalChild);

		return hotelmap;

	}


	public void regionRatesHotelGenerate(/*assignAir air,*/ standardinfoObject obj) throws ParseException {

		int numberOfRegions=obj.getRegionArray().size();
		System.out.println("numberOfRegions"+numberOfRegions);


		Map<String, Map<String, Map<String, Integer>>> finalMapRegionwise=new HashMap<String, Map<String, Map<String, Integer>>>();
		//*********************************************Hotel Net Rate***************************************************

		for (int i = 0; i < numberOfRegions; i++) {


			String regionname=obj.getRegionArray().get(i);


			Map<String, Integer> regionmap=new HashMap<String, Integer>();
			if (obj.DepartureBasedOn.contains("By Period")) {

				int NumberofSelectedPeriods=obj.SelectedDates.split("R").length;

				//String DateFromCurrent = null;

				for (int j = 0; j < NumberofSelectedPeriods; j++) {

					Date DateFromCurrent;
					Date DateToCurrent;

					if (j==0) {

						DateFromCurrent=new SimpleDateFormat("dd/MMM/yyyy").parse(obj.StayForm);
					}else {
						DateFromCurrent=DateCalculator(obj.SelectedDates.split("R")[j-1], 1);
					}


					DateToCurrent=new SimpleDateFormat("yyyy-MM-dd").parse(obj.SelectedDates.split("R")[j]);

					System.out.println("*******************************************************");
					System.out.println("Package From"+new SimpleDateFormat("yyyy-MMM-dd").format(DateFromCurrent));
					System.out.println("Package To"+new SimpleDateFormat("yyyy-MMM-dd").format(DateToCurrent));



					int HotelListSize=obj.getHotellist().size();

					String CurrentGroup="";
					int MaximumRateForTheGroup=0;
					for (int k = 0; k < HotelListSize; k++) {

						HotelDetails currentDetail=new HotelDetails();
						currentDetail=obj.getHotellist().get(k);



						if (CurrentGroup.contentEquals(obj.getHotellist().get(k).getDay())) {


						}else {
							CurrentGroup=obj.getHotellist().get(k).getDay();

							int ratecontractCount=currentDetail.getRegionRates().get(regionname).getRatelist().size();
							RatesObject currentGroupMaxContract=new RatesObject();
							Map<String, Integer> currentGroupMaxContractRateMap=new HashMap<String, Integer>();
							Map<String, Integer> MaxmultiContractRateMap=new HashMap<String, Integer>();

							RatesObject rate1=new RatesObject();
							RatesObject rate2=new RatesObject();
							RatesObject rate3=new RatesObject();


							Map<String, Integer> rate1newmap=new HashMap<String, Integer>();
							Map<String, Integer> rate2newmap=new HashMap<String, Integer>();

							for (int l = 0; l < ratecontractCount; l++) {

								RatesObject rate=new RatesObject();
								rate=currentDetail.getRegionRates().get(regionname).getRatelist().get(l);



								if (rate.getContractFromDate().before(DateFromCurrent) || rate.getContractFromDate().equals(DateFromCurrent)) {


									if (rate.getContractToDate().after(DateToCurrent) || rate.getContractToDate().equals(DateToCurrent)) {



										currentGroupMaxContract=rate;
										currentGroupMaxContractRateMap=hotelRate(rate, currentDetail);
										System.out.println("+++++++++++++++++++++       in Contract              +++++++++++++++++++++++++++");



									}else {

										if (rate.getContractToDate().after(DateFromCurrent)) {

											rate1newmap=hotelRate(rate, currentDetail);




											rate1=rate;
											System.out.println("Rate 1 From"+new SimpleDateFormat("yyyy-MMM-dd").format(rate1.getContractFromDate()));
											System.out.println("Rate 1 TO"+new SimpleDateFormat("yyyy-MMM-dd").format(rate1.getContractToDate()));


											System.out.println("Single Rate 1++++++++++++++++++++++++"+rate1newmap.get("Single"));

										}


									}
								}else {




									if (rate.getContractToDate().after(DateToCurrent) || rate.getContractToDate().equals(DateToCurrent)) {

										if (rate.getContractFromDate().before(DateToCurrent)) {




											rate2=rate;
											rate2newmap=hotelRate(rate, currentDetail);

											System.out.println("Rate 2 From"+new SimpleDateFormat("yyyy-MMM-dd").format(rate2.getContractFromDate()));
											System.out.println("Rate 2 TO"+new SimpleDateFormat("yyyy-MMM-dd").format(rate2.getContractToDate()));

											System.out.println("Single Rate 2++++++++++++++++++++++++"+rate2newmap.get("Single"));

										}


									}else {
										rate3=rate;


									}

								}






							}

							try {

								System.out.println(maximumRateCalculator(rate1newmap.get("Single"), rate2newmap.get("Single")));



								MaxmultiContractRateMap.put("Single", maximumRateCalculator(rate1newmap.get("Single"), rate2newmap.get("Single")));
								MaxmultiContractRateMap.put("Double", maximumRateCalculator(rate1newmap.get("Double"), rate2newmap.get("Double")));
								MaxmultiContractRateMap.put("Triple", maximumRateCalculator(rate1newmap.get("Triple"), rate2newmap.get("Triple")));
								MaxmultiContractRateMap.put("Child", maximumRateCalculator(rate1newmap.get("Child"), rate2newmap.get("Child")));


								currentGroupMaxContractRateMap=MaxmultiContractRateMap;


							} catch (Exception e) {
								// TODO: handle exception
							}








						}


					}

				}




			}else {
				int NumberofSelectedDates;

				NumberofSelectedDates=obj.SelectedDates.split("R").length;

				for (int j = 0; j < NumberofSelectedDates; j++) {







				}
			}























		}




	}



	public int taxAmountCalculator(int value,double taxAmount) {
		int taxvalue = 0;

		if (taxAmount<1) {
			taxvalue=(int) (value*taxAmount);



		}else {
			taxvalue=(int) taxAmount;

		}

		return taxvalue;

	}

	public int maximumHotelTaxCalculator(List<HotelDetails> hotellist, int HotelRate) {

		int totalMaximumTaxRate=0;
		int maxSalesTax=0;
		int maxOccupancyTax=0;
		int maxEnergyTax=0;

		for (int i = 0; i < hotellist.size(); i++) {

			//*** Sales

			int salesTax=taxAmountCalculator(HotelRate, Double.parseDouble(hotellist.get(i).getSalesTax()));
			int occupancyTax=taxAmountCalculator(HotelRate, Double.parseDouble(hotellist.get(i).getOccupancyTax()));
			int energyTax=taxAmountCalculator(HotelRate, Double.parseDouble(hotellist.get(i).getEnergyTax()));


			maxSalesTax=maximumRateCalculator(maxSalesTax, salesTax);
			maxOccupancyTax=maximumRateCalculator(maxOccupancyTax, occupancyTax);
			maxEnergyTax=maximumRateCalculator(maxEnergyTax, energyTax);



		}

		totalMaximumTaxRate=maxSalesTax+maxOccupancyTax+maxEnergyTax;

		return totalMaximumTaxRate;

	}

	public int ActivtyTotal(List<ActivityDetails> activitylist,String region,Date SelectedDate) throws ParseException {


		int totalActivityRate=0;


		for (int i = 0; i < activitylist.size(); i++) {

			totalActivityRate=totalActivityRate+ActivityRateCalculatorRegionSelectedDates(activitylist.get(i), region, SelectedDate);






		}




		return totalActivityRate;

	}

	public int maxAcivityTax(int programcost,List<ActivityDetails> activitylist) {
		int maxreturntax=0;
		int numberofActivities=activitylist.size();

		for (int i = 0; i < numberofActivities; i++) {

			int tax;
			tax=taxAmountCalculator(programcost, Double.parseDouble(activitylist.get(i).getSalesTax()));

			maxreturntax=maximumRateCalculator(maxreturntax, tax);

		}

		return maxreturntax;

	}



	public int ActivityRateCalculatorRegionSelectedDates(ActivityDetails activity,String region,Date SelectedDate) {

		int MaximumActivityRateForDate=0;


		int RateContractCount=activity.getRegionRatemap().get(region).getRatelist().size();
		System.out.println("RateContractCount***********"+RateContractCount);

		for (int j = 0; j < RateContractCount; j++) {

			if (CheckingValidityPeriod(activity.getRegionRatemap().get(region).getRatelist().get(j), SelectedDate)) {

				MaximumActivityRateForDate=(int) activity.getRegionRatemap().get(region).getRatelist().get(j).getStdRate();
				System.out.println("MaximumActivityRateForDate"+MaximumActivityRateForDate);
			}

		}






		return MaximumActivityRateForDate;





	}
	public int maximumRateCalculator(int existingmax, int newvalue) {

		int returnmax;
		if (existingmax>=newvalue) {
			returnmax=existingmax;
		}else {
			returnmax=newvalue;
		}
		return returnmax;	
	}
	public Map<String, Integer> hotelRate(RatesObject rate,HotelDetails hotel) {

		int ratesingle=0;
		int ratedouble=0;
		int ratetriple=0;
		int ratechild=0;


		int numofDays=hotel.getNoOfDays();



		if (hotel.getRateType().equalsIgnoreCase("Per Room")) {

			//***********single highest rate calculation
			ratesingle=(int) ((rate.getStdRate())*numofDays);
			//**********Double rate calculation
			if (hotel.getStdAdultCount().equalsIgnoreCase("1")) {

				ratedouble=(int) ((((rate.getStdRate()))+(rate.getAddRate())/2)*numofDays);

			}else {
				ratedouble=(int) ((((rate.getStdRate()))/2)*numofDays);
			}

			//*********Triple Rate Calculation

			if (hotel.getStdAdultCount().equalsIgnoreCase("1")) {

				ratetriple=(int)( (((rate.getStdRate())+(rate.getAddRate())*2)/3)*numofDays);

			}else if (hotel.getStdAdultCount().equalsIgnoreCase("2")) {
				ratetriple=(int) ((((rate.getStdRate())+(rate.getAddRate()))/3)*numofDays);

			} else {
				ratetriple=(int) ((((rate.getStdRate()))/3)*numofDays);
			}

			//*************childrate
			ratechild=(int) ((((rate.getChildRate())))*numofDays);


		}else {
			//per person rates

			//***********single highest rate calculation
			ratesingle=(int) ((((rate.getStdRate())*Double.parseDouble(hotel.getStdAdultCount())))*numofDays);
			//**********Double rate calculation
			if (hotel.getStdAdultCount().equalsIgnoreCase("1")) {

				ratedouble=(int) ((((rate.getStdRate())+(rate.getAddRate()))/2)*numofDays);

			}else {
				ratedouble=(int) ((((rate.getStdRate())*Double.parseDouble(hotel.getStdAdultCount()))/2)*numofDays);
			}

			//*********Triple Rate Calculation

			if (hotel.getStdAdultCount().equalsIgnoreCase("1")) {

				ratetriple=(int) ((((rate.getStdRate()))+(int) (((rate.getAddRate())*2)/3))*numofDays);

			}else if (hotel.getStdAdultCount().equalsIgnoreCase("2")) {
				ratetriple=(int) ((((rate.getStdRate())*2+((rate.getAddRate())))/3)*numofDays);

			} else {
				ratetriple=(int) ((((rate.getStdRate())*(Double.parseDouble(hotel.getStdAdultCount())))/3)*numofDays);
			}

			//*************childrate
			ratechild=(int) ((((rate.getChildRate())))*numofDays);


		}

		Map<String, Integer> returnmap=new HashMap<String, Integer>();

		returnmap.put("Single", ratesingle);
		returnmap.put("Double", ratedouble);
		returnmap.put("Triple", ratetriple);
		returnmap.put("Child", ratechild);

		return returnmap;






	}

	public Date DateCalculator(String date,int increment) throws ParseException {

		Calendar cal=Calendar.getInstance();
		cal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
		cal.add(Calendar.DATE, increment);
		Date returndate=cal.getTime();
		String fromDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		return returndate; 
	}

	public boolean CheckingValidityPeriod(RatesObject rate, Date datetobechecked) {

		boolean status=false;
		if (datetobechecked.after(rate.getContractFromDate()) && datetobechecked.before(rate.getContractToDate())) {
			status=true;

		}
		return status;

	}



	public static void main(String[] args) throws Exception {

		CostingRegionwise costing=new CostingRegionwise();
		standardinfoObject obj=new standardinfoObject();
		obj=obj.readinfo("RezProduction.xls").get(2);



		costing.TotalCostingObjectConstruct(obj);
		//costing.regionRatesHotelGenerate(obj.getAirMap().get("LHR"), obj);

		//	costing.TotalCostingObjectConstruct(obj);



	}



	public Map<String, Map<String, Map<String, Map<String, costObj>>>> TotalCostingObjectConstruct(standardinfoObject obj) throws ParseException {

		Map<String, Map<String, Map<String, Map<String, costObj>>>> FinalMap=new HashMap<String, Map<String,Map<String,Map<String,costObj>>>>();

		// origin-Region-Date-PassengerType-costobject

		int numberOfOrigins;
		int numberOfRegions=obj.getRegionArray().size();

		int airvalue = 0;
		int airtax=0;
		int hotelValue = 0;
		int hotelTax=0;
		int activityvalue = 0;
		int activitytax = 0;

		int TotalCost;
		int totalTax;
		int DCPM;
		int proposedSellRate;
		int Sellrate;
		int agentcommissionableAmount;
		int affcommissionableAmount;

		int agentCommission;
		int agentProfit;

		int affCommission = 0;
		int affProfit = 0;

		Date selectedDate = null;
		Date periodFromDate=null;
		Date periodToDate=null;



		if (obj.ProductIncludes.contains("F")) {
			numberOfOrigins=obj.getAirMap().size();
		}else {
			numberOfOrigins=1;
		}

		Map<String, assignAir> originmap=new HashMap<String, assignAir>();

		if (obj.ProductIncludes.contains("F")) {
			originmap=obj.getAirMap();
		}else{
			assignAir landorigin=new assignAir();
			landorigin.setOriginAir("Land Only");
			originmap.put("Land Only", landorigin);

		}



		for (Entry<String, assignAir> airelement:originmap.entrySet()) {
			System.out.println("**************************");
			System.out.println("Origin"+airelement.getKey());
			Map<String, Map<String, Map<String, costObj>>> regionmap=new HashMap<String, Map<String,Map<String,costObj>>>();

			for (int j = 0; j < numberOfRegions; j++) {
				System.out.println("*********************");
				System.out.println("Region"+obj.getRegionArray().get(j));

				Map<String, Map<String, costObj>> datemap=new HashMap<String, Map<String,costObj>>();
				for (int i = 0; i < obj.SelectedDates.split("R").length; i++) {



					if (obj.DepartureBasedOn.contains("By Dates")) {

						selectedDate=DateCalculator(obj.SelectedDates.split("R")[i], 0);

					}else {

						if (i==0) {

							periodFromDate=new SimpleDateFormat("dd/MMM/yyyy").parse(obj.StayForm);
						}else {
							periodFromDate=DateCalculator(obj.SelectedDates.split("R")[i-1], 1);
						}

						periodToDate=DateCalculator(obj.SelectedDates.split("R")[i], 0);



					}

					System.out.println("********************");
					System.out.println("Date"+obj.SelectedDates.split("R")[i]);

					String[] PassengerType={"Single","Double","Triple","Child"};

					Map<String, costObj> costmapPassengertype=new HashMap<String, costObj>();

					for (int k = 0; k < PassengerType.length; k++) {

						System.out.println("***************************");
						System.out.println("Passenger type"+PassengerType[k]);

						//*****generate Cost object

						// 1. air Rates Take



						if (obj.ProductIncludes.contains("F")) {
							if (PassengerType[k].contains("Child")) {
								airvalue=Integer.parseInt(airelement.getValue().getChildFare());
								airtax=Integer.parseInt(airelement.getValue().getChildTax());
							}else {
								airvalue=Integer.parseInt(airelement.getValue().getAdultFare());
								airtax=Integer.parseInt(airelement.getValue().getAdultTax());
							}
						}



						System.out.println("airvalue"+airvalue);	


						//Region Hotel Rate

						if (obj.ProductIncludes.contains("H")) {

							if (obj.DepartureBasedOn.contains("By Dates")) {

								hotelValue=hotelRateGenerateSelectedDates(obj.getHotellist(), obj.getRegionArray().get(j), selectedDate).get(PassengerType[k]);		
								hotelTax=maximumHotelTaxCalculator(obj.getHotellist(), hotelValue);


							}else {
								Map<String, Map<String, Integer>> hotelratemap=new HashMap<String, Map<String,Integer>>();
								System.out.println("periodFromDate"+periodFromDate);
								System.out.println("periodToDate"+periodToDate);
								hotelratemap=SelectedPeriodHotelRegion(obj, periodFromDate, periodToDate);
								System.out.println("PassengerType[k]"+PassengerType[k]);
								System.out.println("obj.getRegionArray().get(j):"+obj.getRegionArray().get(j));
								System.out.println(hotelratemap.get(obj.getRegionArray().get(j)).size());
								hotelValue=hotelratemap.get(obj.getRegionArray().get(j)).get(PassengerType[k]);
								hotelTax=maximumHotelTaxCalculator(obj.getHotellist(), hotelValue);
							}


						}


						System.out.println("hotelValue"+hotelValue);

						if (obj.ProductIncludes.contains("A")) {
							//3-Activity Total Rate 
							activityvalue=ActivtyTotal(obj.getActivitylist(), obj.getRegionArray().get(j), selectedDate);
							activitytax=maxAcivityTax(activityvalue, obj.getActivitylist());


						}



						TotalCost=airvalue+hotelValue+activityvalue;
						totalTax=airtax+hotelTax+activitytax;
						double PM=0;
						if (obj.DCPMType.contains("Percentage")) {
							PM=Double.parseDouble(obj.DCPM)/100;

						}else {
							PM=Double.parseDouble(obj.DCPM);
						}
						DCPM=taxAmountCalculator(TotalCost, PM);
						proposedSellRate=TotalCost+totalTax+DCPM;
						Sellrate=((proposedSellRate+99)/100)*100;

						agentcommissionableAmount=Sellrate-totalTax;
						affcommissionableAmount=Sellrate-totalTax;
						double tocom;
						double affcom;
						if (obj.TOPMType.contains("Percentage")) {
							tocom=Double.parseDouble(obj.TOPM)/100;

						}else {
							tocom=Double.parseDouble(obj.TOPM);
						}
						if (obj.AFFPMType.contains("Percentage")) {
							affcom=Double.parseDouble(obj.AFFPM)/100;
						}else {
							affcom=Double.parseDouble(obj.AFFPM);
						}
						agentCommission=taxAmountCalculator(agentcommissionableAmount, tocom);
						agentProfit=(Sellrate-TotalCost-totalTax-agentCommission);
						affCommission=taxAmountCalculator(affcommissionableAmount, affcom);
						affProfit=(Sellrate-TotalCost-totalTax-affCommission);



						costObj costobject=new costObj();
						costobject.setAir(String.valueOf(airvalue));
						costobject.setHotel(String.valueOf(hotelValue));
						costobject.setProgram(String.valueOf(activityvalue));
						costobject.setTotalCost(String.valueOf(TotalCost));
						costobject.setAdjustableTotalCost(String.valueOf(TotalCost));
						costobject.setTotalTaxValue(String.valueOf(totalTax));
						costobject.setProfitMarkupAmount(String.valueOf(DCPM));
						costobject.setProposedSellRate(String.valueOf(proposedSellRate));
						costobject.setFinalSellRate(String.valueOf(Sellrate));
						costobject.setAgentCommissionableAmount(String.valueOf(agentcommissionableAmount));
						costobject.setAgentCommissionAmount(String.valueOf(agentCommission));
						costobject.setAgentNetProfit(String.valueOf(agentProfit));
						costobject.setAffiliateCommissionableAmount(String.valueOf(affcommissionableAmount));
						costobject.setAffiliateCommissionAmount(String.valueOf(affCommission));
						costobject.setAffiliateNetProfit(String.valueOf(affProfit));

						costmapPassengertype.put(PassengerType[k], costobject);



					}

					datemap.put(obj.SelectedDates.split("R")[i], costmapPassengertype);
				}


				regionmap.put(obj.getRegionArray().get(j), datemap);

			}

			FinalMap.put(airelement.getKey(), regionmap);

		}
		return FinalMap;





	}

	public Map<String, Map<String, Integer>> SelectedPeriodHotelRegion(standardinfoObject obj,Date fromDate,Date toDate ) {

		String HotelGroupingID="";
		int maxSingleForGroup=0;
		int maxDoubleForGroup=0;
		int maxTripleForGroup=0;
		int maxChildForGroup=0;

		Map<String, Map<String, Integer>> GroupMax=new HashMap<String, Map<String, Integer>>();
		Map<String, Map<String, Integer>> returnMap=new HashMap<String, Map<String, Integer>>();


		for (int i = 0; i < obj.getHotellist().size(); i++) {

			RatesObject ratesForPeriod=new RatesObject();

			try {
				for (int j = 0; j < obj.getRegionArray().size(); j++) {

					List<RatesObject> ratelist=obj.getHotellist().get(i).getRegionRates().get(obj.getRegionArray().get(j)).getRatelist();

					ratesForPeriod=selectedPeriodHotelRate(ratelist, fromDate, toDate);
					Map<String, Integer> passengermap=new HashMap<String, Integer>();
					passengermap=hotelRate(ratesForPeriod, obj.getHotellist().get(i));

					if (obj.getHotellist().get(i).getDay().equals(HotelGroupingID)) {

						maxSingleForGroup=maximumRateCalculator(maxSingleForGroup, passengermap.get("Single"));
						maxDoubleForGroup=maximumRateCalculator(maxDoubleForGroup, passengermap.get("Double"));
						maxTripleForGroup=maximumRateCalculator(maxTripleForGroup, passengermap.get("Triple"));
						maxChildForGroup=maximumRateCalculator(maxChildForGroup, passengermap.get("Child"));



					}else {
						
						maxSingleForGroup=0;
						maxDoubleForGroup=0;
						maxTripleForGroup=0;
						maxChildForGroup=0;


						maxSingleForGroup=passengermap.get("Single");
						maxDoubleForGroup=passengermap.get("Double");
						maxTripleForGroup=passengermap.get("Triple");
						maxChildForGroup=passengermap.get("Child");





					}

					Map<String, Integer> maximumRates=new HashMap<String, Integer>();
					maximumRates.put("Single", maxSingleForGroup);
					maximumRates.put("Double", maxDoubleForGroup);
					maximumRates.put("Triple", maxTripleForGroup);
					maximumRates.put("Child", maxChildForGroup);

					GroupMax.put(obj.getHotellist().get(i).getDay(), maximumRates);

					int totalSingleval=0;
					int totalDoubleval=0;
					int totalTripleval=0;
					int totalChildval=0;


					for (Entry<String, Map<String, Integer>> val:GroupMax.entrySet()) {

						totalSingleval=totalSingleval+val.getValue().get("Single");
						totalDoubleval=totalDoubleval+val.getValue().get("Double");
						totalTripleval=totalTripleval+val.getValue().get("Triple");
						totalChildval=totalChildval+val.getValue().get("Child");

					}


					Map<String, Integer> regionmap=new HashMap<String, Integer>();

					regionmap.put("Single", totalSingleval);
					regionmap.put("Double", totalDoubleval);
					regionmap.put("Triple", totalTripleval);
					regionmap.put("Child", totalChildval);




					returnMap.put(obj.getRegionArray().get(j), regionmap);


				}
			} catch (Exception e) {
				e.printStackTrace();
			}










		}


		return returnMap;

	}




	public RatesObject	selectedPeriodHotelRate(List<RatesObject> ratelist,Date fromDate,Date toDate) {

		int numberofRateContracts=ratelist.size();

		RatesObject returnRate=new RatesObject();

		double maximumstdRate=0;
		double maximumaddRate=0;
		double maximumchildRate=0;

		for (int i = 0; i < numberofRateContracts; i++) {

			Date ratecontractFromDate=ratelist.get(i).getContractFromDate();
			Date ratecontractToDate=ratelist.get(i).getContractToDate();

			if (fromDate.after(ratecontractFromDate) && toDate.before(ratecontractToDate)) {

				returnRate.setStdRate(String.valueOf(ratelist.get(i).getStdRate()));
				returnRate.setStdRate(String.valueOf(ratelist.get(i).getStdRate()));
				returnRate.setStdRate(String.valueOf(ratelist.get(i).getStdRate()));


			}else {
				if (fromDate.after(ratecontractFromDate) && fromDate.before(ratecontractToDate) && toDate.after(ratecontractToDate)) {
					maximumstdRate=maximumRateCalculator((int)maximumstdRate, (int)ratelist.get(i).getStdRate());
					maximumaddRate=maximumRateCalculator((int)maximumaddRate, (int)ratelist.get(i).getAddRate());
					maximumchildRate=maximumRateCalculator((int)maximumchildRate, (int)ratelist.get(i).getChildRate());

				} else if (fromDate.before(ratecontractFromDate) && fromDate.before(ratecontractToDate) && toDate.before(ratecontractToDate)) {
					maximumstdRate=maximumRateCalculator((int)maximumstdRate, (int)ratelist.get(i).getStdRate());
					maximumaddRate=maximumRateCalculator((int)maximumaddRate, (int)ratelist.get(i).getAddRate());
					maximumchildRate=maximumRateCalculator((int)maximumchildRate, (int)ratelist.get(i).getChildRate());

				}else if (fromDate.before(ratecontractFromDate) && toDate.before(ratecontractToDate)) {
					maximumstdRate=maximumRateCalculator((int)maximumstdRate, (int)ratelist.get(i).getStdRate());
					maximumaddRate=maximumRateCalculator((int)maximumaddRate, (int)ratelist.get(i).getAddRate());
					maximumchildRate=maximumRateCalculator((int)maximumchildRate, (int)ratelist.get(i).getChildRate());


				}


				returnRate.setStdRate(String.valueOf(maximumstdRate));
				returnRate.setAddRate(String.valueOf(maximumaddRate));
				returnRate.setChildRate(String.valueOf(maximumchildRate));

			}





		}
		return returnRate;


	}



}
