package setup.com.packageSetup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import setup.com.dataObjects.ItineraryObject;
import setup.com.dataObjects.standardinfoObject;
import setup.com.readers.ReadExcel;
import setup.com.utill.ExtentReportTemplate;

public class ItineraryDetailLoader {

	public Map<Integer, ItineraryObject> ItineraryReader(String excelPath) {

		Map<Integer, ItineraryObject> objMap=new TreeMap<Integer, ItineraryObject>();

		ReadExcel reader=new ReadExcel();
		ArrayList<Map<Integer, String>> list1=reader.init(excelPath);
		Map<Integer, String> loginDetails=list1.get(5);

		int a=0;

		Iterator<Map.Entry<Integer, String>> it = loginDetails.entrySet().iterator();

		while(it.hasNext())
		{
			String[]   allvalues     = it.next().getValue().split(",");

			ItineraryObject object=new ItineraryObject();

			object.setDay(allvalues[0]);
			object.setImage(allvalues[1]);
			object.setDescription(allvalues[2]);
			
			objMap.put(Integer.parseInt(allvalues[0]), object);

		}




		return objMap;


	}
	
	
	public void ItineraryDetailsLoader(standardinfoObject object,WebDriver driver) throws InterruptedException {
		
		driver.get(object.url+"/packaging/setup/PackageAssignProductsSetupPage.do");
		
		System.out.println(driver.findElements(By.name("selectedDays")).size());
		
		for (int i = 0; i < driver.findElements(By.name("selectedDays")).size() ; i++) {
			
			ItineraryObject iobject=new ItineraryObject();
			iobject=object.getItinerary().get(i+1);
			
			driver.findElements(By.name("selectedDays")).get(i).click();
			
			Thread.sleep(3000);
			
			driver.findElement(By.id("imageFile")).sendKeys(iobject.getImage());
			driver.findElement(By.id("iternaryDiscription")).sendKeys(iobject.getDescription());
			
			((JavascriptExecutor)driver).executeScript("beforeSave();");
			
			
			WebDriverWait wait = new WebDriverWait(driver, 20);
			
			 
			standardinfoObject obj=new standardinfoObject();
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
				// System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());

				if (driver.findElement(By.id("dialogMsgText")).isDisplayed()) {
					String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
					
					
					ExtentReportTemplate.getInstance().addtoReport("Fix Package ItineraryDetails Saving Function "+obj.packageName , " Fix Package ItineraryDetails should be saved", TextMessage, true);
					
					driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
					
				} else {

					String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
					// System.out.println(TextMessage);
				
					ExtentReportTemplate.getInstance().addtoReport("Fix Package ItineraryDetails Saving Function "+obj.packageName , " Fix Package ItineraryDetails should be saved", TextMessage, false);
					
					driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
					
				}

			} catch (Exception e) {
				
				ExtentReportTemplate.getInstance().addtoReport("Fix Package ItineraryDetails Saving Function "+obj.packageName , " Fix Package ItineraryDetails should be saved", e.toString(), false);
				
				// System.out.println(e.toString());

			}
			
			
			
		}
		
	
		
		
		
		
		
	}
	
	public static void main(String[] args) {
		
		ItineraryDetailLoader loader=new ItineraryDetailLoader();
		Map<Integer, ItineraryObject> objMap=new TreeMap<Integer, ItineraryObject>();
		objMap=loader.ItineraryReader("PackageSetup_RezDemo.xls");
		
		System.out.println("size--"+objMap.size());
		
		for (int i = 0; i < objMap.size(); i++) {
			
			System.out.println(objMap.get(i+1).getDay());
			System.out.println(objMap.get(i+1).getImage());
			System.out.println(objMap.get(i+1).getDescription());
		}
		
	}

}
