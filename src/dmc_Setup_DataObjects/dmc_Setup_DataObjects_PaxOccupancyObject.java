package dmc_Setup_DataObjects;

public class dmc_Setup_DataObjects_PaxOccupancyObject {
	
	
	String roomType;
	String noOfAdults;
	String noOfChildren;
	String combinationActive;
	
	
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getNoOfAdults() {
		return noOfAdults;
	}
	public void setNoOfAdults(String noOfAdults) {
		this.noOfAdults =noOfAdults;
	}
	public String getNoOfChildren() {
		return noOfChildren;
	}
	public void setNoOfChildren(String noOfChildren) {
		this.noOfChildren = noOfChildren;
	}
	public String getCombinationActive() {
		return combinationActive;
	}
	public void setCombinationActive(String combinationActive) {
		this.combinationActive = combinationActive;
	}
	
	

}
