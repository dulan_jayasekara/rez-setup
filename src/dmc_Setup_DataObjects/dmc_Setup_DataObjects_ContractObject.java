package dmc_Setup_DataObjects;

import java.util.ArrayList;
import java.util.List;

public class dmc_Setup_DataObjects_ContractObject {
	
	
	String inventoryType;
	String searchSatisfiedweb;
	String searchSatisfiedCC;
	String inventoryExhaustedweb;
	String inventoryExhaustedCC;
	String cutoffAppliedweb;
	String cutoffAppliedCC;
	String blackoutweb;
	String blackoutCC;
	
	
	List<dmc_Setup_DataObjects_PaxOccupancyObject> paxOccupancylist=new ArrayList<dmc_Setup_DataObjects_PaxOccupancyObject>();
	List<dmc_Setup_DataObjects_PaxGroupObject> paxGroupList=new ArrayList<dmc_Setup_DataObjects_PaxGroupObject>();
	
	
	
	public List<dmc_Setup_DataObjects_PaxOccupancyObject> getPaxOccupancylist() {
		return paxOccupancylist;
	}
	public void setPaxOccupancylist(List<dmc_Setup_DataObjects_PaxOccupancyObject> paxOccupancylist) {
		this.paxOccupancylist = paxOccupancylist;
	}
	public List<dmc_Setup_DataObjects_PaxGroupObject> getPaxGroupList() {
		return paxGroupList;
	}
	public void setPaxGroupList(List<dmc_Setup_DataObjects_PaxGroupObject> paxGroupList) {
		this.paxGroupList = paxGroupList;
	}
	public String getInventoryType() {
		return inventoryType;
	}
	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
	}
	public String getSearchSatisfiedweb() {
		return searchSatisfiedweb;
	}
	public void setSearchSatisfiedweb(String searchSatisfiedweb) {
		this.searchSatisfiedweb = searchSatisfiedweb;
	}
	public String getSearchSatisfiedCC() {
		return searchSatisfiedCC;
	}
	public void setSearchSatisfiedCC(String searchSatisfiedCC) {
		this.searchSatisfiedCC = searchSatisfiedCC;
	}
	public String getInventoryExhaustedweb() {
		return inventoryExhaustedweb;
	}
	public void setInventoryExhaustedweb(String inventoryExhaustedweb) {
		this.inventoryExhaustedweb = inventoryExhaustedweb;
	}
	public String getInventoryExhaustedCC() {
		return inventoryExhaustedCC;
	}
	public void setInventoryExhaustedCC(String inventoryExhaustedCC) {
		this.inventoryExhaustedCC = inventoryExhaustedCC;
	}
	public String getCutoffAppliedweb() {
		return cutoffAppliedweb;
	}
	public void setCutoffAppliedweb(String cutoffAppliedweb) {
		this.cutoffAppliedweb = cutoffAppliedweb;
	}
	public String getCutoffAppliedCC() {
		return cutoffAppliedCC;
	}
	public void setCutoffAppliedCC(String cutoffAppliedCC) {
		this.cutoffAppliedCC = cutoffAppliedCC;
	}
	public String getBlackoutweb() {
		return blackoutweb;
	}
	public void setBlackoutweb(String blackoutweb) {
		this.blackoutweb = blackoutweb;
	}
	public String getBlackoutCC() {
		return blackoutCC;
	}
	public void setBlackoutCC(String blackoutCC) {
		this.blackoutCC = blackoutCC;
	}
	
	
	

}
