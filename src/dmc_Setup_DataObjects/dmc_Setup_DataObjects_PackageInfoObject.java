package dmc_Setup_DataObjects;

public class dmc_Setup_DataObjects_PackageInfoObject {
	
	String packageName;
	String packageType;
	int    numberOfPackageDays;
	String productIncludes;
	String childAllowedinPackage;
	String currencyCode;
	String supplierName;
	
	
	
	String contactName;
	String email;
	String country;
	String city;
	
	String bookingPeriodFrom;
	String bookingPeriodTo;
	String stayPeriodTo;
	String stayperiodFrom;
	
	
	
	
	public String getBookingPeriodFrom() {
		return bookingPeriodFrom;
	}
	public void setBookingPeriodFrom(String bookingPeriodFrom) {
		this.bookingPeriodFrom = bookingPeriodFrom;
	}
	public String getBookingPeriodTo() {
		return bookingPeriodTo;
	}
	public void setBookingPeriodTo(String bookingPeriodTo) {
		this.bookingPeriodTo = bookingPeriodTo;
	}
	public String getStayPeriodTo() {
		return stayPeriodTo;
	}
	public void setStayPeriodTo(String stayPeriodTo) {
		this.stayPeriodTo = stayPeriodTo;
	}
	public String getStayperiodFrom() {
		return stayperiodFrom;
	}
	public void setStayperiodFrom(String stayperiodFrom) {
		this.stayperiodFrom = stayperiodFrom;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPackageType() {
		return packageType;
	}
	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}
	public int getNumberOfPackageDays() {
		return numberOfPackageDays;
	}
	public void setNumberOfPackageDays(String numberOfPackageDays) {
		this.numberOfPackageDays = Integer.parseInt(numberOfPackageDays);
	}
	public String getProductIncludes() {
		return productIncludes;
	}
	public void setProductIncludes(String productIncludes) {
		this.productIncludes = productIncludes;
	}
	public String getChildAllowedinPackage() {
		return childAllowedinPackage;
	}
	public void setChildAllowedinPackage(String childAllowedinPackage) {
		this.childAllowedinPackage = childAllowedinPackage;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
	
	

}
