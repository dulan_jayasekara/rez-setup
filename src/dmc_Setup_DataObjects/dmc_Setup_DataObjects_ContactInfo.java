package dmc_Setup_DataObjects;

public class dmc_Setup_DataObjects_ContactInfo {

	
	String contactName;
String email;
String telephone;
String fax;
String contactMedia;
String contactType;

public String getContactName() {
	return contactName;
}
public void setContactName(String contactName) {
	this.contactName = contactName;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getTelephone() {
	return telephone;
}
public void setTelephone(String telephone) {
	this.telephone = telephone;
}
public String getFax() {
	return fax;
}
public void setFax(String fax) {
	this.fax = fax;
}
public String getContactMedia() {
	return contactMedia;
}
public void setContactMedia(String contactMedia) {
	this.contactMedia = contactMedia;
}
public String getContactType() {
	return contactType;
}
public void setContactType(String contactType) {
	this.contactType = contactType;
}
}
