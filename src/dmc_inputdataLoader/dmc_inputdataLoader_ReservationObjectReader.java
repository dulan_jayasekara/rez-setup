package dmc_inputdataLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import dmc_Reservations_DataObjects.dmc_Reservations_DataObjects_OccupancyObject;
import dmc_Reservations_DataObjects.dmc_Reservations_DataObjects_Scenario;
import dmc_Reservations_DataObjects.dmc_Reservations_DataObjects_SearchObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_DMCPackageObject;

public class dmc_inputdataLoader_ReservationObjectReader {

	public List<dmc_Reservations_DataObjects_Scenario> scenarioRead() {

		List<dmc_Reservations_DataObjects_Scenario> scenarioList=new ArrayList<dmc_Reservations_DataObjects_Scenario>();

		try {
			File fXmlFile = new File("DMCPackageReservations.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();

			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("Scenario");

			System.out.println("----------------------------");
			System.out.println("nList.getLength()"+nList.getLength());
			for (int temp = 0; temp < nList.getLength(); temp++) {

				dmc_Reservations_DataObjects_Scenario scenario=new dmc_Reservations_DataObjects_Scenario();

				Node nNode = nList.item(temp);
				System.out.println("Current Element :" + nNode.getNodeName());
				Element eElement = (Element) nNode;
				System.out.println(eElement.getNodeName());

				scenario.setUrl(eElement.getAttribute("url"));
				scenario.setBookingChannel(eElement.getAttribute("bookingChannel"));
				scenario.setPartnerType(eElement.getAttribute("partnerType"));

				dmc_Reservations_DataObjects_SearchObject search=new dmc_Reservations_DataObjects_SearchObject();

				Node searchnode=eElement.getElementsByTagName("search").item(0);
				Element searchElement=(Element) searchnode;

				search.setCountryOfResidence(searchElement.getAttribute("countryOfResidence"));
				search.setFlightInclusive(searchElement.getAttribute("flightInclusive"));
				search.setOrigin(searchElement.getAttribute("Origin"));
				search.setOriginValue(searchElement.getAttribute("OriginValue"));
				search.setDestination(searchElement.getAttribute("destination"));
				search.setDestinationValue(searchElement.getAttribute("destinationValue"));
				search.setDepartureMonth(searchElement.getAttribute("departureMonth"));
				search.setDuration(searchElement.getAttribute("duration"));
				search.setPackageType(searchElement.getAttribute("packageType"));
				search.setPreferredCurrency(searchElement.getAttribute("preferredCurrency"));
				search.setPackageStartDate(searchElement.getAttribute("packageStartDate"));
				search.setNoOfRooms(searchElement.getAttribute("noOfRooms"));
				
				int occupancynodeCount=eElement.getElementsByTagName("occupancy").getLength();
				
				List<dmc_Reservations_DataObjects_OccupancyObject> occupanyList=new ArrayList<dmc_Reservations_DataObjects_OccupancyObject>();

				
				for (int i = 0; i < occupancynodeCount; i++) {
					Node occupancynode=eElement.getElementsByTagName("occupancy").item(i);
					Element occupancyElement=(Element) occupancynode;
					
					dmc_Reservations_DataObjects_OccupancyObject occupancy=new dmc_Reservations_DataObjects_OccupancyObject();
					
					occupancy.setRoomNo(occupancyElement.getAttribute("roomNo"));
					occupancy.setAdultCount(occupancyElement.getAttribute("adultCount"));
					occupancy.setChildCount(occupancyElement.getAttribute("childCount"));
					occupancy.setInfantCount(occupancyElement.getAttribute("infantCount"));
					
					occupanyList.add(occupancy);
					
					
				}
				

				search.setOccupanyList(occupanyList);

				scenario.setSearch(search);
				
				
				scenarioList.add(scenario);

			}


		} catch (Exception e) {
			// TODO: handle exception
		}
		return scenarioList;
	}

	public static void main(String[] args) {

		dmc_inputdataLoader_ReservationObjectReader read=new dmc_inputdataLoader_ReservationObjectReader();
		
		
		System.out.println(read.scenarioRead().size());
		
		
		

	}

}
