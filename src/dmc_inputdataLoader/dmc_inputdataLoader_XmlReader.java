package dmc_inputdataLoader;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import dmc_Setup_DataObjects.dmc_Setup_DataObjects_ContactInfo;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_ContentObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_ContractObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_DMCPackageObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_Image;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_InventoryObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_ItineraryObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_MarkupObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_PackageInfoObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_PaxGroupObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_PaxOccupancyObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_RatesObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_StandardInfoObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_cancellationObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_partnerMarkupObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class dmc_inputdataLoader_XmlReader {

	public List<dmc_Setup_DataObjects_DMCPackageObject> readxml(Map<String, String> Propertymap) {
		List<dmc_Setup_DataObjects_DMCPackageObject> packageList=new ArrayList<dmc_Setup_DataObjects_DMCPackageObject>();

		try {

			File fXmlFile = new File("../Rezrobot_Details/Common/Setup_Details/DMCPackageSetup_rezproduction.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

		
			doc.getDocumentElement().normalize();

			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("DMCObject");

			System.out.println("----------------------------");
			System.out.println("nList.getLength()"+nList.getLength());




			for (int temp = 0; temp < nList.getLength(); temp++) {

				dmc_Setup_DataObjects_DMCPackageObject dmcPackage=new dmc_Setup_DataObjects_DMCPackageObject();
				List<dmc_Setup_DataObjects_RatesObject>  ratelist=new ArrayList<dmc_Setup_DataObjects_RatesObject>();

				Node nNode = nList.item(temp);



				System.out.println("Current Element :" + nNode.getNodeName());

				Element eElement = (Element) nNode;
				System.out.println(eElement.getNodeName());
				dmcPackage.setUrl(Propertymap.get("Hotel.BaseUrl"));
				dmcPackage.setPortal(eElement.getAttribute("Portal"));
				// standard Info Read
				Node standardInfo=eElement.getElementsByTagName("standardinfoObject").item(0);
				dmc_Setup_DataObjects_StandardInfoObject standardinfoObj=new dmc_Setup_DataObjects_StandardInfoObject();
				Element standardInfoElement = (Element) standardInfo;

				Node packageInfo=standardInfoElement.getElementsByTagName("packageInfo").item(0);
				standardinfoObj.setPackageinfo(readpackageInfo(packageInfo));

				int cancellationlistsize=standardInfoElement.getElementsByTagName("CancellaionObject").getLength();

				System.out.println("cancellationlistsize"+cancellationlistsize);

				List<dmc_Setup_DataObjects_cancellationObject> cxlList=new ArrayList<dmc_Setup_DataObjects_cancellationObject>();

				for (int i = 0; i < cancellationlistsize; i++) {

					Node cxlInfo=standardInfoElement.getElementsByTagName("CancellaionObject").item(i);

					cxlList.add(cxlRead(cxlInfo));



				}

				standardinfoObj.setCxlList(cxlList);
				Node contactInfo=standardInfoElement.getElementsByTagName("contactObject").item(0);
				standardinfoObj.setContactInfo(readContactInfo(contactInfo));
				dmcPackage.setStandardInfo(standardinfoObj);

				//ContractTabread
				Node contractInfo=eElement.getElementsByTagName("contractObject").item(0);
				dmcPackage.setContract(contractInfo(contractInfo));

				//contentTabRead

				Node contentInfo=eElement.getElementsByTagName("contentObject").item(0); 
				dmcPackage.setContent(contentRead(contentInfo));



				//Assign Markup tab

				Node markupInfo=eElement.getElementsByTagName("MarkupObject").item(0);

				dmcPackage.setProfitMarkup(markupInfo(markupInfo));


//inventory read


				Node inventoryinfo=eElement.getElementsByTagName("inventory").item(0);

				dmcPackage.setInventory(inventoryRead(inventoryinfo));
				
				
				
				int ratecontract=eElement.getElementsByTagName("rates").getLength();
				
				for (int i = 0; i < ratecontract; i++) {
					
					Node rateinfo=eElement.getElementsByTagName("rates").item(i);
					ratelist.add(rateRead(rateinfo));
					
					
				}
				
				dmcPackage.setRatelist(ratelist);




				packageList.add(dmcPackage);



			}



		} catch (Exception e) {
			e.printStackTrace();
		}
		return packageList;
	}

	public void dmcObjectRead(NodeList nList) {



	}
	
	public dmc_Setup_DataObjects_RatesObject rateRead(Node rateinfo) {
		
		dmc_Setup_DataObjects_RatesObject rateobj=new dmc_Setup_DataObjects_RatesObject();
		
		Element rateElement=(Element) rateinfo;
		
		//			 infantSupplement="" landComponentTaxBy="" landComponentTaxValue=""></rates>

		rateobj.setPaxGroup(rateElement.getAttribute("paxGroup"));
		rateobj.setFlightrate(rateElement.getAttribute("flightrate"));
		rateobj.setSingleRoomRate(rateElement.getAttribute("singleRoomRate"));
		rateobj.setTwinRoomRate(rateElement.getAttribute("twinRoomRate"));
		rateobj.setTripleRoomRate(rateElement.getAttribute("tripleRoomRate"));
		rateobj.setChildWithoutBedRate(rateElement.getAttribute("childWithoutBedRate"));
		rateobj.setChildWithBedRate(rateElement.getAttribute("childWithBedRate"));
		rateobj.setInfantSupplement(rateElement.getAttribute("infantSupplement"));
		rateobj.setLandComponentTaxBy(rateElement.getAttribute("landComponentTaxBy"));
		rateobj.setLandComponentTaxValue(rateElement.getAttribute("landComponentTaxValue"));

		
		
		
		
		return rateobj;
		
	}
	
	public dmc_Setup_DataObjects_InventoryObject inventoryRead(Node inventoryinfo) {
		
		dmc_Setup_DataObjects_InventoryObject inventoyobj=new dmc_Setup_DataObjects_InventoryObject();
		Element inventoryelement=(Element) inventoryinfo;
		
		inventoyobj.setTotalunits(inventoryelement.getAttribute("totalunits"));
		inventoyobj.setCutoff(inventoryelement.getAttribute("cutoff"));
		inventoyobj.setBlockinventory(inventoryelement.getAttribute("blockinventory"));
		
		return inventoyobj;
		
	}
	
	
	
	public dmc_Setup_DataObjects_ContentObject contentRead(Node contentInfo) {

		dmc_Setup_DataObjects_ContentObject content=new dmc_Setup_DataObjects_ContentObject();

		Element contentinfoelement=(Element) contentInfo;

		content.setShortDescription(contentinfoelement.getElementsByTagName("ShortDescription").item(0).getTextContent());
		content.setPackageInclusion(contentinfoelement.getElementsByTagName("PackageInclusion").item(0).getTextContent());
		content.setPackageExclusion(contentinfoelement.getElementsByTagName("PackageExclusion").item(0).getTextContent());
		content.setDestinationDetails(contentinfoelement.getElementsByTagName("DestinationDetails").item(0).getTextContent());
		content.setSpecialNotes(contentinfoelement.getElementsByTagName("SpecialNotes").item(0).getTextContent());
		content.setContactDetailsonArrival(contentinfoelement.getElementsByTagName("ContactDetailsonArrival").item(0).getTextContent());
		content.setTermsandConditions(contentinfoelement.getElementsByTagName("TermsandConditions").item(0).getTextContent());
		content.setPackageCurrencyCode(contentinfoelement.getElementsByTagName("PackageCurrencyCode").item(0).getTextContent());
		content.setAirSectorPrice(contentinfoelement.getElementsByTagName("airSectorPrice").item(0).getTextContent());

		content.setLandSectorPrice(contentinfoelement.getElementsByTagName("LandSectorPrice").item(0).getTextContent());

		List<dmc_Setup_DataObjects_ItineraryObject> itineraryDayWise=new ArrayList<dmc_Setup_DataObjects_ItineraryObject>();
		int numberofDays=contentinfoelement.getElementsByTagName("itineraryObj").getLength();

		for (int i = 0; i < numberofDays; i++) {

			dmc_Setup_DataObjects_ItineraryObject itineraryDay=new dmc_Setup_DataObjects_ItineraryObject();
			Node day=contentinfoelement.getElementsByTagName("itineraryObj").item(i);
			Element dayelement=(Element) day;
			itineraryDay.setDay(dayelement.getAttribute("day"));
			itineraryDay.setTitle(dayelement.getElementsByTagName("title").item(0).getTextContent());
			itineraryDay.setImage(dayelement.getElementsByTagName("image").item(0).getTextContent());
			itineraryDay.setDescription(dayelement.getElementsByTagName("description").item(0).getTextContent());


			itineraryDayWise.add(itineraryDay);


		}
		content.setItineraryDayWise(itineraryDayWise);
		
		List<dmc_Setup_DataObjects_Image> imagelist=new ArrayList<dmc_Setup_DataObjects_Image>();
		
		int numberofImages=contentinfoelement.getElementsByTagName("imagecontent").getLength();

		for (int i = 0; i < numberofImages; i++) {

			dmc_Setup_DataObjects_Image imageinfo=new dmc_Setup_DataObjects_Image();
			Node imagenode=contentinfoelement.getElementsByTagName("imagecontent").item(i);
			Element imageelement=(Element) imagenode;
			imageinfo.setImageCaption(imageelement.getAttribute("imageCaption"));
			imageinfo.setImageFile(imageelement.getAttribute("imageFile"));
			imageinfo.setThumbnailImage(imageelement.getAttribute("thumbnailImage"));
			imageinfo.setDisplayorder(imageelement.getAttribute("displayorder"));

			
			System.out.println("Thumbnail"+imageinfo.getThumbnailImage());

			imagelist.add(imageinfo);


		}
		content.setImagelist(imagelist);
		
		
		

		
		return content;

	}
	public dmc_Setup_DataObjects_MarkupObject markupInfo(Node markupInfo) {

		dmc_Setup_DataObjects_MarkupObject markupObject=new dmc_Setup_DataObjects_MarkupObject();
		Map<String, dmc_Setup_DataObjects_partnerMarkupObject> toNetMarkupmap=new HashMap<String, dmc_Setup_DataObjects_partnerMarkupObject>();
		Map<String, dmc_Setup_DataObjects_partnerMarkupObject> toComMarkupmap=new HashMap<String, dmc_Setup_DataObjects_partnerMarkupObject>();
		Map<String, dmc_Setup_DataObjects_partnerMarkupObject> affMarkupmap=new HashMap<String, dmc_Setup_DataObjects_partnerMarkupObject>();


		Element markupInfoelement=(Element) markupInfo;
		markupInfoelement.getElementsByTagName("DCMarkup").item(0);
		dmc_Setup_DataObjects_partnerMarkupObject dcmarkup=new dmc_Setup_DataObjects_partnerMarkupObject();
		Node dcmarknode=markupInfoelement.getElementsByTagName("DCMarkup").item(0);
		Element dcelement=(Element) dcmarknode;
		dcmarkup.setProfitMarkupType(dcelement.getAttribute("type"));
		dcmarkup.setProfitMarkup(dcelement.getTextContent());
		System.out.println(dcmarkup.getProfitMarkup());

		markupObject.setDirectCustomerMarkup(dcmarkup);

		int numberoftoNetmarkupmarknode=markupInfoelement.getElementsByTagName("TONetMarkup").getLength();

		for (int i = 0; i < numberoftoNetmarkupmarknode; i++) {

			dmc_Setup_DataObjects_partnerMarkupObject toNetmarkup=new dmc_Setup_DataObjects_partnerMarkupObject();

			Node toNetmarkupmarknode=markupInfoelement.getElementsByTagName("TONetMarkup").item(i);
			Element toNetmarkupelement=(Element) toNetmarkupmarknode;
			toNetmarkup.setProfitMarkupType(toNetmarkupelement.getAttribute("type"));
			toNetmarkup.setProfitMarkup(toNetmarkupelement.getTextContent());
			toNetmarkup.setPartnerName(toNetmarkupelement.getAttribute("TOName"));

			toNetMarkupmap.put(toNetmarkup.getPartnerName(), toNetmarkup);



		}



		int numberoftoCommarkupmarknode=markupInfoelement.getElementsByTagName("TOComMarkup").getLength();

		for (int i = 0; i < numberoftoCommarkupmarknode; i++) {

			dmc_Setup_DataObjects_partnerMarkupObject tocommarkup=new dmc_Setup_DataObjects_partnerMarkupObject();

			Node tocommarkupmarknode=markupInfoelement.getElementsByTagName("TOComMarkup").item(i);
			Element tocommarkupelement=(Element) tocommarkupmarknode;

			tocommarkup.setPartnerName(tocommarkupelement.getAttribute("TOName"));

			Node tocommarkupPMnode=tocommarkupelement.getElementsByTagName("Markup").item(0);
			Element tocommarkupPMelement=(Element) tocommarkupPMnode;
			tocommarkup.setProfitMarkupType(tocommarkupPMelement.getAttribute("type"));
			tocommarkup.setProfitMarkup(tocommarkupPMelement.getTextContent());

			Node tocommarkupcommissionnode=tocommarkupelement.getElementsByTagName("Commission").item(0);
			Element tocommarkupcommissionelement=(Element) tocommarkupcommissionnode;
			tocommarkup.setCommissionType(tocommarkupcommissionelement.getAttribute("type"));
			tocommarkup.setCommission(tocommarkupcommissionelement.getTextContent());


			toNetMarkupmap.put(tocommarkup.getPartnerName(), tocommarkup);



		}

		int numberofaffmarknode=markupInfoelement.getElementsByTagName("AffMarkup").getLength();

		for (int i = 0; i < numberofaffmarknode; i++) {

			dmc_Setup_DataObjects_partnerMarkupObject affmarkup=new dmc_Setup_DataObjects_partnerMarkupObject();

			Node affmarkupnode=markupInfoelement.getElementsByTagName("AffMarkup").item(i);
			Element affmarkupnodeelement=(Element) affmarkupnode;

			affmarkup.setPartnerName(affmarkupnodeelement.getAttribute("TOName"));

			Node affmarkupPMnode=affmarkupnodeelement.getElementsByTagName("Markup").item(0);
			Element affmarkupPMelement=(Element) affmarkupPMnode;
			affmarkup.setProfitMarkupType(affmarkupPMelement.getAttribute("type"));
			affmarkup.setProfitMarkup(affmarkupPMelement.getTextContent());

			Node affcommissionnode=affmarkupnodeelement.getElementsByTagName("Commission").item(0);
			Element affcommissionelement=(Element) affcommissionnode;
			affmarkup.setCommissionType(affcommissionelement.getAttribute("type"));
			affmarkup.setCommission(affcommissionelement.getTextContent());


			toNetMarkupmap.put(affmarkup.getPartnerName(), affmarkup);



		}

		markupObject.setToNetMarkupmap(toNetMarkupmap);
		markupObject.setToComMarkupmap(toComMarkupmap);
		markupObject.setAffMarkupmap(affMarkupmap);


		return markupObject;


	}

	public dmc_Setup_DataObjects_ContactInfo readContactInfo(Node contactInfo) {
		dmc_Setup_DataObjects_ContactInfo contactInfoobj=new dmc_Setup_DataObjects_ContactInfo();


		Element contactInfoElement=(Element) contactInfo;

		contactInfoobj.setContactName(contactInfoElement.getAttribute("ContactName")); 
		contactInfoobj.setEmail(contactInfoElement.getAttribute("Email"));
		contactInfoobj.setFax(contactInfoElement.getAttribute("Fax"));
		contactInfoobj.setContactMedia(contactInfoElement.getAttribute("ContactMedia"));
		contactInfoobj.setContactType(contactInfoElement.getAttribute("ContactType"));
		System.out.println(contactInfoobj.getContactMedia());

		return contactInfoobj;

	}


	public dmc_Setup_DataObjects_ContractObject contractInfo(Node contractInfo) {

		dmc_Setup_DataObjects_ContractObject contractObj=new dmc_Setup_DataObjects_ContractObject();
		List<dmc_Setup_DataObjects_PaxOccupancyObject> paxOccupancylist=new ArrayList<dmc_Setup_DataObjects_PaxOccupancyObject>();
		List<dmc_Setup_DataObjects_PaxGroupObject> paxGroupList=new ArrayList<dmc_Setup_DataObjects_PaxGroupObject>();

		Element contractelement=(Element) contractInfo;
		contractObj.setInventoryType(contractelement.getAttribute("inventoryType"));
		contractObj.setSearchSatisfiedweb(contractelement.getAttribute("searchSatisfiedweb"));
		contractObj.setSearchSatisfiedCC(contractelement.getAttribute("searchSatisfiedCC"));
		contractObj.setInventoryExhaustedweb(contractelement.getAttribute("inventoryExhaustedweb"));
		contractObj.setInventoryExhaustedCC(contractelement.getAttribute("inventoryExhaustedCC"));
		contractObj.setCutoffAppliedweb(contractelement.getAttribute("cutoffAppliedweb"));
		contractObj.setCutoffAppliedCC(contractelement.getAttribute("cutoffAppliedCC"));
		contractObj.setBlackoutweb(contractelement.getAttribute("blackoutweb"));
		contractObj.setBlackoutCC(contractelement.getAttribute("blackoutCC"));


		int paxoccupancySize=contractelement.getElementsByTagName("paxoccupancy").getLength();

		for (int i = 0; i < paxoccupancySize; i++) {

			dmc_Setup_DataObjects_PaxOccupancyObject paxoccObj=new dmc_Setup_DataObjects_PaxOccupancyObject();

			Node paxoccupancynode=contractelement.getElementsByTagName("paxoccupancy").item(i);
			Element paxoccupancyelement=(Element) paxoccupancynode;


			paxoccObj.setRoomType(paxoccupancyelement.getAttribute("roomType"));
			paxoccObj.setNoOfAdults(paxoccupancyelement.getAttribute("noOfAdults"));
			paxoccObj.setNoOfChildren(paxoccupancyelement.getAttribute("noOfChildern"));


			paxOccupancylist.add(paxoccObj);

		}

		contractObj.setPaxOccupancylist(paxOccupancylist);
		int paxgroupSize=contractelement.getElementsByTagName("paxGroup").getLength();

		System.out.println("paxgroupSize"+paxgroupSize);

		for (int i = 0; i < paxgroupSize; i++) {

			dmc_Setup_DataObjects_PaxGroupObject paxgroupObj=new dmc_Setup_DataObjects_PaxGroupObject();

			Node paxgroupnode=contractelement.getElementsByTagName("paxGroup").item(i);
			Element paxgroupelement=(Element) paxgroupnode;


			paxgroupObj.setMaxNoOfPax(paxgroupelement.getAttribute("maxNoOfPax"));
			paxgroupObj.setMinNoOfPax(paxgroupelement.getAttribute("minNoOfPax"));
			paxgroupObj.setCombinationActive("Y");


			paxGroupList.add(paxgroupObj);

		}
		
		contractObj.setPaxGroupList(paxGroupList);

		return contractObj;

	}
	public dmc_Setup_DataObjects_PackageInfoObject readpackageInfo(Node packageInfo) {

		dmc_Setup_DataObjects_PackageInfoObject packageinfoobj=new dmc_Setup_DataObjects_PackageInfoObject();

		Element packageinfoElement=(Element) packageInfo;

		packageinfoobj.setPackageName(packageinfoElement.getAttribute("PackageName")); 
		System.out.println("PP="+packageinfoobj.getPackageName());
		packageinfoobj.setPackageType(packageinfoElement.getAttribute("PackageType"));
		packageinfoobj.setNumberOfPackageDays(packageinfoElement.getAttribute("PackageDays"));
		packageinfoElement.getAttribute("PackageActiveStatus");
		packageinfoElement.getAttribute("StopSale");
		packageinfoElement.getAttribute("BookingChannel");
		packageinfoElement.getAttribute("DisplayRanking");
		packageinfoobj.setProductIncludes(packageinfoElement.getAttribute("ProductIncludes"));
		packageinfoobj.setChildAllowedinPackage(packageinfoElement.getAttribute("ChildAllowed"));
		packageinfoobj.setCurrencyCode(packageinfoElement.getAttribute("CurrencyCode"));
		packageinfoobj.setSupplierName(packageinfoElement.getAttribute("SupplierName"));
		packageinfoobj.setCountry(packageinfoElement.getAttribute("Country"));
		packageinfoobj.setCity(packageinfoElement.getAttribute("City"));
		packageinfoobj.setBookingPeriodFrom(packageinfoElement.getAttribute("BookingPeriodFrom"));
		packageinfoobj.setBookingPeriodTo(packageinfoElement.getAttribute("BookingPeriodTo"));
		packageinfoobj.setStayperiodFrom(packageinfoElement.getAttribute("StayperiodFrom"));
		packageinfoobj.setStayPeriodTo(packageinfoElement.getAttribute("StayPeriodTo"));


		return packageinfoobj;

	}


	public dmc_Setup_DataObjects_cancellationObject cxlRead(Node cxlinfo) {

		dmc_Setup_DataObjects_cancellationObject cxlObject=new dmc_Setup_DataObjects_cancellationObject();

		Element cxlElement=(Element) cxlinfo;

		cxlObject.setFromDate(cxlElement.getAttribute("FromDate"));
		cxlObject.setToDate(cxlElement.getAttribute("ToDate"));
		cxlObject.setPackageRefundable(cxlElement.getAttribute("PackageRefundable"));

		if (cxlObject.getPackageRefundable().contains("Y")) {
			Node stdCancellation=cxlElement.getElementsByTagName("StandardCancellation").item(0);
			Element stdcancellationElement=(Element) stdCancellation;
			cxlObject.setApplyifarrivaldateislessthan(stdcancellationElement.getAttribute("Applyifarrivaldateislessthan"));
			cxlObject.setBasedon(stdcancellationElement.getAttribute("Basedon"));

		}



		return cxlObject;

	}

	public static void main(String[] args) {

		dmc_inputdataLoader_XmlReader read=new dmc_inputdataLoader_XmlReader();
//		read.readxml();

	}



}

