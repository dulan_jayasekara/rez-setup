package dmc_Reservations_Calculators;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;

import dmc_Reservations_DataObjects.dmc_Reservations_DataObjects_DisplayRateObject;
import dmc_Reservations_DataObjects.dmc_Reservations_DataObjects_Scenario;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_DMCPackageObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_PaxGroupObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_PaxOccupancyObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_RatesObject;
import dmc_inputdataLoader.dmc_inputdataLoader_ReservationObjectReader;
import dmc_inputdataLoader.dmc_inputdataLoader_XmlReader;

public class dmc_Reservations_Calculators_occupancyCalculators {
	
	dmc_Reservations_DataObjects_DisplayRateObject rateobj=new dmc_Reservations_DataObjects_DisplayRateObject();
	dmc_Setup_DataObjects_RatesObject grouprate=new dmc_Setup_DataObjects_RatesObject();
	int totalRoombase=0;
	
	
	int totalOccupancy=0;
	
	public void SelectTheRooms(dmc_Reservations_DataObjects_Scenario scenario1,dmc_Setup_DataObjects_DMCPackageObject package1) {
		
		
		
		int occupancyCount=scenario1.getSearch().getOccupanyList().size();
		
		
		
		for (int i = 0; i < occupancyCount; i++) {
			
			
			int adultCount=Integer.parseInt(scenario1.getSearch().getOccupanyList().get(i).getAdultCount());
			int childCount=Integer.parseInt(scenario1.getSearch().getOccupanyList().get(i).getChildCount());
			int infantCount=Integer.parseInt(scenario1.getSearch().getOccupanyList().get(i).getInfantCount());
		
			totalOccupancy=totalOccupancy+adultCount+childCount+infantCount;
		}
		
		System.out.println("totalOccupancy"+totalOccupancy);
		for (int i = 0; i < occupancyCount; i++) {
			
			int roombase=0;
			int nextroombase=0;
			int infantBase=0;
			int childwithoutbedbase=0;
			int childwithbedbase=0;
			int diffwithoutbedtoinfant=0;
			int diffwithbedtoinfant=0;
			int diffnextroomtoinfant=0;
			int diffchildwithbedtochildwithoutbed=0;
			int diffnextroomtochildwithoutbed=0;
			
			int adultCount=Integer.parseInt(scenario1.getSearch().getOccupanyList().get(i).getAdultCount());
			int childCount=Integer.parseInt(scenario1.getSearch().getOccupanyList().get(i).getChildCount());
			int infantCount=Integer.parseInt(scenario1.getSearch().getOccupanyList().get(i).getInfantCount());
			
			System.out.println(infantCount);

			
			String paxgroup="";
			
			// Selecting the Occupancy Group
			
			for (int j = 0; j < package1.getContract().getPaxGroupList().size(); j++) {
				int min=Integer.parseInt(package1.getContract().getPaxGroupList().get(j).getMinNoOfPax());
				int max=Integer.parseInt(package1.getContract().getPaxGroupList().get(j).getMaxNoOfPax());

				
				if ((totalOccupancy>=min) && (totalOccupancy<=max)) {
					
					paxgroup=package1.getContract().getPaxGroupList().get(j).getMinNoOfPax()+" - "+package1.getContract().getPaxGroupList().get(j).getMaxNoOfPax();
					
				}
				
				
				
			}
			System.out.println("paxgroup"+paxgroup);
			for (int j = 0; j < package1.getRatelist().size(); j++) {
				
				if (package1.getRatelist().get(j).getPaxGroup().contains(paxgroup)) {
					
					grouprate=package1.getRatelist().get(j);
					
				}
			}
			
			System.out.println("package1.getContract().getPaxOccupancylist().size("+package1.getContract().getPaxOccupancylist().size());
			
			for (int j = 0; j < package1.getContract().getPaxOccupancylist().size(); j++) {
				
				String roomName="";
				String nextRoomName="";
			
				
				
				dmc_Setup_DataObjects_PaxOccupancyObject occupancyroom=new dmc_Setup_DataObjects_PaxOccupancyObject();
				
				occupancyroom=package1.getContract().getPaxOccupancylist().get(j);
				
				int roomadultCount=Integer.parseInt(occupancyroom.getNoOfAdults());
				int roomchildCount=Integer.parseInt(occupancyroom.getNoOfChildren());
				
				
				if (roomadultCount==1) {
					
					roomName="Single";
					nextRoomName="Twin";
					
				}else if (roomadultCount==2) {
					roomName="Twin";
					nextRoomName="Triple";
				}else {
					roomName="Triple";
				}
				
				
				System.out.println(occupancyroom.getRoomType());
				if ((adultCount+childCount+infantCount)<=(roomadultCount+roomchildCount)) {
					
					if (adultCount==roomadultCount) {
						
					
						
						
						if (roomName.contains("Single")) {
							roombase=Integer.parseInt(grouprate.getSingleRoomRate())*1;
							nextroombase=Integer.parseInt(grouprate.getTwinRoomRate())*2;
						}else if (roomName.contains("Twin")) {
							roombase=Integer.parseInt(grouprate.getTwinRoomRate())*2;
							nextroombase=Integer.parseInt(grouprate.getTripleRoomRate())*3;

						}else {
							roombase=Integer.parseInt(grouprate.getTripleRoomRate())*3;

						}
						
						infantBase=roombase+Integer.parseInt(grouprate.getInfantSupplement());
						childwithoutbedbase=roombase+Integer.parseInt(grouprate.getChildWithoutBedRate());
						childwithbedbase=roombase+Integer.parseInt(grouprate.getChildWithBedRate());
						diffwithoutbedtoinfant=childwithoutbedbase-infantBase;
						diffwithbedtoinfant=childwithbedbase-infantBase;
						diffnextroomtoinfant=nextroombase-infantBase;
						diffchildwithbedtochildwithoutbed=childwithbedbase-childwithoutbedbase;
						diffnextroomtochildwithoutbed=nextroombase-childwithoutbedbase;
						
						System.out.println("Infant Count="+infantCount);
						if ((infantCount+childCount)==0) {
							
							System.out.println(roomName+" + USD 0");
							
							
						}else {
							if( (infantCount>0)&& (childCount==0)) {
								
								// Rate
								roombase=infantBase;
								System.out.println("roombase"+roombase);
								System.out.println(roomName.replace("Standard", "").replace("Room", "").trim()+" Sharing with Infant Supplement + USD 0");
								infantBase=roombase+Integer.parseInt(grouprate.getInfantSupplement());
								System.out.println(roomName.replace("Standard", "").replace("Room", "").trim()+" Sharing Child without Bed + USD "+diffwithoutbedtoinfant);
								System.out.println(roomName.replace("Standard", "").replace("Room", "").trim()+" Sharing Child with Bed + USD "+diffwithbedtoinfant);
								if (!(nextRoomName=="")) {
									System.out.println("Standard "+nextRoomName+" Room + USD  "+diffnextroomtoinfant);

								}

								


							}
							
							if ( (infantCount==0)&& (childCount>0)) {
								roombase=childwithoutbedbase;
							/*	System.out.println(roomName+" Sharing Child without Bed + USD 0");
								System.out.println(roomName+" Sharing Child with Bed + USD "+diffchildwithbedtochildwithoutbed);
							*/	if (!(nextRoomName=="")) {
/*									System.out.println("Standard "+nextRoomName+" Room + USD  "+diffnextroomtochildwithoutbed);
*/
								}

								
							}
							
/*if ( (infantCount==0)&& (childCount>0)) {
								
								System.out.println(roomName+" Sharing Child without Bed + USD 0");
								System.out.println(roomName+" Sharing Child with Bed + USD "+diffchildwithbedtochildwithoutbed);
								if (!(nextRoomName=="")) {
									System.out.println("Standard "+nextRoomName+" Room + USD  "+diffnextroomtochildwithoutbed);

								}

								
							}*/
						}
						
						/*if ((infantCount+childCount)<=roomchildCount) {
							
							System.out.println(occupancyroom.getRoomType());
							
						}else if((infantCount+childCount+adultCount)<=roomadultCount){
							
							System.out.println(occupancyroom.getRoomType());
						}*/
						
						
					}
					
				}
				
				
				
				
				
				
			}
			
			/*if (adultCount) {
				
			}*/
			
			
			totalRoombase=totalRoombase+roombase;
		}
		
		
		
		
		
		
	}
	
	public dmc_Reservations_DataObjects_DisplayRateObject ratescal(dmc_Reservations_DataObjects_Scenario scenario1,dmc_Setup_DataObjects_DMCPackageObject package1) {
		
		
		SelectTheRooms(scenario1, package1);
		rateobj.setAirbasefare(0);
		rateobj.setAirtax(0);
		rateobj.setLandbasefare(totalRoombase);
		
		System.out.println(rateobj.getLandbasefare());
		double tax=0;
		double profitMarkup=0;
		double profitMarkup1=0;
		profitMarkup1=(int) package1.getProfitMarkup().getDirectCustomerMarkup().getProfitMarkup();
		System.out.println(profitMarkup1);
		
		System.out.println(profitMarkup1/100);
		System.out.println(rateobj.getLandbasefare()+rateobj.getAirbasefare());
		profitMarkup=((rateobj.getLandbasefare()+rateobj.getAirbasefare())*(profitMarkup1/100));
		System.out.println(Math.ceil(profitMarkup));
		
		tax=((Double.parseDouble(grouprate.getLandComponentTaxValue())/100)*rateobj.getLandbasefare())+rateobj.getAirtax();
		
		
		System.out.println(tax);
		
		System.out.println("subtotal"+(rateobj.getLandbasefare()+rateobj.getAirbasefare()+profitMarkup));
		System.out.println("tax"+tax);
		System.out.println("total"+(rateobj.getLandbasefare()+rateobj.getAirbasefare()+profitMarkup+tax));
		return rateobj;
	}
	
	
	public static void main(String[] args) {
		
//		dmc_inputdataLoader_ReservationObjectReader reader=new dmc_inputdataLoader_ReservationObjectReader();
//		dmc_Reservations_DataObjects_Scenario scenario1=new dmc_Reservations_DataObjects_Scenario();
//		scenario1=reader.scenarioRead().get(0);
//		
//		
//		dmc_inputdataLoader_XmlReader reader2=new dmc_inputdataLoader_XmlReader();
//		List<dmc_Setup_DataObjects_DMCPackageObject> packageList=new ArrayList<dmc_Setup_DataObjects_DMCPackageObject>();
//
//			packageList=reader2.readxml();
//		
//		dmc_Setup_DataObjects_DMCPackageObject packageselected=new dmc_Setup_DataObjects_DMCPackageObject();
//		packageselected=packageList.get(3);
//		System.out.println("PAckage"+packageselected.getStandardInfo().getPackageinfo().getPackageName());
//		
//		dmc_Reservations_Calculators_occupancyCalculators calc=new dmc_Reservations_Calculators_occupancyCalculators();
//		calc.ratescal(scenario1,packageselected);
	}

}
