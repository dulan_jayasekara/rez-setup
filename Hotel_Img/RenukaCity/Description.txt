Hotel Renuka and Renuka City Hotel, brings you to the heart of Colombo and is conveniently located just steps away from some of the City�s best attractions. 


Yet, the hotels quiet residential neighborhood makes it an ideal getaway for business and leisure travelers who prefer a more serene atmosphere.
Once you enter our doors, you�ll experience the best in luxury accommodation,
complemented by the highest levels of service designed to cater to your discerning taste. Let us do all the work for you, while you discover true Sri Lankan hospitality, delivered with a smile.


Latitude=6.927078600000000000
Longitude=79.861243000000060000